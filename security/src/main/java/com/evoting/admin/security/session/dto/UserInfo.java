package com.evoting.admin.security.session.dto;

import com.evoting.entity.security.entity.UserEntity;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserInfo {
    private Integer id;
    private String firstName;
    private String lastName;

    public UserInfo(UserEntity entity) {
        if (entity != null) {
            this.id = entity.getId();
            this.firstName = entity.getFirstName();
            this.lastName = entity.getLastName();
        }
    }
}
