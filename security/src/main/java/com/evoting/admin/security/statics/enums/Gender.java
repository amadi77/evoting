package com.evoting.admin.security.statics.enums;

/**
 * @author evoting on 5/18/19
 */
public enum Gender {

    MALE(0),
    FEMALE(1);

    int value;

    Gender(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }}
