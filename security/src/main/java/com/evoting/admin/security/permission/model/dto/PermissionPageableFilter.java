package com.evoting.admin.security.permission.model.dto;

import com.evoting.utility.model.object.SortOption;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class PermissionPageableFilter extends PermissionFilter {

    private Integer page;
    private Integer size;
    private List<SortOption> sort;

    public PermissionPageableFilter() {
        this.page = 1;
        this.size = 50;
        this.sort = new ArrayList<>();
    }

}
