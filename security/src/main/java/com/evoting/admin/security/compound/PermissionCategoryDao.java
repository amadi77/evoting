//package com.evoting.security.compound;
//
//import com.evoting.database.security.PermissionCategoryEntity;
//import com.evoting.database.security.enums.PermissionType;
//import com.evoting.utility.db.provider.data.SpringDataBaseDao;
//import org.springframework.stereotype.Repository;
//
//import java.util.List;
//
//@Repository
//public interface PermissionCategoryDao extends SpringDataBaseDao<PermissionCategoryEntity, Integer> {
//    List<PermissionCategoryEntity> findAllByPermissionType(PermissionType permissionType);
//}
