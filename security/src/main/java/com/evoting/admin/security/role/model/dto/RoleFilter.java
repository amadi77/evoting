package com.evoting.admin.security.role.model.dto;

import com.evoting.utility.repository.service.FilterBase;
import lombok.Data;

@Data
public class RoleFilter implements FilterBase {

    private Integer id;
    private String name;
    private Boolean active;
//    private RoleType type;
}
