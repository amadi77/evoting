package com.evoting.admin.security.session.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserSessionIn {
    private String ip;
    private Integer userId;
    private String agent;
}
