package com.evoting.admin.security.repository;

import com.evoting.entity.security.entity.UserEntity;
import com.evoting.utility.repository.Dao;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class UserDao extends Dao<UserEntity> {

    public UserEntity findByUsername(String username) {
        Query query = this.getEntityManager().createQuery("select user from UserEntity user where user.username=:username or user.phoneNumber = :username");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("username", username);
        List<UserEntity> users = this.queryHql(query, parameters);
        return users.isEmpty() ? null : users.get(0);
    }

    public List<UserEntity> getAllByIds(Iterable<Integer> integerList) {
        Query query = this.getEntityManager().createQuery("select user from UserEntity user where user.id in (:ids)");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("ids", integerList);
        return this.queryHql(query, parameters);
    }

    public UserEntity getById(Integer id) {
        Query query = this.getEntityManager().createQuery("select user from UserEntity user where user.id = :id");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("id", id);
        List<UserEntity> users = this.queryHql(query, parameters);
        return users.isEmpty() ? null : users.get(0);
    }

    public boolean existsByUsernameAndActive(String username, boolean active) {
        Query query = this.getEntityManager().createQuery("select user from UserEntity user where (user.username=:username or user.phoneNumber = :username) and user.active = :active");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("username", username);
        parameters.put("active", active);
        List<UserEntity> users = this.queryHql(query, parameters);
        return users.isEmpty();
    }


}
