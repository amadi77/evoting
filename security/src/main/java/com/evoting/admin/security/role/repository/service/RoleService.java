package com.evoting.admin.security.role.repository.service;

import com.evoting.utility.model.object.ReportFilter;
import com.evoting.utility.model.object.SystemException;
import com.evoting.utility.model.object.report.ReportCondition;
import com.evoting.utility.model.object.report.ReportOption;
import com.evoting.utility.repository.AbstractFilterableService;
import com.evoting.utility.repository.Dao;
import com.evoting.entity.security.entity.UserRoleEntity;
import com.evoting.admin.security.role.model.dto.RoleFilter;
import com.evoting.admin.security.role.model.dto.RoleIn;
import com.evoting.admin.security.role.model.dto.RoleOut;
import com.evoting.admin.security.role.model.dto.RolePageableFilter;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RoleService extends AbstractFilterableService<UserRoleEntity, RoleFilter, Dao<UserRoleEntity>> {

    private final ModelMapper modelMapper;
//    private final RoleDao roleDao;

    @Autowired
//    public RoleService(RoleDao roleDao, ModelMapper modelMapper) {
    public RoleService(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
//        this.roleDao = roleDao;
    }

    public List<UserRoleEntity> getAllEntities(RolePageableFilter rolePageableFilter, String[] include) {
        return getAllEntities(filter(rolePageableFilter), include);
    }

    public int count(RoleFilter roleFilter) {
        return countEntity(filter(roleFilter));
    }


    public boolean delete(int id) {
        return deleteById(id);
    }

    //com.evoting.response.com.evoting.answer.com.evoting.supplement.dto methods

    public List<RoleOut> getAll(RolePageableFilter rolePageableFilter, String[] include) {
        List<UserRoleEntity> entities = getAllEntities(rolePageableFilter, include);
        if (entities != null) {
            return entities.stream().map(source -> modelMapper.map(source, RoleOut.class))
                    .collect(Collectors.toList());
        } else {
            return new ArrayList<>();
        }
    }

    public RoleOut getById(int id, String[] include) throws SystemException {
        return modelMapper.map(getEntityById(id, include), RoleOut.class);
    }

    public RoleOut create(RoleIn roleIn) {
        return modelMapper.map(createEntity(modelMapper.map(roleIn, UserRoleEntity.class)), RoleOut.class);

    }

    public RoleOut update(int id, RoleIn roleIn) throws SystemException {
        UserRoleEntity roleEntity = getEntityById(id, null);
        modelMapper.map(roleIn, roleEntity);
        updateEntity(roleEntity);
        return modelMapper.map(roleEntity, RoleOut.class);
    }

//    public List<UserRoleEntity> getByIds(List<Integer> ids) {
//        return roleDao.findAllById(ids);
//    }

//    public List<UserRoleEntity> getRolesByType(RoleType type) {
//        return roleDao.findByRoleTypeAndActive(type, true);
//    }


    @Override
    public ReportFilter filter(RoleFilter filter) {
        ReportOption reportOption = new ReportOption();
        if (filter instanceof RolePageableFilter) {
            RolePageableFilter rolePageableFilter = (RolePageableFilter) filter;
            reportOption.setPageNumber(rolePageableFilter.getPage());
            reportOption.setPageSize(rolePageableFilter.getSize());
            reportOption.setSortOptions(rolePageableFilter.getSort());
        }

        ReportCondition reportCondition = new ReportCondition();
        reportCondition.addEqualCondition("id", filter.getId());
        reportCondition.addLikeCondition("name", filter.getName());
        reportCondition.addEqualCondition("active", filter.getActive());
//        reportCondition.addEqualCondition("roleType", filter.getType());
        return new ReportFilter(reportCondition, reportOption);
    }
}
