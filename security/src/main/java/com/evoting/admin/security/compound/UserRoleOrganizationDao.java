//package com.evoting.security.compound;
//
//import com.evoting.database.security.UserRoleOrganizationEntity;
//import com.evoting.utility.db.provider.data.SpringDataBaseDao;
//import org.springframework.stereotype.Repository;
//
//import java.util.List;
//
//@Repository
//public interface UserRoleOrganizationDao extends SpringDataBaseDao<UserRoleOrganizationEntity, Integer> {
//    boolean existsByUserId(Integer userId);
//
//    UserRoleOrganizationEntity findByUserId(Integer userId);
//
//    List<UserRoleOrganizationEntity> findAllByOrganizationId(Integer organizationId);
//
//}
