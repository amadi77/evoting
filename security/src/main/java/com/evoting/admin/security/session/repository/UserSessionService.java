package com.evoting.admin.security.session.repository;

import com.evoting.entity.security.entity.UserEntity;
import com.evoting.entity.security.entity.UserSessionEntity;
import com.evoting.admin.security.role.model.dto.ClientInfo;
import com.evoting.admin.security.session.dto.UserSessionOut;
import com.evoting.admin.security.session.repository.dao.UserSessionDao;
import com.evoting.utility.repository.AbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class UserSessionService extends AbstractService<UserSessionEntity, UserSessionDao> {

    @Autowired
    public UserSessionService(UserSessionDao dao) {
        super(dao);
    }

    public UserSessionOut create(UserEntity entity, ClientInfo clientInfo) {
        UserSessionEntity userSessionEntity = new UserSessionEntity();
        userSessionEntity.setUserId(entity.getId());
        userSessionEntity.setAgent(clientInfo.getAgent());
        userSessionEntity.setIp(clientInfo.getMainIP());
        userSessionEntity.setCreated(LocalDateTime.now());
        userSessionEntity.setClientOs(clientInfo.getClientOS());

        createEntity(userSessionEntity);

        return new UserSessionOut(userSessionEntity);
    }

    public UserSessionEntity getEntityById(int id) {
        return getDao().getEntityById(id);
    }

    public void deleteByUserId(int userId) {
        getDao().deleteByUserId(userId);
    }
}
