package com.evoting.admin.security.role.model.dto;

import com.evoting.utility.model.object.SortOption;

import java.util.ArrayList;
import java.util.List;

public class RolePageableFilter extends RoleFilter {

    private Integer page;
    private Integer size;
    private List<SortOption> sort;

    public RolePageableFilter() {
        this.page = 1;
        this.size = 50;
        this.sort = new ArrayList<>();
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public List<SortOption> getSort() {
        return sort;
    }

    public void setSort(List<SortOption> sort) {
        this.sort = sort;
    }
}
