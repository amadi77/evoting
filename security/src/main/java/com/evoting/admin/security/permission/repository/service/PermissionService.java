package com.evoting.admin.security.permission.repository.service;

import com.evoting.entity.security.entity.SecurityPermissionEntity;
import com.evoting.admin.security.permission.model.dto.PermissionFilter;
import com.evoting.admin.security.permission.model.dto.PermissionIn;
import com.evoting.admin.security.permission.model.dto.PermissionOut;
import com.evoting.admin.security.permission.model.dto.PermissionPageableFilter;
import com.evoting.utility.model.object.ReportFilter;
import com.evoting.utility.model.object.SystemException;
import com.evoting.utility.model.object.report.ReportCondition;
import com.evoting.utility.model.object.report.ReportOption;
import com.evoting.utility.repository.AbstractFilterableService;
import com.evoting.utility.repository.Dao;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PermissionService extends AbstractFilterableService<SecurityPermissionEntity, PermissionFilter, Dao<SecurityPermissionEntity>> {

    private final ModelMapper modelMapper;

    @Autowired
    public PermissionService(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public List<SecurityPermissionEntity> getAllEntities(PermissionPageableFilter permissionPageableFilter, String[] include) {
        return getAllEntities(filter(permissionPageableFilter), include);
    }

    public int countEntity(PermissionFilter filter) {
        return countEntity(filter(filter));
    }

    @Override
    public SecurityPermissionEntity getEntityById(int id, String[] include) throws SystemException {
        return getEntityById(id, include);
    }

    public boolean deleteEntity(int id) {
        return deleteById(id);
    }

    public List<PermissionOut> getAll(PermissionPageableFilter filter, String[] include) {
        List<SecurityPermissionEntity> entities = getAllEntities(filter, include);
        if (entities != null) {
            return entities.stream().map(source -> modelMapper.map(source, PermissionOut.class))
                    .collect(Collectors.toList());
        } else {
            return new ArrayList<>();
        }
    }

    public PermissionOut getById(int id, String[] include) throws SystemException {
        return modelMapper.map(getEntityById(id, include), PermissionOut.class);
    }

    public PermissionOut create(PermissionIn model) {
        return modelMapper.map(createEntity(modelMapper.map(model, SecurityPermissionEntity.class)), PermissionOut.class);

    }

    public PermissionOut update(int id, PermissionIn model) throws SystemException {
        SecurityPermissionEntity permissionEntity = getEntityById(id, null);
        modelMapper.map(model, permissionEntity);
        updateEntity(permissionEntity);
        return modelMapper.map(permissionEntity, PermissionOut.class);
    }

    @Override
    public ReportFilter filter(PermissionFilter filter) {
        ReportOption reportOption = new ReportOption();
        if (filter instanceof PermissionPageableFilter) {
            PermissionPageableFilter permissionPageableFilter = (PermissionPageableFilter) filter;
            reportOption.setPageNumber(permissionPageableFilter.getPage());
            reportOption.setPageSize(permissionPageableFilter.getSize());
            reportOption.setSortOptions(permissionPageableFilter.getSort());
        }

        ReportCondition reportCondition = new ReportCondition();
        return new ReportFilter(reportCondition, reportOption);
    }
}
