package com.evoting.admin.security.bl;

import com.evoting.entity.security.entity.UserEntity;
import com.evoting.entity.security.entity.UserSessionEntity;
import com.evoting.admin.security.object.JwtClaim;
import com.evoting.admin.security.repository.UserDao;
import com.evoting.admin.security.session.repository.UserSessionService;
import com.evoting.utility.model.object.SystemError;
import com.evoting.utility.model.object.SystemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Exceptions error code range: 2051-2070
 */
@SuppressWarnings("ALL")
@Service
public class AvailableService {

    private final UserDao userDao;
    private final UserSessionService userSessionService;

    @Autowired
    public AvailableService(UserDao userDao, UserSessionService userSessionService) {
        this.userDao = userDao;
        this.userSessionService = userSessionService;
    }

    public UserEntity getUserByAuthentication(Map<String, Object> claims) throws SystemException {
        JwtClaim jwtClaim = JwtService.validateClaims(claims);
        UserEntity user = this.userDao.getById(jwtClaim.getUserId());
        if (user == null)
            throw new SystemException(SystemError.USER_NOT_ACTIVE, "user", 2055);
        if (!user.isActive())
            throw new SystemException(SystemError.USER_NOT_ACTIVE, "userId:" + user.getId(), 2056);

        return user;
    }
    public Integer getSessionIdByAuthentication(Map<String, Object> claims) throws SystemException {
        try {
            JwtClaim jwtClaim = JwtService.validateClaims(claims);
            UserEntity user = this.userDao.getById(jwtClaim.getUserId());
            if (user == null)
                throw new SystemException(SystemError.USER_NOT_FOUND, "user", 2051);
//            if (user.isSuspended())
//                throw new SystemException(SystemError.USER_NOT_ACTIVE, "userId:" + user.getId(), 2052);

            this.userSessionService.getEntityById(jwtClaim.getSessionId(), null);
            return jwtClaim.getSessionId();
        } catch (SystemException e) {
            throw new SystemException(SystemError.SESSION_EXPIRED, "", 2053);
        }
    }

    public UserEntity getConfirmedUserByAuthentication(Map<String, Object> claims) throws SystemException {
        JwtClaim jwtClaim = JwtService.validateClaims(claims);
        UserSessionEntity session = userSessionService.getEntityById(jwtClaim.getSessionId());
        if (session == null) {
            throw new SystemException(SystemError.USER_NOT_ACTIVE, "user", 2055);
        }
        if (!session.getUser().isActive()) {
            throw new SystemException(SystemError.USER_NOT_ACTIVE, "userId:" + session.getUserId(), 2056);
        }
//        if (user.get().getExpiration() != null) {
//            Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());
//            if (user.get().getExpiration().compareTo(currentTimestamp) < 0)
//                throw new SystemException(SystemError.ACCESS_DENIED, "ur access is expired", 123);
//        }
        return session.getUser();
    }

    public UserEntity getPublicUser(Map<String, Object> claims) throws SystemException {
        JwtClaim jwtClaim = JwtService.validateClaims(claims);
        UserEntity user = this.userDao.getById(jwtClaim.getUserId());
        if (user == null)
            throw new SystemException(SystemError.USER_NOT_ACTIVE, "user", 2055);
        return user;
    }

    /* ************************************************************************************************************** */

}
