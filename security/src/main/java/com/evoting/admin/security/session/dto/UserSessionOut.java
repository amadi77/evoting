package com.evoting.admin.security.session.dto;

import com.evoting.entity.security.entity.UserSessionEntity;
import com.evoting.utility.config.StaticApplicationContext;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.Hibernate;
import org.modelmapper.ModelMapper;

@Setter
@Getter
public class UserSessionOut extends UserSessionIn {
    private int id;
    @Setter(AccessLevel.PRIVATE)
    private UserInfo user;

    public UserSessionOut(UserSessionEntity entity) {
        if (entity != null) {
            ModelMapper modelMapper = StaticApplicationContext.getContext().getBean(ModelMapper.class);
            modelMapper.map(entity, this);
            if (Hibernate.isInitialized(entity.getUser())) {
                this.user = new UserInfo(entity.getUser());
            }
        }
    }
}
