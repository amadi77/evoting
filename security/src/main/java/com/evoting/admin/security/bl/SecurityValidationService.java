package com.evoting.admin.security.bl;

import com.evoting.utility.model.object.SystemError;
import com.evoting.utility.model.object.SystemException;
import org.springframework.stereotype.Service;

@Service
public class SecurityValidationService {

    public String validateAuthHeaderToken(String value) throws SystemException {
        // 7 characters for "Bearer " and 2 characters for two dots
        if (value == null || value.length() < 10)
            throw new SystemException(SystemError.INVALID_TOKEN_HEADER, "value", 2101);
        return value.replaceAll("Bearer ", "");
    }

    public String validatePublicHeaderToken(String value) {
        // 7 characters for "Bearer " and 2 characters for two dots
        if (value == null || value.length() < 10)
            return null;
        return value.replaceAll("Bearer ", "");
    }

}
