package com.evoting.admin.security.statics.enums;

/**
 * @author evoting on 5/18/19
 */
public enum  LocaleType {
    en(0),
    fa(1),
    ar(2);


    int value;

    LocaleType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

}
