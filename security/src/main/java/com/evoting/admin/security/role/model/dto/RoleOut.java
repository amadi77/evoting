package com.evoting.admin.security.role.model.dto;

import lombok.Data;

@Data
public class RoleOut {
    private Integer id;
    private String name;
}
