package com.evoting.admin.security.bl;

import com.evoting.entity.security.entity.UserEntity;
import com.evoting.admin.security.statics.constant.SecurityConstant;
import com.evoting.utility.model.object.SystemError;
import com.evoting.utility.model.object.SystemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

/**
 * Exceptions error code range: 2021-2050
 */

@Service
public class AccessService {

    private final HeaderService headerService;
    private final JwtService jwtService;

    @Autowired
    public AccessService(HeaderService headerService, JwtService jwtService) {
        this.headerService = headerService;
        this.jwtService = jwtService;
    }

    /* ****************************************************************************************************************** */

    public UserEntity getLoggedInUser(HttpServletRequest request) throws SystemException {
        if (request == null)
            throw new SystemException(SystemError.ILLEGAL_ARGUMENT, "com.evoting.request", 2021);
        Object object = request.getAttribute(SecurityConstant.REQUEST_EXTENDED_ATTRIBUTE);
        if (object == null)
            throw new SystemException(SystemError.ILLEGAL_ARGUMENT, "object", 2022);
        if (!(object instanceof UserEntity))
            throw new SystemException(SystemError.ILLEGAL_ARGUMENT, "object", 2023);
        return (UserEntity) request.getAttribute(SecurityConstant.REQUEST_EXTENDED_ATTRIBUTE);
    }

    public String getAuthenticatedToken(HttpServletRequest request) throws SystemException {
        if (request == null)
            throw new SystemException(SystemError.ILLEGAL_ARGUMENT, "com.evoting.request", 2025);

        return this.headerService.extractAuthTokenClient(request);
    }

    public UserEntity getAuthenticatedUserFromAccessToken(HttpServletRequest request) throws SystemException {
        if (request == null) {
            throw new SystemException(SystemError.ILLEGAL_ARGUMENT, "com.evoting.request", 2028);
        }

        String authToken = this.headerService.extractAuthTokenClient(request);
        return this.jwtService.extractConfirmedUserFromToken(authToken, SecurityConstant.ACCESS_TOKEN_SUBJECT);
    }

    public UserEntity getAuthenticatedUserFromRefreshToken(HttpServletRequest request) throws SystemException {
        if (request == null) {
            throw new SystemException(SystemError.ILLEGAL_ARGUMENT, "com.evoting.request", 2030);
        }

        String authToken = this.headerService.extractAuthTokenClient(request);
        return this.jwtService.extractConfirmedUserFromToken(authToken, SecurityConstant.REFRESH_TOKEN_SUBJECT);
    }


    public UserEntity getPublicUser(HttpServletRequest request) throws SystemException {
        if (request == null)
            throw new SystemException(SystemError.ILLEGAL_ARGUMENT, "com.evoting.request", 2033);

        UserEntity appUserEntity = null;
        try {
            String authToken = this.headerService.extractPublicTokenClient(request);
            if (authToken != null)
                appUserEntity = this.jwtService.extractPublicUserFromToken(authToken, SecurityConstant.ACCESS_TOKEN_SUBJECT);
        } catch (SystemException ignored) {
        }
        return appUserEntity;
    }
    public Integer getSessionIdFromAccessToken(HttpServletRequest request) throws SystemException {
        if (request == null)
            throw new SystemException(SystemError.ILLEGAL_ARGUMENT, "request", 2031);

        String authToken = this.headerService.extractAuthTokenClient(request);
        return this.jwtService.extractSessionIdFromAccessToken(authToken);
    }
    /* ****************************************************************************************************************** */

}
