package com.evoting.admin.security.bl;

import com.evoting.utility.bl.ValidationEngine;
import com.evoting.utility.config.ApplicationProperties;
import com.evoting.utility.model.object.Pair;
import com.evoting.utility.model.object.SystemError;
import com.evoting.utility.model.object.SystemException;
import com.evoting.entity.security.entity.UserEntity;
import com.evoting.entity.security.statics.ClientType;
import com.evoting.admin.security.object.JwtClaim;
import com.evoting.admin.security.statics.constant.SecurityConstant;
import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Exceptions error code range: 2081-2100
 */
@Service
public class JwtService {
    private static final String CLAIM_ISSUER = "iss";
    private static final String CLAIM_SUBJECT = "sub";
    private static final String CLAIM_EXPIRATION = "exp";
    private static final String CLAIM_USER_ID = "uid";
    private static final String CLAIM_SESSION_ID = "sid";
    private final AvailableService availableService;
    private final ApplicationProperties applicationProperties;

    @Autowired
    public JwtService(AvailableService availableService, ApplicationProperties applicationProperties) {
        this.availableService = availableService;
        this.applicationProperties = applicationProperties;
    }

    /* ****************************************************************************************************************** */

    /**
     * Create Access Refresh Tokens with User Context Data
     * <br />Possible {@link SystemException} in Function Call of {@link JwtService#buildToken(Map, String, Integer)}
     *
     * @param userId A {@link Integer} Instance Representing Serial Number of Client's Application
     * @return A {@link Pair} Instance Representing Access and Refresh Tokens in Json Wen Token Format
     */
    public Pair<String, String> create(Integer userId, Integer sessionId, ClientType clientType) throws SystemException {
        Map<String, Object> claims = new HashMap<>();
        claims.put(JwtService.CLAIM_USER_ID, userId);
        claims.put(JwtService.CLAIM_SESSION_ID, sessionId);

        Integer accessTokenExpirationTime = this.extractAccessTokenExpirationTime(clientType);
        Integer refreshTokenExpirationTime = this.extractRefreshTokenExpirationTime(clientType);
        String access = this.buildToken(claims, SecurityConstant.ACCESS_TOKEN_SUBJECT, accessTokenExpirationTime);
        String refresh = this.buildToken(claims, SecurityConstant.REFRESH_TOKEN_SUBJECT, refreshTokenExpirationTime);
        return new Pair<>(access, refresh);
    }

    public Pair<String, String> refresh(String token, ClientType clientType) throws SystemException {
        Map<String, Object> claims = this.extractClaims(token, SecurityConstant.REFRESH_TOKEN_SUBJECT);

        Integer accessTokenExpirationTime = this.extractAccessTokenExpirationTime(clientType);
        Integer refreshTokenExpirationTime = this.extractRefreshTokenExpirationTime(clientType);

        String access = this.buildToken(claims, SecurityConstant.ACCESS_TOKEN_SUBJECT, accessTokenExpirationTime);
        String refresh = this.buildToken(claims, SecurityConstant.REFRESH_TOKEN_SUBJECT, refreshTokenExpirationTime);
        return new Pair<>(access, refresh);
    }

    /* ****************************************************************************************************************** */

    public UserEntity extractPublicUserFromToken(String token, String tokenType) throws SystemException {
        Map<String, Object> claims = this.extractClaims(token, tokenType);
        return this.availableService.getPublicUser(claims);
    }

    public UserEntity extractConfirmedUserFromToken(String token, String tokenType) throws SystemException {
        Map<String, Object> claims = this.extractClaims(token, tokenType);
        return this.availableService.getConfirmedUserByAuthentication(claims);
    }

    public Integer extractSessionIdFromRefreshToken(String token) throws SystemException {
        Map<String, Object> claims = this.extractClaims(token, SecurityConstant.REFRESH_TOKEN_SUBJECT);
        return this.availableService.getSessionIdByAuthentication(claims);
    }

    public Integer extractSessionIdFromAccessToken(String token) throws SystemException {
        Map<String, Object> claims = this.extractClaims(token, SecurityConstant.ACCESS_TOKEN_SUBJECT);
        return this.availableService.getSessionIdByAuthentication(claims);
    }

    public Integer extractUserIdFromToken(String token) {
        String tokenSecret =  applicationProperties.getJwt().getTokenSecretKey();
        return Integer.valueOf(Jwts.parser()
                .setSigningKey(tokenSecret).parseClaimsJws(token.replace("Bearer ", "")).getBody().getSubject());
    }

    /* ****************************************************************************************************************** */

    private String buildToken(Map<String, Object> claims, String subject, Integer plusHours) throws SystemException {
        try {
            String tokenSecret = applicationProperties.getJwt().getTokenSecretKey();
            if (tokenSecret == null)
                throw new SystemException(SystemError.DATA_NOT_FOUND, "tokenSecretKey", 2082);

            Timestamp now = Timestamp.valueOf(LocalDateTime.now());
            Timestamp expiration = Timestamp.valueOf(LocalDateTime.now().plusHours(plusHours));
            return Jwts.builder()
                    .setHeaderParam("typ", "JWT")
                    .setClaims(claims)
                    .setIssuer(SecurityConstant.TOKEN_ISSUER)
                    .setSubject(subject)
                    .setExpiration(expiration)
                    .setIssuedAt(now)
                    .setId(UUID.randomUUID().toString())
                    .compressWith(CompressionCodecs.GZIP)
                    .signWith(SignatureAlgorithm.HS512, tokenSecret)
                    .compact();
        } catch (RuntimeException e) {
            throw new SystemException(SystemError.TOKEN_CREATION_FAILED, "claims:" + claims + ",subject:" + subject + ",plusHours:" + plusHours, 2083);
        }
    }

    private Map<String, Object> extractClaims(String token, String subject) throws SystemException {
        try {
            String secretKey = applicationProperties.getJwt().getTokenSecretKey();
            if (secretKey == null)
                throw new SystemException(SystemError.DATA_NOT_FOUND, "tokenSecretKey", 2085);
            return Jwts.parser()
                    .requireSubject(subject)
                    .setSigningKey(secretKey).parseClaimsJws(token).getBody();
        } catch (MissingClaimException | IncorrectClaimException e) {
            throw new SystemException(SystemError.TOKEN_VERIFICATION_INVALID_TYPE, "token:" + token + ",subject:" + subject, 2086);
        } catch (ExpiredJwtException e) {
            if (subject.equals(SecurityConstant.REFRESH_TOKEN_SUBJECT)) {
                throw new SystemException(SystemError.REFRESH_TOKEN_VERIFICATION_EXPIRED, "subject:" + subject, 2087);
            } else {
                throw new SystemException(SystemError.TOKEN_VERIFICATION_EXPIRED, "subject:" + subject, 2088);
            }
        } catch (RuntimeException e) {
            throw new SystemException(SystemError.TOKEN_VERIFICATION_FAILED, "token:" + token + ",subject:" + subject, 2089);
        }
    }

    public static JwtClaim validateClaims(Map<String, Object> claims) throws SystemException {
        Integer claimUserId;
        Integer claimSessionId;
        try {
            claimUserId = ValidationEngine.validateInteger(claims.get(JwtService.CLAIM_USER_ID).toString());
            claimSessionId = ValidationEngine.validateInteger(claims.get(JwtService.CLAIM_SESSION_ID).toString());
        } catch (SystemException e) {
            throw new SystemException(SystemError.TOKEN_VERIFICATION_FAILED, "claims:" + claims, 2090);
        } catch (NullPointerException | ClassCastException e) {
            throw new SystemException(SystemError.TOKEN_VERIFICATION_FAILED, "claims:" + claims, 2091);
        }
        return new JwtClaim(claimUserId,claimSessionId);
    }

    private Integer extractAccessTokenExpirationTime(ClientType clientType) throws SystemException {
        return 1000000;
//        if (clientType == ClientType.WEB) {
//            SystemVariableEntity browserAccessTokenExp = this.systemVariableService.getByCache(SystemVariable.ACCESS_TOKEN_BROWSER_EXPIRATION_HOURS.getValue());
//            if (browserAccessTokenExp == null)
//                throw new SystemException(SystemError.DATA_NOT_FOUND, "variableId:" + SystemVariable.ACCESS_TOKEN_BROWSER_EXPIRATION_HOURS.getValue(), 2092);
//            return ValidationEngine.validateInteger(browserAccessTokenExp.getValue());
//        } else {
//            SystemVariableEntity mobileAccessTokenExp = this.systemVariableService.getByCache(SystemVariable.ACCESS_TOKEN_MOBILE_EXPIRATION_HOURS.getValue());
//            if (mobileAccessTokenExp == null)
//                throw new SystemException(SystemError.DATA_NOT_FOUND, "variableId:" + SystemVariable.ACCESS_TOKEN_MOBILE_EXPIRATION_HOURS.getValue(), 2093);
//            return ValidationEngine.validateInteger(mobileAccessTokenExp.getValue());
//        }
    }

    private Integer extractRefreshTokenExpirationTime(ClientType clientType) throws SystemException {
//
        return 10000000;
    }
}


