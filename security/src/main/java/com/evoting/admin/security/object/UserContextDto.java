package com.evoting.admin.security.object;

import com.evoting.entity.security.entity.UserEntity;
import com.evoting.entity.security.entity.UserRoleEntity;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.Hibernate;

import java.util.List;
import java.util.stream.Collectors;

@Setter
@Getter
public class UserContextDto {
    private Integer id;
    private String fullName;
    private List<String> roles;

    public UserContextDto(UserEntity entity) {
        this.id = entity.getId();
        this.fullName = entity.getFullName();
        if (Hibernate.isInitialized(entity.getRoles())
                && entity.getRoles() != null
                && !entity.getRoles().isEmpty()) {
            this.roles = entity.getRoles().stream().map(UserRoleEntity::getName).collect(Collectors.toList());
        }
    }
}
