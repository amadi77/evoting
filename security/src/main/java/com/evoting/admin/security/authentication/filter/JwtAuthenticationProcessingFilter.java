package com.evoting.admin.security.authentication.filter;

import com.evoting.admin.security.bl.HeaderService;
import com.evoting.admin.security.bl.LogAnnotation;
import com.evoting.admin.security.object.JwtInputAuthentication;
import com.evoting.admin.security.statics.constant.SecurityConstant;
import com.evoting.utility.model.object.ErrorResult;
import com.evoting.utility.model.object.SystemException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.stereotype.Component;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Component
public class JwtAuthenticationProcessingFilter extends AbstractAuthenticationProcessingFilter {

    private static final Logger auditLogger = LogManager.getLogger("audit");

    private final HeaderService headerService;

    @Autowired
    public JwtAuthenticationProcessingFilter(JwtRequestMatcher jwtRequestMatcher, HeaderService headerService) {
        super(jwtRequestMatcher);
        this.headerService = headerService;
    }

    @Autowired
    public void init(AuthenticationManager authenticationManager) {
        super.setAuthenticationManager(authenticationManager);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {
        try {
            String authTokenClient = this.headerService.extractAuthTokenClient(request);
            JwtInputAuthentication possibleToken = new JwtInputAuthentication(authTokenClient);
            return getAuthenticationManager().authenticate(possibleToken);
        } catch (SystemException e) {
            throw new JwtAuthenticationException(e);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
                                            Authentication possibleUser) throws IOException, ServletException {
        request.setAttribute(SecurityConstant.REQUEST_EXTENDED_ATTRIBUTE, possibleUser.getPrincipal());
        SecurityContextHolder.getContext().setAuthentication(possibleUser);
        chain.doFilter(request, response);
    }

    @LogAnnotation
    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
                                              AuthenticationException exception) throws IOException {
        JwtAuthenticationException exp = (JwtAuthenticationException) exception;

        ErrorResult errorResult = new ErrorResult(exp.getSystemException(), response);

        SecurityContextHolder.clearContext();
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.getOutputStream().write(new ObjectMapper().writeValueAsBytes(errorResult));
    }
}
