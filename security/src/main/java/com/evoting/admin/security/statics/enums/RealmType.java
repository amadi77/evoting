package com.evoting.admin.security.statics.enums;

/**
 * @author evoting on 5/18/19
 */
public enum RealmType {

    STORE(0),
    WARE_HOUSE(1),
    INVENTORY(1);

    int value;

    RealmType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }}


