package com.evoting.admin.security.statics.constant;

public abstract class EndPoint {
    public final static String PUBLIC_MATCHER="/pub/**";
    public final static String SECURE_MATCHER="/sec/**";

    public final static String PUB_API ="/pub";
    public final static String SEC_API ="/sec";

    public final static String CRR ="/crr";
    public final static String AGENT ="/agent";
    public final static String BRANCH ="/branch";
    public final static String COMMON ="/common";
    public final static String ADMIN="/admin";



}
