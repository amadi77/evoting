package com.evoting.admin.security.config;

import com.evoting.admin.security.authentication.exception.JwtAuthenticationEntryPoint;
import com.evoting.admin.security.authentication.filter.IdentifiableAnonymousAuthenticationFilter;
import com.evoting.admin.security.authentication.filter.JwtAuthenticationProcessingFilter;
import com.evoting.admin.security.authentication.filter.JwtAuthenticationProvider;
import com.evoting.admin.security.authorization.exception.JwtAccessDeniedHandler;
import com.evoting.admin.security.bl.JwtService;
import com.evoting.utility.statics.constants.RestApi;
import com.google.common.collect.ImmutableList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private IdentifiableAnonymousAuthenticationFilter identifiableAnonymousAuthenticationFilter;
    @Autowired
    private JwtAuthenticationProcessingFilter jwtAuthenticationProcessingFilter;
    @Autowired
    private JwtAuthenticationProvider jwtAuthenticationProvider;
    @Autowired
    private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;
    @Autowired
    private JwtAccessDeniedHandler jwtAccessDeniedHandler;
    @Autowired
    private JwtService jwtService;

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(this.jwtAuthenticationProvider);
    }

   /* @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
//        configuration.setAllowedOrigins(Arrays.asList("*"));
        configuration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE", "OPTIONS", "PATCH"));
        configuration.setAllowCredentials(true);
        configuration.setAllowedHeaders(ImmutableList.of("Authorization", "Cache-Control", "Content-Type", "Content-Range", "uniqueId", "Range", "Access-Control-Expose-Headers"));
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }*/

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors();

        http.anonymous().authenticationFilter(identifiableAnonymousAuthenticationFilter);
        http.authorizeRequests().antMatchers(RestApi.MATCHER_PUBLIC).permitAll();
        http.authorizeRequests().antMatchers(RestApi.MATCHER_IDENTIFIED).authenticated();
        http.authorizeRequests().antMatchers(RestApi.MATCHER_ADMIN).authenticated();
//        http.authorizeRequests().antMatchers("/api/swagger-ui.html/*").permitAll();
//        http.authorizeRequests().antMatchers(EndPoint.MATCHER_IDENTIFIED).authenticated();

//        try {
//            List<SecurityAccessRule> accessRules = this.accessRuleService.fetchAllRules();
//            for (SecurityAccessRule rule : accessRules) {
//                http.authorizeRequests().antMatchers(rule.getHttpMethod(), rule.getUrl()).hasAnyRole(rule.getAccess().toArray(new String[0]));
//            }
//
//        } catch (SystemException ignored) {
//        }
        http.authorizeRequests().antMatchers("/").denyAll();

        this.jwtAuthenticationProcessingFilter.init(authenticationManagerBean());
        http.addFilterBefore(this.jwtAuthenticationProcessingFilter, UsernamePasswordAuthenticationFilter.class);

        http.exceptionHandling()
                .authenticationEntryPoint(this.jwtAuthenticationEntryPoint)
                .accessDeniedHandler(this.jwtAccessDeniedHandler);

        http.sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.csrf().disable();
    }
}
