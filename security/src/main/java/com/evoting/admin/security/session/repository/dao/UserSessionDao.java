package com.evoting.admin.security.session.repository.dao;

import com.evoting.entity.security.entity.UserSessionEntity;
import com.evoting.utility.repository.Dao;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class UserSessionDao extends Dao<UserSessionEntity> {

    public void deleteByUserId(int userId) {
        Query query = this.getEntityManager().createQuery("delete from UserSessionEntity where userId=:userId");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("userId", userId);
        updateHqlQuery(query, parameters);
    }

    public UserSessionEntity getEntityById(int id) {
        Query query = this.getEntityManager().createQuery("select entity from UserSessionEntity entity" +
                " join fetch entity.user where entity.id = :id");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("id", id);
        List<UserSessionEntity> sessions = queryHql(query, parameters);
        return sessions.isEmpty() ? null : sessions.get(0);
    }
}
