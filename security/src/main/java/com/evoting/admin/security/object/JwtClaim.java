package com.evoting.admin.security.object;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class JwtClaim {
    private Integer userId;
    private Integer sessionId;

    public JwtClaim(Integer userId, Integer sessionId) {
//    public JwtClaim(Integer userId) {
        this.userId = userId;
        this.sessionId = sessionId;
    }

}
