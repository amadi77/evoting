package com.evoting.admin.security.permission.model.dto;


import com.evoting.utility.repository.service.FilterBase;

public class PermissionFilter implements FilterBase {

    private Integer id;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
