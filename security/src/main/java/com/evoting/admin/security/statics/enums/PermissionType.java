package com.evoting.admin.security.statics.enums;

/**
 * @author evoting on 5/16/19
 */
public enum  PermissionType {


    NODE_HEAD(1),
    NODE_READ (10),
    NODE_CREATE(20),
    NODE_UPDATE (30),
    NODE_DELETE(40);

    int value;
     PermissionType(int value){
        this.value=value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }}
