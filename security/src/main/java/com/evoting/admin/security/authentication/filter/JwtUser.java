package com.evoting.admin.security.authentication.filter;

import com.evoting.entity.security.entity.UserEntity;
import com.evoting.admin.security.object.UserContextDto;
import com.evoting.utility.model.object.SystemError;
import com.evoting.utility.model.object.SystemException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class JwtUser {

    public static UserContextDto getAuthenticatedUser() throws SystemException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getPrincipal() instanceof UserEntity) {
            UserEntity userEntity = (UserEntity) authentication.getPrincipal();
            return new UserContextDto(userEntity);
        }
        throw new SystemException(SystemError.USER_NOT_FOUND, "کاربر پیدا نشد", 2233);
    }
}
