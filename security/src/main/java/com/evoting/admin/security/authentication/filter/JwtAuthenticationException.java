package com.evoting.admin.security.authentication.filter;

import com.evoting.utility.model.object.SystemException;
import org.springframework.security.core.AuthenticationException;


public class JwtAuthenticationException extends AuthenticationException {
    private final SystemException systemException;

    public JwtAuthenticationException(SystemException systemException) {
        super("", systemException);
        this.systemException = systemException;
    }

    public SystemException getSystemException() {
        return systemException;
    }
}
