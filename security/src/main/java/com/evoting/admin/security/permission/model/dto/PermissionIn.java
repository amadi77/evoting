package com.evoting.admin.security.permission.model.dto;

import lombok.Data;

@Data
public class PermissionIn {
    private String name;
}
