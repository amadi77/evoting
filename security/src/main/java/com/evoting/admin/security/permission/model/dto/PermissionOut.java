package com.evoting.admin.security.permission.model.dto;

public class PermissionOut extends PermissionIn{

    private String permission;

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }
}
