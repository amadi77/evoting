package com.evoting.admin.security.authorization.exception;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.evoting.utility.model.object.ErrorResult;
import com.evoting.utility.model.object.SystemError;
import com.evoting.utility.model.object.SystemException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
public class JwtAccessDeniedHandler implements AccessDeniedHandler {

    private static final Logger auditLogger = LogManager.getLogger("audit");
    private static final Logger devLogger = LogManager.getLogger(JwtAccessDeniedHandler.class);

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response,
                       AccessDeniedException exp) throws IOException {
//        try {
//            UserEntity user = JwtUser.getAuthenticatedUser();
//            user.isAdmin();
//        } catch (SystemException e) {
//        }

        List<ErrorResult> results = new ArrayList<>();
        ErrorResult errorResult = new ErrorResult(new SystemException(SystemError.ACCESS_DENIED, "user", 2011), response);
        results.add(errorResult);

        SecurityContextHolder.clearContext();
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.getOutputStream().write(new ObjectMapper().writeValueAsBytes(results));
    }
}
