package com.evoting.admin.security.authentication.filter;

import com.evoting.utility.statics.constants.RestApi;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class JwtRequestMatcher implements RequestMatcher {
    private RequestMatcher skipMatcher;
    private RequestMatcher secureMatcher;
    private RequestMatcher secureMatcher2;
//    private RequestMatcher identifiedMatcher;

    public JwtRequestMatcher() {
        this.skipMatcher = new AntPathRequestMatcher(RestApi.MATCHER_PUBLIC);
        this.secureMatcher = new AntPathRequestMatcher(RestApi.MATCHER_IDENTIFIED);
        this.secureMatcher2 = new AntPathRequestMatcher(RestApi.MATCHER_ADMIN);
//        this.identifiedMatcher = new AntPathRequestMatcher(EndPoint.MATCHER_IDENTIFIED);
    }

    @Override
    public boolean matches(HttpServletRequest request) {
        return !skipMatcher.matches(request) && (secureMatcher.matches(request) || secureMatcher2.matches(request));
    }
}
