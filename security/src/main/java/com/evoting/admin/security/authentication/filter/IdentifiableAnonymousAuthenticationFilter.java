package com.evoting.admin.security.authentication.filter;

import com.evoting.entity.security.entity.UserEntity;
import com.evoting.admin.security.bl.HeaderService;
import com.evoting.admin.security.bl.JwtService;
import com.evoting.admin.security.object.JwtOutputAuthentication;
import com.evoting.admin.security.statics.constant.SecurityConstant;
import com.evoting.utility.model.object.SystemError;
import com.evoting.utility.model.object.SystemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AnonymousAuthenticationFilter;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class IdentifiableAnonymousAuthenticationFilter extends AnonymousAuthenticationFilter {

    @Autowired
    private JwtService jwtService;
    @Autowired
    private HeaderService headerService;

    public static final String KEY_IDENTIFIABLE_ANONYMOUS_AUTHENTICATION_FILTER
            = "Key.IdentifiableAnonymousAuthenticationFilter";

    public IdentifiableAnonymousAuthenticationFilter() {
        this(KEY_IDENTIFIABLE_ANONYMOUS_AUTHENTICATION_FILTER);
    }

    public IdentifiableAnonymousAuthenticationFilter(String key) {
        super(key);
    }

    @Override
    protected Authentication createAuthentication(HttpServletRequest request) {
        try {
            if (request == null)
                throw new SystemException(SystemError.ILLEGAL_ARGUMENT, "com.evoting.request", 2028);

            String authToken = this.headerService.extractPublicTokenClient(request);
            if (authToken == null)
                return new JwtOutputAuthentication(null);
            UserEntity possibleUser = this.jwtService.extractPublicUserFromToken(authToken, SecurityConstant.ACCESS_TOKEN_SUBJECT);
            return new JwtOutputAuthentication(possibleUser);
        } catch (SystemException e) {
        }
        return null;
    }
}
