package com.evoting.admin.security.role.statics;

public abstract class RoleResApi {
    public final static  String ROLES="/roles";
    public final static String ROLE_ID="/roles/{id}";
    public final static String ROLES_COUNT="/roles/count";
}
