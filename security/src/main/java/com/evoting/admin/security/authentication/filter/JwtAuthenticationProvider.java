package com.evoting.admin.security.authentication.filter;

import com.evoting.entity.security.entity.UserEntity;
import com.evoting.admin.security.bl.JwtService;
import com.evoting.admin.security.object.JwtInputAuthentication;
import com.evoting.admin.security.object.JwtOutputAuthentication;
import com.evoting.admin.security.statics.constant.SecurityConstant;
import com.evoting.utility.model.object.SystemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

@Component
public class JwtAuthenticationProvider implements AuthenticationProvider {

    private final JwtService jwtService;

    @Autowired
    public JwtAuthenticationProvider(JwtService jwtService) {
        this.jwtService = jwtService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        try {
            UserEntity possibleUser = this.jwtService.extractConfirmedUserFromToken((String) authentication.getPrincipal(), SecurityConstant.ACCESS_TOKEN_SUBJECT);
            return new JwtOutputAuthentication(possibleUser);
        } catch (SystemException e) {
            throw new JwtAuthenticationException(e);
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return JwtInputAuthentication.class.equals(authentication);
    }
}
