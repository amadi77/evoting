package com.evoting.admin.security.object;

import com.evoting.entity.security.statics.ClientType;

public class AuthRequestHeader {
    private String token;
    private Long clientSerialNumber;
    private ClientType clientType;

    public AuthRequestHeader(String token, Long clientSerialNumber, ClientType clientType) {
        this.token = token;
        this.clientSerialNumber = clientSerialNumber;
        this.clientType = clientType;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Long getClientSerialNumber() {
        return clientSerialNumber;
    }

    public void setClientSerialNumber(Long clientSerialNumber) {
        this.clientSerialNumber = clientSerialNumber;
    }

    public ClientType getClientType() {
        return clientType;
    }

    public void setClientType(ClientType clientType) {
        this.clientType = clientType;
    }
}
