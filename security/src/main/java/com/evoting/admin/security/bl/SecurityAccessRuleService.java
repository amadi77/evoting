//package com.evoting.security.bl;
//
//import com.evoting.security.object.SecurityAccessRule;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.util.List;
//import java.util.Objects;
//import java.util.stream.Collectors;
//
//@Service
//public class SecurityAccessRuleService {
//
//    private final BaseDaoImplementation baseDaoImplementation;
//
//    @Autowired
//    public SecurityAccessRuleService(BaseDaoImplementation baseDaoImplementation) {
//        this.baseDaoImplementation = baseDaoImplementation;
//    }
//
//    public List<SecurityAccessRule> fetchAllRules() {
//        return baseDaoImplementation.list(EndpointEntity.class).stream().filter(Objects::nonNull).map(SecurityAccessRule::new).collect(Collectors.toList());
//    }
//
//}
