package com.evoting.admin.security.object;

import com.evoting.entity.security.entity.UserEntity;
import com.evoting.entity.security.entity.UserRoleEntity;
import com.evoting.admin.security.statics.constant.SecurityConstant;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.HashSet;

public class JwtOutputAuthentication implements Authentication {

    private final UserEntity user;

    public JwtOutputAuthentication(UserEntity user) {
        this.user = user;
    }

    @Override
    public Object getPrincipal() {
        return this.user;
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Collection<SimpleGrantedAuthority> rules = new HashSet<>();
        for (UserRoleEntity role : user.getRoles()) {
            rules.add(new SimpleGrantedAuthority(SecurityConstant.SIMPLE_GRANTED_AUTHORITY_PREFIX + role.getId()));
        }
        return rules;
    }

    @Override
    public Object getDetails() {
        return null;
    }

    @Override
    public boolean isAuthenticated() {
        return true;
    }

    @Override
    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {

    }

    @Override
    public String getName() {
        return null;
    }
}
