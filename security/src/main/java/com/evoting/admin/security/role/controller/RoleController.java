package com.evoting.admin.security.role.controller;

import com.evoting.admin.security.role.model.dto.RoleFilter;
import com.evoting.admin.security.role.model.dto.RoleIn;
import com.evoting.admin.security.role.model.dto.RoleOut;
import com.evoting.admin.security.role.model.dto.RolePageableFilter;
import com.evoting.admin.security.role.repository.service.RoleService;
import com.evoting.admin.security.role.statics.RoleResApi;
import com.evoting.utility.model.object.SystemException;
import com.evoting.utility.statics.constants.RestApi;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(RestApi.REST_ADMIN)
@Validated
@Api("Role API")
public class RoleController {

    private final RoleService service;

    @Autowired
    public RoleController(RoleService service) {
        this.service = service;
    }

    @ApiOperation(value = "count entity", response = Integer.class)
    @RequestMapping(value = RoleResApi.ROLES_COUNT, method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> count(@Valid RoleFilter filter, BindingResult bindingResult, HttpServletRequest httpServletRequest) {
        return new ResponseEntity<>(service.count(filter), HttpStatus.OK);
    }

    @ApiOperation(value = "get entity by id", response = RoleOut.class)
    @RequestMapping(value = RoleResApi.ROLE_ID, method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> getOne(@PathVariable("id") Integer id, @RequestParam(value = "include", required = false) String[] include, HttpServletRequest httpServletRequest) throws SystemException {
        return new ResponseEntity<>(service.getById(id, include), HttpStatus.OK);
    }

    @ApiOperation(value = "get all entities with pageable filters and include", response = List.class)
    @RequestMapping(value = RoleResApi.ROLES, method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> getAll(@Valid RolePageableFilter pageableFilter, BindingResult bindingResult, @RequestParam(value = "include", required = false) String[] include, HttpServletRequest httpServletRequest, HttpServletResponse response) throws SystemException {
        response.setHeader("range", String.valueOf(service.count(pageableFilter)));
        return new ResponseEntity<>(service.getAll(pageableFilter, include), HttpStatus.OK);
    }

    @ApiOperation(value = "create entity and save in to db", response = RoleOut.class)
    @RequestMapping(value = RoleResApi.ROLES, method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> create(@Valid @RequestBody RoleIn model, BindingResult bindingResult, HttpServletRequest httpServletRequest) {
        return new ResponseEntity<>(service.create(model), HttpStatus.OK);
    }

    @ApiOperation(value = "update entity by id", response = RoleOut.class)
    @RequestMapping(value = RoleResApi.ROLE_ID, method = RequestMethod.PUT, produces = "application/json")
    public ResponseEntity<?> update(@PathVariable("id") Integer id, @Valid @RequestBody RoleIn model, BindingResult bindingResult, HttpServletRequest httpServletRequest) throws SystemException {
        return new ResponseEntity<>(service.update(id, model), HttpStatus.OK);
    }

    @ApiOperation(value = "delete entity by id", response = void.class)
    @RequestMapping(value = RoleResApi.ROLE_ID, method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity<?> delete(@PathVariable("id") Integer id, HttpServletRequest httpServletRequest) throws SystemException {
        return new ResponseEntity<>(service.delete(id), HttpStatus.OK);
    }

}
