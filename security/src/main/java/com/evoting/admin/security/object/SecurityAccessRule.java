package com.evoting.admin.security.object;

import com.evoting.entity.security.entity.SecurityRestEntity;
import com.evoting.admin.security.statics.constant.SecurityConstant;
import org.springframework.http.HttpMethod;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class SecurityAccessRule {
    private String url;
    private HttpMethod httpMethod;
    private List<String> access;

    public SecurityAccessRule(SecurityRestEntity entity) {
        this.url = entity.getUrl();
        this.httpMethod = entity.getMethod();
        this.access = entity.getPermissions().stream().filter(Objects::nonNull)
                .map(x -> SecurityConstant.ACCESS_RULE_PREFIX + x.getId())
                .collect(Collectors.toList());
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public HttpMethod getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(HttpMethod httpMethod) {
        this.httpMethod = httpMethod;
    }

    public List<String> getAccess() {
        return access;
    }

    public void setAccess(List<String> access) {
        this.access = access;
    }
}
