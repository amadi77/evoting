package com.evoting.admin.security.endpoint.model.dto;

import com.evoting.utility.model.object.SortOption;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class EndpointPageableFilter extends EndpointFilter {
    private Integer page;
    private Integer size;
    private List<SortOption> sort;

    public EndpointPageableFilter() {
        this.page = 1;
        this.size = 50;
        this.sort = new ArrayList<>();
    }
}
