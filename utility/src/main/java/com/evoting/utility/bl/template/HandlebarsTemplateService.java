package com.evoting.utility.bl.template;

import com.evoting.utility.model.object.SystemError;
import com.evoting.utility.model.object.SystemException;
import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * Exceptions error code range: 1051-1070
 */

@Service
@Primary
public class HandlebarsTemplateService implements ITemplateService {
    private final Handlebars handlebars;

    @Autowired
    public HandlebarsTemplateService(Handlebars handlebars) {
        this.handlebars = handlebars;
    }

    @Override
    public String render(String templateName, Object model) throws SystemException {
        try {
            Template template = handlebars.compile(templateName);
            return template.apply(model);
        } catch (IOException e) {
            throw new SystemException(SystemError.IO_EXCEPTION, "file:" + templateName, 1051);
        }
    }

}
