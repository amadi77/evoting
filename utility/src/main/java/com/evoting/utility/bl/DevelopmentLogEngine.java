package com.evoting.utility.bl;

import com.evoting.utility.model.object.SystemError;

public abstract class DevelopmentLogEngine {

    private DevelopmentLogEngine() {
    }

    public static String createConfigLogPattern(String methodName, String description) {
        return methodName + description + " ";
    }

    public static String createCriticalLogPattern(String className, String methodName, String argument, SystemError error, Integer errorCode) {
        return className + " " + methodName + " " + argument + " " + error + " " + errorCode;
    }

    public static String createConstraintViolationException(String className, String methodName, String message) {
        return className + " " + methodName + " " + message;
    }

    public static String createServerErrorLogPattern(String className, String methodName, String message) {
        return className + " " + methodName + " " + message + " ";
    }

    public static String createAuthLogPattern(String methodName, String type) {
        return "method:" + methodName + " type:" + type + " ";
    }
}
