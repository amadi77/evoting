package com.evoting.utility.bl.template;

import com.evoting.utility.model.object.SystemException;
import org.springframework.stereotype.Service;

@Service
public interface ITemplateService {
    String render(String templateName, Object model) throws SystemException;
}
