package com.evoting.utility.bl.otp;

import com.evoting.utility.model.object.SystemException;

public interface IOtpService {

    String generateOtp(String key) throws SystemException;

    boolean validateOtp(String key, String inputCode);

}
