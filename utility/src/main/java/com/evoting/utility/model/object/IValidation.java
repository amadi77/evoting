package com.evoting.utility.model.object;

public interface IValidation {
    void validate() throws SystemException;
}
