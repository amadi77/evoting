package com.evoting.utility.model.object;

import lombok.Getter;

import javax.servlet.http.HttpServletResponse;

@Getter
public class ErrorResult {
    private SystemError code;
    private Integer status;
    private Integer errorCode;
    private String description;

    public ErrorResult(SystemError error, Integer errorCode, String description) {
        this.code = error;
        this.errorCode = errorCode;
        this.description = description;
        this.status = error.getValue();
    }

    public ErrorResult(SystemException exception) {
        this.code = exception.getError();
        this.errorCode = exception.getErrorCode();
        this.description = exception.getArgument().toString();
        this.status = exception.getError().getValue();
    }

    public ErrorResult(SystemException exception, HttpServletResponse response) {
        this.code = exception.getError();
        this.status = exception.getError().getValue();
        this.errorCode = exception.getErrorCode();
        response.setStatus(exception.getError().getValue());
    }

}
