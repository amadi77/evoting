package com.evoting.utility.model.object.report;

public enum AggregationType {
    SUM,
    AVG,
    MAX,
    MIN
}
