package com.evoting.utility.model.object;

import com.evoting.utility.model.object.report.ReportCondition;
import com.evoting.utility.model.object.report.ReportOption;

import java.io.Serializable;

/**
 * @author imax
 * @author Mohammad Yasin Sadeghi
 */
public class ReportFilter implements Serializable {
    private ReportCondition conditions;
    private ReportOption options;

    public ReportFilter(ReportCondition conditions, ReportOption options) {
        this.conditions = conditions;
        this.options = options;
    }

    public ReportFilter() {
    }

    public ReportCondition getConditions() {
        return conditions;
    }

    public void setConditions(ReportCondition conditions) {
        this.conditions = conditions;
    }

    public ReportOption getOptions() {
        return options;
    }

    public void setOptions(ReportOption options) {
        this.options = options;
    }
}
