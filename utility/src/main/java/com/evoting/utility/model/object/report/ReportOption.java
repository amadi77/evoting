package com.evoting.utility.model.object.report;


import com.evoting.utility.model.object.SortOption;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ReportOption {
    private boolean distinct = false;
    private Integer pageSize;
    private Integer pageNumber; // start from 1
    private List<SortOption> sortOptions;

    public ReportOption() {
        this.pageSize = 10;
        this.pageNumber = 1;
    }

    public ReportOption(Integer pageSize, Integer pageNumber, List<SortOption> sortOptions) {
        this.pageSize = pageSize;
        this.pageNumber = pageNumber;
        this.sortOptions = sortOptions;
    }

}
