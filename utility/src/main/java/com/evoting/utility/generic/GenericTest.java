//package com.evote.utility.generic;
//
//import org.mockito.MockingDetails;
//import org.mockito.invocation.InvocationOnMock;
//
//import java.beans.BeanInfo;
//import java.beans.IntrospectionException;
//import java.beans.Introspector;
//import java.beans.PropertyDescriptor;
//import java.lang.reflect.Method;
//import java.util.Arrays;
//import java.util.Set;
//import java.util.stream.Collectors;
//
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.mockito.Mockito.mockingDetails;
//
///**
// * @author : ahmad on 04.02.20 - 09:18
// **/
//public class GenericTest {
//    private GenericTest() {
//    }
//
//    /**
//     * Assertion For Class That All Getter Has Been Called
//     */
//    public static void assertAllGettersCalled(Object spy, Class<?> clazz) throws IntrospectionException {
//        BeanInfo beanInfo = Introspector.getBeanInfo(clazz);
//        /** Get All Getter Of Class {clazz}*/
//        Set<Method> setOfDescriptors = Arrays.stream(beanInfo.getPropertyDescriptors())
//                .map(PropertyDescriptor::getReadMethod)
//                .filter(p -> !p.getName().contains("getClass"))
//                .collect(Collectors.toSet());
//        MockingDetails details = mockingDetails(spy);
//        /** Check Called Getter Of Class {clazz}*/
//        Set<Method> setOfTestedMethods = details.getInvocations()
//                .stream()
//                .map(InvocationOnMock::getMethod)
//                .collect(Collectors.toSet());
//        setOfDescriptors.removeAll(setOfTestedMethods);
//        assertThat(setOfDescriptors).isEmpty();
//    }
//}
