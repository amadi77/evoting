package com.evoting.utility.generic;

import java.util.Collection;

/**
 * @author imax on 12/12/19
 */
public class GenericUtil {

    private GenericUtil() {

    }

    public static boolean checkListSizeAndNullable(Collection<?>... t) {
        for (Collection<?> ts : t) {
            if (ts != null && !ts.isEmpty())
                return false;
        }
        return true;
    }

}
