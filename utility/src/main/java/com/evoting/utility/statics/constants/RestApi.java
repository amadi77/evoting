package com.evoting.utility.statics.constants;

public abstract class RestApi {
    public static final String REST_IDENTIFIED = "/idn";
    public static final String REST_PUBLIC = "/pub";

    public static final String REST_ADMIN = "/admin";

    public static final String MATCHER_ADMIN = "/admin/**";
    public static final String MATCHER_IDENTIFIED = "/idn/**";
    public static final String MATCHER_PUBLIC = "/pub/**";

}
