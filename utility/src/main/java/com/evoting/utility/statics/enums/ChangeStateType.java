package com.evoting.utility.statics.enums;

public enum ChangeStateType {
    ENABLE,
    DISABLE,
    DELETE
}
