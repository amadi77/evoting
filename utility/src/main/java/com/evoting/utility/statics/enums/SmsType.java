package com.evoting.utility.statics.enums;

public enum SmsType {
    SERVICE,
    ADVERTISING;
}
