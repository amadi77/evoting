package com.evoting.utility.statics.enums;

public enum TimePeriodType {
    DAILY,
    WEEKLY,
    MONTHLY,
    YEARLY
}
