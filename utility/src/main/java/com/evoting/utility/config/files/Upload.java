package com.evoting.utility.config.files;

import lombok.Data;

@Data
public class Upload {
    private String path;
    private String url;
}
