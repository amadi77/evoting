package com.evoting.utility.config.files;

import lombok.Data;

@Data
public class Jwt {
    private String tokenSecretKey;
    private long expirationTime;
    private long refreshTime;
}
