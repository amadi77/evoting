package com.evoting.utility.config;

import com.evoting.utility.config.files.EmailConfig;
import com.evoting.utility.config.files.Jwt;
import com.evoting.utility.config.files.Upload;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
public class ApplicationProperties {
    private Jwt jwt;
    private Upload upload;
    private EmailConfig emailConfig;
}
