package com.evoting.utility.config.files;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class EmailConfig {
    private String host;
    private String port;
    private String username;
    private String password;
    private String protocol;
    private boolean auth;
    private boolean starttls;
}
