package com.evoting.utility.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;

@Configuration
public class PropertiesInitializer {
    @Scope("prototype")
    @Bean
    public ApplicationProperties applicationProperties() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(new ClassPathResource(
                "application-properties.json").getInputStream(), ApplicationProperties.class);
    }
}
