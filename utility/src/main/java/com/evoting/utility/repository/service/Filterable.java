package com.evoting.utility.repository.service;

import com.evoting.utility.model.object.ReportFilter;

/**
 * @author Mohammad Yasin Sadeghi
 */
public interface Filterable<T extends FilterBase> {

    ReportFilter filter(T filter);
}
