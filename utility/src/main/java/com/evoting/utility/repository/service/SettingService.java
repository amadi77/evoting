//package com.evoting.utility.repository.service;
//
//import com.evoting.utility.config.property.ApplicationProperties;
//import com.evoting.utility.config.property.identity.AddressOptions;
//import com.evoting.utility.config.property.identity.IdentitySettings;
//import com.evoting.utility.model.dto.SettingDto;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//@Service
//public class SettingService {
//    private final ApplicationProperties applicationProperties;
//
//    @Autowired
//    public SettingService(ApplicationProperties applicationProperties) {
//        this.applicationProperties = applicationProperties;
//    }
//
//    public SettingDto getApplicationSetting(String[] include) {
//        SettingDto settingDto = new SettingDto();
//        if (include == null) {
//            return settingDto;
//        }
//        for (String settingInclude : include) {
//            switch (settingInclude) {
//                case "signIn":
//                    if (settingDto.getIdentitySettings() == null) {
//                        settingDto.setIdentitySettings(new IdentitySettings());
//                    }
//                    settingDto.getIdentitySettings().setSignIn(applicationProperties.getIdentitySettings().getSignIn());
//                    break;
//                case "profile":
//                    if (settingDto.getIdentitySettings() == null) {
//                        settingDto.setIdentitySettings(new IdentitySettings());
//                    }
//                    settingDto.getIdentitySettings().setProfile(applicationProperties.getIdentitySettings().getProfile());
//                    settingDto.getIdentitySettings().getProfile().validateDetails();
//                    break;
//                case "registration":
//                    if (settingDto.getIdentitySettings() == null) {
//                        settingDto.setIdentitySettings(new IdentitySettings());
//                    }
//                    settingDto.getIdentitySettings().setRegistration(applicationProperties.getIdentitySettings().getRegistration());
//                    settingDto.getIdentitySettings().getRegistration().validateDetails();
//                    break;
//                case "password":
//                    if (settingDto.getIdentitySettings() == null) {
//                        settingDto.setIdentitySettings(new IdentitySettings());
//                    }
//                    settingDto.getIdentitySettings().setPassword(applicationProperties.getIdentitySettings().getPassword());
//                    break;
//                case "address":
//                    if (settingDto.getAddressOptions() == null) {
//                        settingDto.setAddressOptions(new AddressOptions());
//                    }
//                    settingDto.setAddressOptions(applicationProperties.getAddressOptions());
//                    break;
//                default:
//                    break;
//            }
//        }
//        return settingDto;
//    }
//}
