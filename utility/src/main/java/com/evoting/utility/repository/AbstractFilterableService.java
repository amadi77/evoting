package com.evoting.utility.repository;

import com.evoting.utility.repository.service.FilterBase;
import com.evoting.utility.repository.service.Filterable;

/**
 * @author Mohammad Yasin Sadeghi
 */
public abstract class AbstractFilterableService<T, F extends FilterBase, D extends Dao<T>>
        extends AbstractService<T, D>
        implements Filterable<F> {

    public AbstractFilterableService() {
    }

    public AbstractFilterableService(D dao) {
        super(dao);
    }
}
