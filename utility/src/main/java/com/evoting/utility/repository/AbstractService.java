package com.evoting.utility.repository;

import com.evoting.utility.config.StaticApplicationContext;
import com.evoting.utility.model.object.ReportFilter;
import com.evoting.utility.model.object.SystemError;
import com.evoting.utility.model.object.SystemException;
import com.evoting.utility.repository.service.Service;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Exceptions error code range: 1201-1250
 *
 * @author imax on 5/25/19
 * @author Mohammad Yasin Sadeghi
 */
public abstract class AbstractService<T, D extends Dao<T>> extends Parameterized<T> implements Service<T> {

    @Autowired
    private ReportDao<T> reportDao;

    private D dao;

    public AbstractService() {
        dao = (D) StaticApplicationContext.getContext().getBean("dao");
    }

    public AbstractService(D dao) {
        this.dao = dao;
    }

    @Override
    public List<T> getAllEntities(ReportFilter reportFilter, String[] include) {
        return reportDao.reportEntityWithIncludeJoin(reportFilter, include, getClazz());
    }

    @Override
    public List<T> getAllEntitiesWithoutMaxLimit(ReportFilter reportFilter, String[] include) {
        return reportDao.reportEntityWithIncludeJoinWithoutMaxLimit(reportFilter, include, getClazz());
    }

    @Override
    public List<T> getAllEntitiesWithFilter(ReportFilter reportFilter, String[] include) {
        return reportDao.reportEntityWithIncludeJoin(reportFilter, include, getClazz());
    }

    @Override
    public List<T> getAllEntitiesJoin(ReportFilter reportFilter, String[] include) {
        return reportDao.reportEntityWithIncludeJoin(reportFilter, include, getClazz());
    }

    @Override
    public int countEntity(ReportFilter reportFilter) {
        return reportDao.countEntity(reportFilter, getClazz());
    }

    @Override
    public T getEntityById(int id, String[] includes) throws SystemException {
        return getEntityById((Object) id, includes);
    }

    @Override
    public T getEntityById(Object id, String[] includes) throws SystemException {
        Map<String, Object> map = new HashMap<>();
        map.put("id", id);
        T result = dao.byAndConditions(map, includes, getClazz());
        if (result == null) {
            throw new SystemException(SystemError.DATA_NOT_FOUND, "id:" + id, 1201);
        }
        return result;
    }

    @Override
    public T createEntity(T entity) {
        return dao.saveEntity(entity);
    }

    @Override
    public T updateEntity(T entity) {
        return dao.updateEntity(entity);
    }

    @Override
    public T mergeEntity(T entity) {
        return dao.mergeEntity(entity);
    }

    @Override
    public void detach(T entity) {
        dao.detach(entity);
    }

    @Override
    public void flush() {
        dao.flush();
    }

    @Override
    public void createOrUpdateEntity(T entity) {
        dao.saveOrUpdate(entity);
    }

    @Override
    public void saveOrUpdateEntityCollection(Collection<T> collection) {
        dao.saveOrUpdateCollection(collection);
    }

    @Override
    public void saveOrUpdateEntity(T entity) {
        dao.saveOrUpdate(entity);
    }

    @Override
    public boolean deleteById(int id) {
        dao.deleteById(id, getClazz());
        return true;
    }

    @Override
    public boolean deleteById(Object id) {
        dao.deleteById(id, getClazz());
        return true;
    }

    @Override
    public void deleteEntity(T entity) {
        dao.delete(entity);
    }

    @Override
    public List<T> getEntitiesByIds(List<Integer> ids) {
        return dao.listByIds(ids, getClazz());
    }

    public <T> List<T> getEntitiesWithSqlFilter(String select, String groupBy, ReportFilter reportFilter, Map<String, Object> parameters, Class<T> cls) {
        return dao.reportEntityWithSqlQuery(select, groupBy, reportFilter, parameters, cls);
    }

    protected D getDao() {
        return dao;
    }
}
