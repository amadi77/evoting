package com.evoting.utility.repository.sp;

import com.evoting.utility.model.object.SystemError;
import com.evoting.utility.model.object.SystemException;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.StoredProcedureQuery;

/**
 * The General Stored Procedure Class,
 * Containing Global Database Operation
 *
 * @author Bijan Ghahremani
 * @version 1.0
 * @since 2016-09-22
 */
@Repository
@Transactional
public class ResetSP {

    @PersistenceContext
    private EntityManager entityManager;

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public Session getSession() {
        Session session = getEntityManager().unwrap(Session.class);
        return session;
    }

    public void superAdminPermission() throws SystemException {
        StoredProcedureQuery storedProcedure = getSession().createStoredProcedureQuery("super_admin_permission");
        try {
            storedProcedure.execute();
        } catch (PersistenceException e) {
            throw new SystemException(SystemError.STORED_PROCEDURE_FAILED, "session", 1550);
        }
    }
}
