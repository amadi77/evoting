package com.evoting.utility.repository.service;

import com.evoting.utility.model.object.ReportFilter;
import com.evoting.utility.model.object.SystemException;

import java.util.Collection;
import java.util.List;

/**
 * Todo T must be restricted to baseEntity.
 *
 * @author Mohammad Yasin Sadeghi
 */
public interface Service<T> {

    List<T> getAllEntities(ReportFilter reportFilter, String[] include);

    List<T> getAllEntitiesWithoutMaxLimit(ReportFilter reportFilter, String[] include);

    List<T> getAllEntitiesWithFilter(ReportFilter reportFilter, String[] include);

    List<T> getAllEntitiesJoin(ReportFilter reportFilter, String[] include);

    int countEntity(ReportFilter reportFilter);

    T getEntityById(int id, String[] includes) throws SystemException;

    T getEntityById(Object id, String[] includes) throws SystemException;

    T createEntity(T entity);

    T updateEntity(T entity);

    T mergeEntity(T entity);

    void detach(T entity);

    void flush();

    void createOrUpdateEntity(T entity);

    void saveOrUpdateEntityCollection(Collection<T> collection);

    void saveOrUpdateEntity(T entity);

    boolean deleteById(int id);

    boolean deleteById(Object id);

    void deleteEntity(T entity);

    List<T> getEntitiesByIds(List<Integer> ids);
}
