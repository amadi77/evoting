FROM openjdk:8-jdk-alpine
LABEL maintainer="amadi"
VOLUME /app
USER root
RUN mkdir -p /opt/voting/files/
ARG JAR_FILE=application/target/application-1.0.0.jar
ADD ${JAR_FILE} /app/demo.jar
ENTRYPOINT ["java","-jar","/app/demo.jar"]
EXPOSE 4044

#COPY application/target/application-1.0.0.jar /app/Application-1.0.0.jar
#
#ENTRYPOINT cd /app && java -jar Application-1.0.0.jar


