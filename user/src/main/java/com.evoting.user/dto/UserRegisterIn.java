package com.evoting.user.dto;

import com.evoting.utility.model.object.IValidation;
import com.evoting.utility.model.object.SystemError;
import com.evoting.utility.model.object.SystemException;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserRegisterIn extends UserRegisterEditIn implements IValidation {
    private String password;
    private String repeatPassword;

    @Override
    public void validate() throws SystemException {
        if (!password.equals(repeatPassword)){
            throw new SystemException(SystemError.VIOLATION_ERROR,"password and repeat not match",1901);
        }
    }
}
