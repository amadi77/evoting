package com.evoting.user.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class SmsIn {
    private String phoneNumber;
    private String massage;
    private String receiver;
}
