package com.evoting.user.dto;

import com.evoting.entity.security.entity.UserEntity;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserRegisterOut {
    private String email;
    private String phoneNumber;
    private String username;

    public UserRegisterOut(UserEntity entity) {
        this.email = entity.getEmail();
        this.phoneNumber = entity.getPhoneNumber();
        this.username = entity.getUsername();
    }
}
