package com.evoting.user.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserRegisterEditOut extends UserRegisterEditIn{
    private Integer id;
}
