package com.evoting.user.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ChangePasswordFirst {
    private String newPassword;
    private String confirmPassword;
}
