package com.evoting.user.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class LoginPasswordChangeIn extends ChangePasswordFirst {
    private String code;
    private String username;
}
