package com.evoting.user.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ChangePasswordIn extends ChangePasswordFirst {
    private String oldPassword;
}
