package com.evoting.user.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class LoginWithResetPassword {
    private String username;
    private String sendPassword;
    private String newPassword;
    private String repeatPassword;
}
