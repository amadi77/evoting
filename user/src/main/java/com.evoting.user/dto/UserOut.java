package com.evoting.user.dto;

import com.evoting.entity.security.entity.SecurityPermissionEntity;
import com.evoting.entity.security.entity.UserEntity;
import com.evoting.entity.security.entity.UserRoleEntity;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.Hibernate;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Setter
@Getter
public class UserOut {
    private Integer id;
    private String fullName;
    private boolean isAdmin;
    private List<String> permissions;

    public UserOut(UserEntity entity) {
        if (entity != null) {
            this.id = entity.getId();
            this.fullName = entity.getFullName();
            this.isAdmin = entity.isAdmin();
            this.permissions = new ArrayList<>();
            if (Hibernate.isInitialized(entity.getRoles())) {
                entity.getRoles().stream().map(UserRoleEntity::getPermissions).collect(Collectors.toList())
                        .stream()
                        .map(s -> permissions.addAll(s.stream().map(SecurityPermissionEntity::getName)
                                .collect(Collectors.toList())));
            }
        }
    }
}
