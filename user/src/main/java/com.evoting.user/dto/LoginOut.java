package com.evoting.user.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class LoginOut {
    private String token;
    private Integer id;
    private String fullName;
    private List<String> permissions = new ArrayList<>();
    private Boolean passwordChange;
    private Boolean isAdmin;

}
