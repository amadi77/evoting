package com.evoting.user.dto;

import com.evoting.entity.core.statics.Gender;
import com.evoting.utility.bl.NormalizeEngine;
import com.evoting.utility.bl.ValidationEngine;
import com.evoting.utility.model.object.IValidation;
import com.evoting.utility.model.object.SystemException;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Setter
@Getter
public class UserRegisterEditIn implements IValidation {
    private String firstName;
    private String lastName;
    @NotNull
    private String email;
    private String phoneNumber;
    private String username;
    private Gender gender;

    public void setEmail(String email) throws SystemException {
        ValidationEngine.validateEmail(email);
        this.email = email;
    }

    public void setPhoneNumber(String phoneNumber) throws SystemException {
        this.phoneNumber = NormalizeEngine.getNormalizedPhoneNumber(phoneNumber);
    }

    @Override
    public void validate() throws SystemException {
        setEmail(email);
        if (phoneNumber != null) {
            setPhoneNumber(phoneNumber);
        }
    }
}
