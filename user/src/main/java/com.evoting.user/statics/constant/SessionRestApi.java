package com.evoting.user.statics.constant;

import lombok.Getter;

@Getter
public abstract class SessionRestApi {
    public static final String ACCOUNT = "/account";

    //SecurityRoleController
    public static final String SECURITY_ID_SESSION_COUNT = "/users/sessions/count";
    public static final String SECURITY_ID_SESSIONS = "/users/sessions";
    public static final String SECURITY_SESSION_BY_ID = "/users/sessions/{id}";
    public static final String SECURITY_SESSION_MY_COUNT = "/users/sessions/my/count";
    public static final String SECURITY_SESSION_MY = "/users/sessions/my";
    public static final String SECURITY_SESSION_MY_ID = "/users/sessions/my/{id}";

    public static final String LOGIN = ACCOUNT+  "/login";
    public static final String LOGOUT = ACCOUNT+  "/logout";
//    public static final String LOGIN_RESET = ACCOUNT+  "/login/reset-password";
    public static final String REGISTER = ACCOUNT+  "/register";
    public static final String PROFILE = ACCOUNT+  "/profile";
    public static final String CHANGE_PASSWORD= ACCOUNT+ "/change-password";
    public static final String RESET_PASSWORD= ACCOUNT+ "/reset-password";
    public static final String CHANGE_PASSWORD_FIRST= ACCOUNT+ "/change-password/first";
    public static final String FORGOT_PASSWORD= ACCOUNT+ "/forgot-password";


}
