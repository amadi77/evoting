package com.evoting.user.controller;


import com.evoting.admin.security.authentication.filter.JwtUser;
import com.evoting.admin.security.object.UserContextDto;
import com.evoting.user.dto.UserOut;
import com.evoting.user.repository.UserService;
import com.evoting.utility.model.object.SystemException;
import com.evoting.utility.statics.constants.RestApi;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@Api(tags = "User Controller")
@RestController
@Validated
public class UserController {
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @ApiOperation(value = "get user by id", response = UserOut.class)
    @GetMapping(path = RestApi.REST_IDENTIFIED + "/users", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserOut> login(@RequestParam(required = false) String[] include, HttpServletRequest servletRequest) throws SystemException {
        UserContextDto user = JwtUser.getAuthenticatedUser();
        return new ResponseEntity<>(userService.getById(user.getId(), include), HttpStatus.OK);
    }

}
