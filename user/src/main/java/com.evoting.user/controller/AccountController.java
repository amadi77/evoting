package com.evoting.user.controller;

import com.evoting.admin.security.bl.AccessService;
import com.evoting.admin.security.role.model.dto.ClientInfo;
import com.evoting.entity.security.entity.UserEntity;
import com.evoting.entity.security.statics.ClientType;
import com.evoting.user.dto.*;
import com.evoting.user.repository.UserService;
import com.evoting.user.statics.constant.SessionRestApi;
import com.evoting.utility.model.object.SystemException;
import com.evoting.utility.statics.constants.RestApi;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Api(tags = "Account Controller")
@RestController
@Validated
public class AccountController {
    private final AccessService accessService;
    private final UserService userService;

    @Autowired
    public AccountController(AccessService accessService, UserService userService) {
        this.accessService = accessService;
        this.userService = userService;
    }

    @ApiOperation(value = "login user", response = LoginOut.class)
    @PostMapping(path = RestApi.REST_PUBLIC + SessionRestApi.LOGIN, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LoginOut> login(@RequestBody LoginIn loginIn, BindingResult bindingResult, HttpServletRequest servletRequest) throws SystemException {
        return new ResponseEntity<>(userService.login(loginIn, ClientType.WEB, new ClientInfo(servletRequest)), HttpStatus.OK);
    }

    @ApiOperation(value = "login user", response = LoginOut.class)
    @PostMapping(path = RestApi.REST_IDENTIFIED + SessionRestApi.LOGOUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public void logout(HttpServletRequest servletRequest) throws SystemException {
        Integer id = accessService.getSessionIdFromAccessToken(servletRequest);
        userService.logout(id);
    }
/*
    @ApiOperation(value = "login user", response = LoginOut.class)
    @PostMapping(path = RestApi.REST_PUBLIC + SessionRestApi.LOGIN_RESET, produces = MediaType.APPLICATION_JSON_VALUE)
    public void loginWithResetPassword(@RequestBody LoginWithResetPassword loginIn, BindingResult bindingResult, HttpServletRequest servletRequest) throws SystemException {
        userService.loginWithResetPassword(loginIn);
    }*/

    @ApiOperation(value = "register user", response = UserRegisterOut.class)
    @PostMapping(path = RestApi.REST_PUBLIC + SessionRestApi.REGISTER, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserRegisterOut> register(@Valid @RequestBody UserRegisterIn registerIn, BindingResult bindingResult, HttpServletRequest servletRequest) throws SystemException {
        return new ResponseEntity<>(userService.register(registerIn), HttpStatus.OK);
    }

    @ApiOperation(value = "update user")
    @PutMapping(path = RestApi.REST_IDENTIFIED + SessionRestApi.PROFILE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void updateProfile(@Valid @RequestBody UserRegisterEditIn registerIn, BindingResult bindingResult, HttpServletRequest servletRequest) throws SystemException {
        userService.changeProfile(registerIn);
    }

    @ApiOperation(value = "get user")
    @GetMapping(path = RestApi.REST_IDENTIFIED + SessionRestApi.PROFILE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserRegisterEditOut> getProfile(HttpServletRequest servletRequest) throws SystemException {
        return new ResponseEntity<>(userService.getProfile(), HttpStatus.OK);
    }

    @ApiOperation(value = "change password", response = ChangePasswordOut.class)
    @PostMapping(path = RestApi.REST_IDENTIFIED + SessionRestApi.CHANGE_PASSWORD, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ChangePasswordOut> changePassword(@RequestBody ChangePasswordIn passwordIn, BindingResult bindingResult, HttpServletRequest servletRequest) throws SystemException {
        UserEntity userEntity = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return new ResponseEntity<>(userService.changePassword(passwordIn, userEntity.getId()), HttpStatus.OK);
    }

    @ApiOperation(value = "change password", response = ChangePasswordOut.class)
    @PostMapping(path = RestApi.REST_PUBLIC + SessionRestApi.RESET_PASSWORD, produces = MediaType.APPLICATION_JSON_VALUE)
    public void resetPassword(@RequestBody LoginPasswordChangeIn passwordIn, BindingResult bindingResult, HttpServletRequest servletRequest) throws SystemException {
        userService.changePasswordAfterResetPassword(passwordIn);
    }

    @ApiOperation(value = "change password first", response = ChangePasswordOut.class)
    @PostMapping(path = RestApi.REST_IDENTIFIED + SessionRestApi.CHANGE_PASSWORD_FIRST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ChangePasswordOut> changePasswordFirst(ChangePasswordFirst passwordFirst, BindingResult bindingResult, HttpServletRequest servletRequest) throws SystemException {
        UserEntity userEntity = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return new ResponseEntity<>(userService.changePasswordFirst(passwordFirst, userEntity.getId()), HttpStatus.OK);
    }

    @ApiOperation(value = "forgot password", response = ForgotPasswordOut.class)
    @PostMapping(path = RestApi.REST_PUBLIC + SessionRestApi.FORGOT_PASSWORD, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ForgotPasswordOut> forgotPassword(@RequestBody ForgotPasswordIn forgotPasswordIn, BindingResult bindingResult, HttpServletRequest servletRequest) throws SystemException {
        return new ResponseEntity<>(userService.forgotPassword(forgotPasswordIn), HttpStatus.OK);
    }
}
