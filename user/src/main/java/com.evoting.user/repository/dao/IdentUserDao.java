package com.evoting.user.repository.dao;

import com.evoting.entity.security.entity.UserEntity;
import com.evoting.utility.bl.NormalizeEngine;
import com.evoting.utility.model.object.SystemException;
import com.evoting.utility.repository.Dao;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class IdentUserDao extends Dao<UserEntity> {

    public UserEntity getEntityWithUsername(String username) {
        Query query = this.getEntityManager().createQuery("select user from UserEntity user " +
                " left join fetch user.roles roles " +
                " left join fetch roles.permissions " +
                " where user.username=:username or user.email = :username or user.phoneNumber=:phone ");
        Map<String, Object> objectMap = new HashMap<>();
        objectMap.put("username", username);
        try {
            objectMap.put("phone", NormalizeEngine.getNormalizedPhoneNumber(username));
        } catch (SystemException ignored) {
            objectMap.put("phone", username);
        }

        List<UserEntity> entities = queryHql(query, objectMap);
        return entities.isEmpty() ? null : entities.get(0);
    }

    public UserEntity getEntityWithPhoneNumberOrEmail(String phoneNumber, String email) {
        Query query = this.getEntityManager().createQuery("select user from UserEntity user where user.phoneNumber=:phoneNumber or user.email=:email");
        Map<String, Object> objectMap = new HashMap<>();
        objectMap.put("phoneNumber", phoneNumber);
        objectMap.put("email", email);

        List<UserEntity> entities = queryHql(query, objectMap);
        return entities.isEmpty() ? null : entities.get(0);
    }
    public UserEntity getEntityWithPhoneNumberOrEmail(String username) {
        Query query = this.getEntityManager().createQuery("select user from UserEntity user where user.phoneNumber=:phone or user.email=:email or user.username=:username");
        Map<String, Object> objectMap = new HashMap<>();
        try {
            objectMap.put("phone", NormalizeEngine.getNormalizedPhoneNumber(username));
        } catch (SystemException ignored) {
            objectMap.put("phone", username);
        }
        objectMap.put("email", username);
        objectMap.put("username", username);

        List<UserEntity> entities = queryHql(query, objectMap);
        return entities.isEmpty() ? null : entities.get(0);
    }
}
