package com.evoting.user.repository;

import com.evoting.admin.masseging.bl.NotificationService;
import com.evoting.admin.masseging.gmail.dto.GmailIn;
import com.evoting.admin.security.authentication.filter.JwtUser;
import com.evoting.admin.security.bl.JwtService;
import com.evoting.admin.security.object.UserContextDto;
import com.evoting.admin.security.role.model.dto.ClientInfo;
import com.evoting.admin.security.session.dto.UserSessionOut;
import com.evoting.admin.security.session.repository.UserSessionService;
import com.evoting.entity.security.entity.SecurityPermissionEntity;
import com.evoting.entity.security.entity.UserEntity;
import com.evoting.entity.security.entity.UserRoleEntity;
import com.evoting.entity.security.statics.ClientType;
import com.evoting.user.dto.*;
import com.evoting.user.repository.dao.IdentUserDao;
import com.evoting.utility.bl.HashService;
import com.evoting.utility.model.object.Pair;
import com.evoting.utility.model.object.SystemError;
import com.evoting.utility.model.object.SystemException;
import com.evoting.utility.repository.AbstractService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Random;
import java.util.stream.Collectors;

@Service
public class UserService extends AbstractService<UserEntity, IdentUserDao> {
    private final JwtService jwtService;
    private final NotificationService notificationService;
    private final HashService hashService;
    private final UserSessionService userSessionService;
    private final ModelMapper modelMapper;

    public UserService(IdentUserDao identUserDao, JwtService jwtService, NotificationService notificationService, HashService hashService, UserSessionService userSessionService, ModelMapper modelMapper) {
        super(identUserDao);
        this.jwtService = jwtService;
        this.notificationService = notificationService;
        this.hashService = hashService;
        this.userSessionService = userSessionService;
        this.modelMapper = modelMapper;
    }

    public UserOut getById(int id, String[] include) throws SystemException {
        return new UserOut(getEntityById(id, include));
    }

    public LoginOut login(LoginIn loginIn, ClientType clientType, ClientInfo clientInfo) throws SystemException {
        UserEntity entity = getDao().getEntityWithUsername(loginIn.getUsername());
        if (entity == null) {
            throw new SystemException(SystemError.DATA_NOT_FOUND, "رمز عبور یا نام کاربری اشتباه است", 1101);
        }
        String hashedPassword = hashService.hash(loginIn.getPassword());
        if (hashedPassword.equals(entity.getHashedPassword())) {
            LoginOut loginOut = new LoginOut();
            loginOut.setId(entity.getId());
            loginOut.setFullName(entity.getFullName());
            loginOut.setIsAdmin(entity.isAdmin());
            loginOut.setPasswordChange(entity.isRequirePasswordChange());

            UserSessionOut userSessionOut = userSessionService.create(entity, clientInfo);
            Pair<String, String> tokens = jwtService.create(entity.getId(), userSessionOut.getId(), clientType);
            loginOut.setToken(tokens.getFirst());
            entity.getRoles().stream().map(UserRoleEntity::getPermissions)
                    .map(item -> loginOut.getPermissions()
                            .addAll(item.stream()
                                    .map(SecurityPermissionEntity::getName)
                                    .collect(Collectors.toList())));
            return loginOut;
        } else {
            throw new SystemException(SystemError.DATA_NOT_FOUND, "رمز عبور یا نام کاربری اشتباه است", 1102);
        }
    }

    public void logout(int sessionId) throws SystemException {
        userSessionService.deleteById(sessionId);
    }

    @Transactional(rollbackOn = Exception.class)
    public LoginOut loginWithResetPassword(LoginIn loginIn, ClientType clientType, ClientInfo clientInfo) throws SystemException {
        UserEntity entity = getDao().getEntityWithUsername(loginIn.getUsername());
        if (entity == null) {
            throw new SystemException(SystemError.DATA_NOT_FOUND, "رمز عبور یا نام کاربری اشتباه است", 1101);
        }
        if (entity.getResetPassword() == null) {
            throw new SystemException(SystemError.ILLEGAL_ARGUMENT, "خطایی رخ داده است", 1111);
        }
        String resetPassword = hashService.hash(loginIn.getPassword());
        if (resetPassword.equals(entity.getResetPassword())) {
            LoginOut loginOut = new LoginOut();
            loginOut.setId(entity.getId());
            loginOut.setFullName(entity.getFullName());
            loginOut.setIsAdmin(entity.isAdmin());
            loginOut.setPasswordChange(entity.isRequirePasswordChange());

            UserSessionOut userSessionOut = userSessionService.create(entity, clientInfo);
            Pair<String, String> tokens = jwtService.create(entity.getId(), userSessionOut.getId(), clientType);
            loginOut.setToken(tokens.getFirst());
            entity.getRoles().stream().map(UserRoleEntity::getPermissions)
                    .map(item -> loginOut.getPermissions()
                            .addAll(item.stream()
                                    .map(SecurityPermissionEntity::getName)
                                    .collect(Collectors.toList())));

            entity.setResetPassword(null);
            entity.setHashedPassword(null);
            updateEntity(entity);
            return loginOut;
        } else {
            throw new SystemException(SystemError.DATA_NOT_FOUND, "رمز عبور یا نام کاربری اشتباه است", 1102);
        }
    }

    public void loginWithResetPassword(LoginWithResetPassword loginIn) throws SystemException {
        UserEntity entity = getDao().getEntityWithUsername(loginIn.getUsername());
        if (entity == null) {
            throw new SystemException(SystemError.DATA_NOT_FOUND, "رمز عبور یا نام کاربری اشتباه است", 1101);
        }
        if (entity.getResetPassword() == null) {
            throw new SystemException(SystemError.ILLEGAL_ARGUMENT, "خطایی رخ داده است", 1111);
        }
        if (!loginIn.getNewPassword().equals(loginIn.getRepeatPassword())) {
            throw new SystemException(SystemError.ILLEGAL_ARGUMENT, "خطایی رخ داده است", 1112);
        }
        String resetPassword = hashService.hash(loginIn.getSendPassword());
        if (resetPassword.equals(entity.getResetPassword())) {
            /*LoginOut loginOut = new LoginOut();
            loginOut.setId(entity.getId());
            loginOut.setFullName(entity.getFullName());
            loginOut.setIsAdmin(entity.isAdmin());
            loginOut.setPasswordChange(entity.isRequirePasswordChange());

            UserSessionOut userSessionOut = userSessionService.create(entity, clientInfo);
            Pair<String, String> tokens = jwtService.create(entity.getId(), userSessionOut.getId(), clientType);
            loginOut.setToken(tokens.getFirst());
            entity.getRoles().stream().map(UserRoleEntity::getPermissions)
                    .map(item -> loginOut.getPermissions()
                            .addAll(item.stream()
                                    .map(SecurityPermissionEntity::getName)
                                    .collect(Collectors.toList())));*/

            entity.setResetPassword(null);
            entity.setHashedPassword(hashService.hash(loginIn.getNewPassword()));
            updateEntity(entity);
//            return loginOut;
        } else {
            throw new SystemException(SystemError.DATA_NOT_FOUND, "رمز عبور یا نام کاربری اشتباه است", 1102);
        }
    }


    public ForgotPasswordOut forgotPassword(ForgotPasswordIn forgotPasswordIn) throws SystemException {
        UserEntity entity = getDao().getEntityWithPhoneNumberOrEmail(forgotPasswordIn.getUsername());
        if (entity == null) {
            throw new SystemException(SystemError.DATA_NOT_FOUND, "کاربر وجود ندارد", 1102);
        }
        if (entity.getLock() != null && entity.getLock().isAfter(LocalDateTime.now())) {
            throw new SystemException(SystemError.DATA_NOT_FOUND, "امکان اجرای این درخواست در حال حاضر وجود ندارد", 1113);
        }
        Random random = new Random();
        String newPassword = random.nextInt(LocalDateTime.now().getNano()) + "";
        StringBuilder message = new StringBuilder();
        message.append("رمز عبور شما با موفقیت تغییر کرد\n")
                .append("رمز عبور جدید شما:\n")
                .append(newPassword)
                .append("\n\n این ایمیل به صورت اتوماتیک ارسال شده است و شما همچنان میتوانید با رمز عبور قبلی خود وارد شوید.");


        entity.setResetPassword(hashService.hash(newPassword));
        LocalDateTime now = LocalDateTime.now();
        now = now.plusMinutes(5);
        entity.setSendPasswordLock(LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), now.getHour(), now.getMinute()));
        entity.setRequirePasswordChange(true);
        updateEntity(entity);
        if (entity.getEmail() != null) {
            GmailIn gmailIn = new GmailIn();
            gmailIn.setEmail(entity.getEmail());
            gmailIn.setMessage(message.toString());
            gmailIn.setSubject("رمز عبور جدید");
            notificationService.sendEmail(gmailIn);
//            mailSender.sendEmail(gmailIn);
        } else if (entity.getPhoneNumber() != null) {
            SmsIn smsIn = new SmsIn();
            smsIn.setMassage(message.toString());
            smsIn.setPhoneNumber(entity.getPhoneNumber());
            //todo implement sms sender
        }
        return new ForgotPasswordOut();
    }

    public ChangePasswordOut changePassword(ChangePasswordIn changePasswordIn, Integer userId) throws SystemException {
        UserEntity entity = getEntityById(userId, null);
        if (entity.getHashedPassword().equals(hashService.hash(changePasswordIn.getOldPassword()))) {
            if (changePasswordIn.getNewPassword().equals(changePasswordIn.getConfirmPassword())) {
                entity.setHashedPassword(hashService.hash(changePasswordIn.getNewPassword()));
                entity.setRequirePasswordChange(false);
                updateEntity(entity);
                return new ChangePasswordOut();
            }
            throw new SystemException(SystemError.ILLEGAL_REQUEST, "رمز عبور و تکرار رمز عبور برابر نیستند", 1103);
        }
        throw new SystemException(SystemError.ILLEGAL_REQUEST, "پسورد قبلی صحیح نیست", 1104);
    }

    public void loginAfterForgotPassword(LoginPasswordChangeIn loginPasswordChangeIn) throws SystemException {
        if (!loginPasswordChangeIn.getNewPassword().equals(loginPasswordChangeIn.getConfirmPassword())) {
            throw new SystemException(SystemError.ILLEGAL_REQUEST, "رمز عبور و تکرار رمز عبور برابر نیستند", 1103);
        }
        UserEntity entity = getDao().getEntityWithPhoneNumberOrEmail(loginPasswordChangeIn.getUsername());
        String hashCode = hashService.hash(loginPasswordChangeIn.getCode());
        if (entity.getResetPassword() != null && entity.getResetPassword().equals(hashCode)) {
            entity.setResetPassword(null);
            entity.setHashedPassword(hashService.hash(loginPasswordChangeIn.getNewPassword()));
            updateEntity(entity);
            return;
        }
        throw new SystemException(SystemError.ILLEGAL_REQUEST, "خطایی رخ داده است", 1103);
    }

    public ChangePasswordOut changePasswordFirst(ChangePasswordFirst passwordFirst, Integer userId) throws SystemException {
        UserEntity entity = getEntityById(userId, null);
        if (passwordFirst.getNewPassword().equals(passwordFirst.getConfirmPassword())) {
            entity.setHashedPassword(hashService.hash(passwordFirst.getNewPassword()));
            entity.setRequirePasswordChange(false);
            updateEntity(entity);
            return new ChangePasswordOut();
        }
        throw new SystemException(SystemError.ILLEGAL_REQUEST, "رمز عبور و تکرار رمز عبور برابر نیستند", 1103);

    }

    public ChangePasswordOut changePasswordAfterResetPassword(LoginPasswordChangeIn loginPasswordChangeIn) throws SystemException {
        if (!loginPasswordChangeIn.getNewPassword().equals(loginPasswordChangeIn.getConfirmPassword())) {
            throw new SystemException(SystemError.ILLEGAL_REQUEST, "رمز عبور و تکرار رمز عبور برابر نیستند", 1103);
        }
        UserEntity entity = getDao().getEntityWithPhoneNumberOrEmail(loginPasswordChangeIn.getUsername());
        String hashCode = hashService.hash(loginPasswordChangeIn.getCode());
        if (!hashCode.equals(entity.getResetPassword())) {
            throw new SystemException(SystemError.ILLEGAL_REQUEST, "رمز عبور یا نام کاربری اشتباه است", 1102);
        }
        entity.setHashedPassword(hashService.hash(loginPasswordChangeIn.getNewPassword()));
        entity.setRequirePasswordChange(false);
        entity.setLock(null);
        updateEntity(entity);
        return new ChangePasswordOut();

    }

    public UserEntity getByMobileOrEmail(String mobile, String email) {
        return getDao().getEntityWithPhoneNumberOrEmail(mobile, email);
    }

    public UserRegisterOut register(UserRegisterIn userRegisterIn) throws SystemException {
        UserEntity entity = modelMapper.map(userRegisterIn, UserEntity.class);
        entity.setActive(true);
        entity.setSendPasswordLock(null);
        entity.setLock(null);
        entity.setFullName();
        entity.setRequirePasswordChange(false);
        entity.setHashedPassword(hashService.hash(userRegisterIn.getPassword()));
        createEntity(entity);
        return new UserRegisterOut(entity);
    }

    public void changeProfile(UserRegisterEditIn userRegisterEditIn) throws SystemException {
        userRegisterEditIn.validate();
        UserContextDto user = JwtUser.getAuthenticatedUser();
        UserEntity entity = getEntityById(user.getId(), null);
        entity.setFullName();
        modelMapper.map(userRegisterEditIn, entity);
        updateEntity(entity);
    }

    public UserRegisterEditOut getProfile() throws SystemException {
        UserContextDto user = JwtUser.getAuthenticatedUser();
        UserEntity userEntity = getEntityById(user.getId(), null);
        return modelMapper.map(userEntity, UserRegisterEditOut.class);
    }

    public boolean hasPassword() throws SystemException {
        UserContextDto user = JwtUser.getAuthenticatedUser();
        UserEntity entity = getEntityById(user.getId(), null);
        return entity.getHashedPassword() == null;
    }
}
