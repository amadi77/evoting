//package com.evoting.masseging.gmail.repository;
//
//import com.evoting.masseging.gmail.dto.GmailIn;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.mail.SimpleMailMessage;
//import org.springframework.mail.javamail.JavaMailSender;
//import org.springframework.stereotype.Service;
//
//@Service("MailSender")
//public class MailSender {
//
//    private final JavaMailSender javaMailSender;
//
//    @Autowired
//    public MailSender(JavaMailSender javaMailSender) {
//        this.javaMailSender = javaMailSender;
//    }
//
//    public void sendEmail(GmailIn gmailIn) {
//        SimpleMailMessage msg = new SimpleMailMessage();
//        msg.setTo(gmailIn.getEmail());
//        msg.setSubject(gmailIn.getSubject());
//        msg.setText(gmailIn.getMessage());
//        javaMailSender.send(msg);
//    }
//}
