package com.evoting.admin.masseging.bl;

import org.springframework.stereotype.Service;

import javax.mail.MessagingException;

@Service
public interface IEmailService {
    void sendEmail(String text, String subject, String from, String to) throws MessagingException;
}
