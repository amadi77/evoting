//package com.evoting.masseging.bl;
//
//import com.evoting.utility.config.files.EmailConfig;
//import org.springframework.context.annotation.Bean;
//import org.springframework.mail.javamail.JavaMailSender;
//import org.springframework.mail.javamail.JavaMailSenderImpl;
//import org.springframework.stereotype.Component;
//
//import java.util.Properties;
//
//@Component
//public class EmailSenderConfig {
///*
//
//
//    @Bean
//    public JavaMailSender javaMailService() {
//        JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
//
//        javaMailSender.setHost("smtp.gmail.com");
//        javaMailSender.setPort(587);
//
//        javaMailSender.setJavaMailProperties(getMailProperties());
//        javaMailSender.setUsername("voting.voting@gmail.com");
//        javaMailSender.setPassword("123voting@123");
//
//        return javaMailSender;
//    }
//
//    private Properties getMailProperties() {
//        Properties properties = new Properties();
//        properties.setProperty("mail.transport.protocol", "smtp");
//        properties.setProperty("mail.smtp.auth", "true");
//        properties.setProperty("mail.smtp.starttls.enable", "true");
//        properties.setProperty("mail.debug", "true");
//        properties.setProperty("mail.smtp.ssl.enable", "true");
//        properties.setProperty("mail.test-connection", "true");
//        return properties;
//    }
//*/
//
//    public JavaMailSender getJavaMailSender(EmailConfig emailConfig) {
//        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
//        mailSender.setHost(emailConfig.getHost());
//        mailSender.setPort(Integer.parseInt(emailConfig.getPort()));
//        mailSender.setUsername(emailConfig.getUsername());
//        mailSender.setPassword(emailConfig.getPassword());
//        Properties props = mailSender.getJavaMailProperties();
//        props.put("mail.transport.protocol", emailConfig.getProtocol());
//        props.put("mail.smtp.auth", emailConfig.isAuth());
//        props.put("mail.smtp.starttls.enable", emailConfig.isStarttls());
//        props.put("mail.debug", "true");
//        return mailSender;
//    }
//}
