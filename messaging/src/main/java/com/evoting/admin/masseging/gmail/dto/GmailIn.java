package com.evoting.admin.masseging.gmail.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class GmailIn {
    private String message;
    private String email;
    private String subject;
    private String from;
}
