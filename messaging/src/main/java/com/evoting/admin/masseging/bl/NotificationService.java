package com.evoting.admin.masseging.bl;

import com.evoting.utility.model.object.SystemError;
import com.evoting.utility.model.object.SystemException;
import com.evoting.admin.masseging.gmail.dto.GmailIn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;

@Service
public class NotificationService {

    private final IEmailService emailService;

    @Autowired
    public NotificationService(@Qualifier("GmailEmailService") IEmailService emailService) {
        this.emailService = emailService;
    }

    public void sendEmail(GmailIn gmailIn) throws SystemException {

        try {
            emailService.sendEmail(gmailIn.getMessage(), gmailIn.getSubject(), gmailIn.getFrom(), gmailIn.getEmail());
        } catch (MessagingException e) {
            System.out.println(e.getCause());
            System.out.println(e.getMessage());
            for (StackTraceElement each : e.getStackTrace()) {
                System.out.println(each);
            }
            throw new SystemException(SystemError.ILLEGAL_ARGUMENT, "ارسال ایمیل با خطایی مواجه شده است", 4901);
        }
    }
}
