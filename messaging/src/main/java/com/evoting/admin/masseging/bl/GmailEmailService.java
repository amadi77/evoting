package com.evoting.admin.masseging.bl;

import com.evoting.utility.config.ApplicationProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

@Service("GmailEmailService")
public class GmailEmailService implements IEmailService {
    private final ApplicationProperties applicationProperties;

    @Autowired
    public GmailEmailService(ApplicationProperties applicationProperties) {
        this.applicationProperties = applicationProperties;
    }

    @Override
    public void sendEmail(String text, String subject, String from, String to) throws MessagingException {
        Properties properties = new Properties();
        properties.put("mail.smtp.host", applicationProperties.getEmailConfig().getHost());
        properties.put("mail.smtp.port", applicationProperties.getEmailConfig().getPort());
        properties.put("mail.smtp.auth", applicationProperties.getEmailConfig().isAuth());
        properties.put("mail.smtp.starttls.enable", applicationProperties.getEmailConfig().isStarttls());
        properties.put("mail.smtp.user", applicationProperties.getEmailConfig().getUsername());

        Session session = Session.getDefaultInstance(properties);

        Message msg = new MimeMessage(session);

        msg.setFrom(new InternetAddress(applicationProperties.getEmailConfig().getUsername()));
        InternetAddress[] toAddresses = {new InternetAddress(to)};
        msg.setRecipients(Message.RecipientType.TO, toAddresses);
        msg.setSubject(subject);
        msg.setText(text);

        Transport t = session.getTransport("smtp");
        t.connect(applicationProperties.getEmailConfig().getUsername(), applicationProperties.getEmailConfig().getPassword());
        t.sendMessage(msg, msg.getAllRecipients());
        t.close();
    }
}
