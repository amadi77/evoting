package com.evoting.admin.file.statics;

public class FileRestApi {
    public static final String DOWNLOAD="/download/{url}";
    public static final String UPLOAD="/upload";
    public static final String DELETE="/delete";
}
