package com.evoting.admin.file.repository;

import com.evoting.admin.file.model.FileOut;
import com.evoting.utility.config.ApplicationProperties;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class FileService {
    private final ApplicationProperties applicationProperties;

    public FileService(ApplicationProperties applicationProperties) {
        this.applicationProperties = applicationProperties;
    }

    public String upload(MultipartFile multipartFile) {
        String fileUrl = null;
        try {
            String uuid = UUID.nameUUIDFromBytes((multipartFile.getOriginalFilename() + new Date().getTime()).getBytes()).toString();
            String dir = applicationProperties.getUpload().getPath();
            new File(dir + StringUtils.cleanPath(uuid)).mkdir();
            fileUrl = StringUtils.cleanPath(uuid) + File.separator + multipartFile.getOriginalFilename();
            Path path = Paths.get(dir + fileUrl);
            Files.copy(multipartFile.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fileUrl;
    }

    public FileOut download(String url) {
        return null;
    }

    public List<String> uploadList(List<MultipartFile> multipartFiles) {
        return multipartFiles.stream().map(this::upload).collect(Collectors.toList());
    }

    public void deleteFile(String url) {
        try {
            url = url.split("/")[0];
            url = url.replace("%20", " ");
            File file = new File(applicationProperties.getUpload().getPath() + url);
            file.deleteOnExit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
