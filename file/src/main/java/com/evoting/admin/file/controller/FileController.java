//package com.evoting.admin.file.controller;
//
//import com.evoting.admin.file.repository.FileService;
//import com.evoting.admin.file.model.FileOut;
//import com.evoting.admin.file.statics.FileRestApi;
//import com.evoting.utility.statics.constants.RestApi;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.MediaType;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.multipart.MultipartFile;
//
//import javax.servlet.http.HttpServletRequest;
//
//@Api(tags = "File Controller")
//@RestController
//@RequestMapping(value = RestApi.REST_PUBLIC)
//public class FileController {
//    private final FileService fileService;
//
//    public FileController(FileService fileService) {
//        this.fileService = fileService;
//    }
//
//    @ApiOperation(value = "upload file in multipart format")
//    @PostMapping(value = FileRestApi.UPLOAD, produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<String> upload(@RequestBody MultipartFile multipartFile, HttpServletRequest request) {
//        return new ResponseEntity<String>(fileService.upload(multipartFile), HttpStatus.OK);
//    }
//
//    @ApiOperation(value = "upload file in multipart format")
//    @GetMapping(value = FileRestApi.UPLOAD, produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<String> downloadFor(@RequestParam("file") String url, HttpServletRequest request) {
//        return new ResponseEntity<>("localhost:8080/" + url, HttpStatus.OK);
//    }
//
//    @ApiOperation(value = "upload file in multipart format")
//    @PostMapping(value = FileRestApi.DOWNLOAD, produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<FileOut> download(@PathVariable String url, HttpServletRequest request) {
//        return new ResponseEntity<FileOut>(fileService.download(url), HttpStatus.OK);
//    }
//
//    @ApiOperation(value = "upload file in multipart format")
//    @PostMapping(value = FileRestApi.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
//    public void delete(@RequestParam("file") String url, HttpServletRequest request) {
//        fileService.deleteFile(url);
//    }
//}
