INSERT INTO security_permission(id_pk, node_type, parent_id_fk, traversal, name)
VALUES (100000, 1, null, false, 'selection'),
       (100001, 10, 100000, false, 'selection.votes');

INSERT INTO role (id_pk, name, fa_name)
VALUES (1, 'admin', 'ادمین'),
       (2, 'kanoon', 'کانون'),
       (3, 'anjoman_elmi', 'انجمن علمی');

INSERT INTO security_permission(id_pk, name, node_type, parent_id_fk, traversal)
values (1, 'admin', 1, null, true),
       (2, 'admin.selection', 10, 1, true)


insert into