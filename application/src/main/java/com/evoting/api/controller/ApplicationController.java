package com.evoting.api.controller;

import com.evoting.utility.config.ApplicationProperties;
import com.evoting.utility.config.files.EmailConfig;
import com.evoting.utility.statics.constants.RestApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = RestApi.REST_PUBLIC)
public class ApplicationController {
    private final ApplicationProperties applicationProperties;

    @Autowired
    public ApplicationController(ApplicationProperties applicationProperties) {
        this.applicationProperties = applicationProperties;
    }


    @GetMapping(value = "/status",produces = MediaType.APPLICATION_JSON_VALUE)
    public EmailConfig status(HttpServletRequest httpServletRequest){
        return applicationProperties.getEmailConfig();
    }
    @GetMapping(value = "/status/ok",produces = MediaType.APPLICATION_JSON_VALUE)
    public String statusOk(HttpServletRequest httpServletRequest){
        return "status";
    }
}
