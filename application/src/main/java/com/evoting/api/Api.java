package com.evoting.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@SpringBootApplication
@EnableTransactionManagement
@EntityScan(basePackages = {"com.evoting.*"})
@ComponentScan(basePackages = {"com.evoting.*"})
@ConfigurationPropertiesScan("com.evoting")
@EnableJpaRepositories(basePackages = "com.evoting")
public class Api {

    public static void main(String[] args) {
        SpringApplication.run(Api.class, args);
    }
}
