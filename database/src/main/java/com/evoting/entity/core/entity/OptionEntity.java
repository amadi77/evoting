package com.evoting.entity.core.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * @author ahmad
 */
@Entity
@Table(name = "option", schema = "evote")
@Setter
@Getter
public class OptionEntity {
    @Id
    @Column(name = "id_pk", scale = 0, precision = 10, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Option_Generator")
    @SequenceGenerator(name = "Option_Generator", allocationSize = 1, sequenceName = "OPTION_GENERATOR_SEQ")
    private int id;
    @Basic
    @Column(name = "description", length = 120)
    private String description;
    @Basic
    @Column(name = "title", length = 60)
    private String title;
    @Basic
    @Column(name = "uniq_code", scale = 0, precision = 10)
    private String uniqCode;
    @Basic
    @Column(name = "vote_count", scale = 0, precision = 10)
    private int voteCount = 0;
    @Basic
    @Column(columnDefinition = "text", name = "fields")
    private String fields;
    @Column(name = "selection_id_fk", nullable = false, scale = 0, precision = 10)
    private Integer selectionId;
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "selection_id_fk", insertable = false, updatable = false)
    private SelectionEntity selection;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "option")
    private Set<VoteEntity> votes = new HashSet<>();
}
