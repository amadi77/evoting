package com.evoting.entity.core.entity;

import com.evoting.entity.security.entity.UserEntity;
import com.evoting.entity.security.statics.TerminalType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @author ahmad
 */
@Setter
@Getter
@Entity
@Table(name = "user_in_selection", schema = "evote")
public class UserInSelectionEntity {
    @Id
    @Column(name = "ID_PK", nullable = false, scale = 0, precision = 10)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "User_Selection_generator")
    @SequenceGenerator(name = "User_Selection_generator", allocationSize = 1, sequenceName = "USER_SELECTION_SEQ")
    private int id;
    @Column(name = "user_id_fk", nullable = false)
    private int userId;
    @Column(name = "terminal", nullable = false)
    @Enumerated(EnumType.STRING)
    private TerminalType terminal;
    @Column(name = "selection_id_fk", nullable = false)
    private int selectionId;
    @Column(name = "created")
    private LocalDateTime created;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id_fk", insertable = false, updatable = false)
    private UserEntity user;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "selection_id_fk", insertable = false, updatable = false)
    private SelectionEntity selection;
}
