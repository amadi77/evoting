package com.evoting.entity.core.statics;

import lombok.Getter;

@Getter
public enum SelectionType {
    MULTI_SELECT(0),
    SINGLE_SELECT(1),
    POINT(2);
    private int value;

    SelectionType(int value) {
        this.value = value;
    }
}
