package com.evoting.entity.core.statics;

import lombok.Getter;
import lombok.Setter;

@Getter
public enum Gender {
    MALE(0),
    FEMALE(1);

    private int value;

    Gender(int value) {
        this.value = value;
    }

    public static Gender getGender(int value) {
        if (value == 0) {
            return Gender.MALE;
        } else if (value == 1) {
            return Gender.FEMALE;
        }
        return null;
    }
}
