package com.evoting.entity.core.entity;

import com.evoting.entity.security.entity.UserEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * @author ahmad
 */
@Entity
@Table(name = "VOTE", schema = "evote", uniqueConstraints = {@UniqueConstraint(columnNames = {"SELECTION_ID_FK", "OPTION_ID_FK", "USER_ID_FK"})})
@Setter
@Getter
public class VoteEntity {
    @Id
    @Column(name = "ID_PK", scale = 0, precision = 10)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Vote_Generator")
    @SequenceGenerator(name = "Vote_Generator", allocationSize = 1, sequenceName = "VOTE_GENERATOR_SEQ")
    private int id;
    @Basic
    @Column(name = "SELECTION_ID_FK", scale = 0, precision = 10, nullable = false)
    private int selectionId;
    @Basic
    @Column(name = "OPTION_ID_FK", scale = 0, precision = 10, nullable = false)
    private int optionId;
    @Basic
    @Column(name = "USER_ID_FK", scale = 0, precision = 10, nullable = false)
    private int userId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID_FK", updatable = false, insertable = false)
    private UserEntity user;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "OPTION_ID_FK", updatable = false, insertable = false)
    private OptionEntity option;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SELECTION_ID_FK", updatable = false, insertable = false)
    private SelectionEntity selection;
    @Column(name = "vote_hash",length = 500)
    @Basic
    private String voteHash;
}
