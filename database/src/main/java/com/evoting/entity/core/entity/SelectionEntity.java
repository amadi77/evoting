package com.evoting.entity.core.entity;

import com.evoting.entity.security.entity.UserEntity;
import com.evoting.entity.security.entity.party.PartyEntity;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * @author ahmad
 */
@Entity
@Table(name = "SELECTION", schema = "evote")
@Setter
@Getter
@Where(clause = "deleted is null")
public class SelectionEntity {
    @Id
    @Column(name = "ID_PK", nullable = false, scale = 0, precision = 10)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Selection_generator")
    @SequenceGenerator(name = "Selection_generator", allocationSize = 1, sequenceName = "SELECTION_GENERATOR_SEQ")
    private int id;
    @Basic
    @Column(name = "NAME", nullable = false, length = 80)
    private String name;
    @Basic
    @Column(name = "CREATED", nullable = false)
    private LocalDateTime created = LocalDateTime.now();
    @Basic
    @Column(name = "LIMIT_VOTE", nullable = false, scale = 0, precision = 10)
    private Integer limitVote;
    @Basic
    @Column(name = "FROM_DATE", nullable = false)
    private LocalDateTime fromDate;
    @Basic
    @Column(name = "DEADLINE", nullable = false)
    private LocalDateTime deadline;
    @Column(name = "TO_DATE", nullable = false)
    private LocalDateTime toDate;
    @Basic
    @Column(name = "CHEATING", columnDefinition = "boolean default false")
    private boolean cheating;
    @Basic
    @Column(name = "FINISHED", columnDefinition = "boolean default false")
    private boolean finished;
    @Column(name = "DELETED")
    private LocalDateTime deleted;
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "selection")
    private Set<OptionEntity> options = new HashSet<>();
    @Basic
    @Column(name = "owner_id_fk", nullable = false)
    private Integer ownerId;
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "owner_id_fk", nullable = false, insertable = false, updatable = false)
    private UserEntity owner;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "party_in_selection", schema = "evote",
            inverseJoinColumns = @JoinColumn(name = "party_id_fk", nullable = false, referencedColumnName = "id_pk"),
            joinColumns = @JoinColumn(name = "selection_if_fk", nullable = false, referencedColumnName = "id_pk")
    )
    private Set<PartyEntity> parties = new HashSet<>();
}
