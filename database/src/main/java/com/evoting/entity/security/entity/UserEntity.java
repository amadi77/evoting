package com.evoting.entity.security.entity;

import com.evoting.entity.core.statics.Gender;
import com.evoting.entity.security.entity.party.PartyEntity;
import com.evoting.entity.security.entity.party.UserInPartyEntity;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "users", schema = "evote")
@Setter
@Getter
public class UserEntity {

    @Id
    @Column(name = "id_pk", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "User_Generator")
    @SequenceGenerator(name = "User_Generator", allocationSize = 1, sequenceName = "USER_SEQ")
    private int id;
    @Basic
    @Column(name = "first_name", length = 40)
    private String firstName;
    @Basic
    @Column(name = "last_name", length = 80)
    private String lastName;
    @Basic
    @Column(name = "full_name", length = 120)
    private String fullName;
    @Basic
    @Column(name = "phone_number", length = 15)
    private String phoneNumber;
    @Enumerated(EnumType.STRING)
    @Column(name = "gender")
    private Gender gender;
    @Basic
    @Column(name = "email", length = 254)
    private String email;
    @Basic
    @Column(name = "national_code", length = 10)
    private String nationalCode;
    @Basic
    @Column(name = "username", length = 50)
    private String username;
    @Basic
    @Column(name = "active", nullable = false)
    private boolean active = true;
    @Basic
    @Column(name = "is_admin", nullable = false)
    private boolean isAdmin = false;
    @Basic
    @Column(name = "require_password_change", nullable = false)
    private boolean requirePasswordChange = false;
    @Basic
    @Column(name = "send_password")
    private LocalDateTime sendPasswordLock;
    @Basic
    @Column(name = "lock")
    private LocalDateTime lock;
    @Basic
    @Column(name = "failed_count", nullable = false)
    private int failedCount = 0;
    @Basic
    @Column(name = "hashed_password", length = 150)
    private String hashedPassword;
    @Basic
    @Column(name = "reset_password", length = 150)
    private String resetPassword;
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_role", schema = "evote",
            joinColumns = @JoinColumn(name = "user_id_fk", nullable = false, referencedColumnName = "id_pk"),
            inverseJoinColumns = @JoinColumn(name = "role_id_fk", nullable = false, referencedColumnName = "id_pk")
    )
    private Set<UserRoleEntity> roles;
    @OneToMany(mappedBy = "owner", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<PartyEntity> ownParties = new HashSet<>();
    /*@ManyToMany(fetch = FetchType.LAZY, mappedBy = "members")
    @JoinTable(name = "user_in_party", schema = "evote",
            joinColumns = @JoinColumn(name = "user_id_fk", nullable = false, referencedColumnName = "id_pk"),
            inverseJoinColumns = @JoinColumn(name = "party_id_fk", nullable = false, referencedColumnName = "id_pk"))
    */
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    private Set<UserInPartyEntity> memberParties = new HashSet<>();

    public void setUsername(String username) {
        if (username != null) {
            this.username = username;
        } else {
            this.username = this.nationalCode;
        }
    }

    public void setNationalCode(String nationalCode) {
        this.nationalCode = nationalCode;
        setUsername(this.username);
    }

    public void setFullName() {
        this.fullName = this.firstName + " " + this.lastName;
    }
}
