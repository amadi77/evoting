package com.evoting.entity.security.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Setter
@Getter
@Table(name = "user_session", schema = "evote")
@Entity
public class UserSessionEntity {
    @Id
    @Column(name = "id_pk")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "UserSessionSeq")
    @SequenceGenerator(sequenceName = "user_session_seq", name = "UserSessionSeq")
    private int id;
    @Basic
    @Column(name = "user_id_fk", nullable = false)
    private Integer userId;
    @Basic
    @Column(name = "ip", nullable = false, length = 30)
    private String ip;
    @Basic
    @Column(name = "agent", nullable = false, length = 250)
    private String agent;
    @Basic
    @Column(name = "client_os", nullable = false, length = 250)
    private String clientOs;
    @Basic
    @Column(name = "created", nullable = false)
    private LocalDateTime created;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id_fk", insertable = false, updatable = false)
    private UserEntity user;
}
