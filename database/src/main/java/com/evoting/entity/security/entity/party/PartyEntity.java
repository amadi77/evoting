package com.evoting.entity.security.entity.party;

import com.evoting.entity.core.entity.SelectionEntity;
import com.evoting.entity.security.entity.UserEntity;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Table(name = "party", schema = "evote")
@Entity
@Setter
@Getter
public class PartyEntity {
    @Id
    @Column(name = "id_pk", nullable = false)
    @SequenceGenerator(name = "party_sequence_generator", allocationSize = 50, sequenceName = "partySequence")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "partySequence")
    private int id;
    @Basic
    @Column(name = "name", length = 100, nullable = false)
    private String name;
    @Basic
    @Column(name = "owner_id_fk", nullable = false)
    private Integer ownerId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "owner_id_fk", nullable = false, insertable = false, updatable = false)
    private UserEntity owner;
    @Basic
    @Column(name = "created", nullable = false)
    private LocalDateTime created;
    /*@ManyToMany(fetch = FetchType.LAZY, mappedBy = "memberParties")
    @JoinTable(name = "user_in_party", schema = "evote",
            joinColumns = @JoinColumn(name = "party_id_fk", nullable = false, referencedColumnName = "id_pk"),
            inverseJoinColumns = @JoinColumn(name = "user_if_fk", nullable = false, referencedColumnName = "id_pk")
    )
    */
    @OneToMany(fetch = FetchType.LAZY,mappedBy = "party")
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    private Set<UserInPartyEntity> members = new HashSet<>();
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "party_in_selection", schema = "evote",
            joinColumns = @JoinColumn(name = "party_id_fk", nullable = false, referencedColumnName = "id_pk"),
            inverseJoinColumns = @JoinColumn(name = "selection_if_fk", nullable = false, referencedColumnName = "id_pk")
    )
    private Set<SelectionEntity> selections = new HashSet<>();

    @PrePersist
    public void prePersist() {
        this.created = LocalDateTime.now();
    }
}
