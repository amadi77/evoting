package com.evoting.entity.security.statics;

import lombok.Getter;

@Getter
public enum TerminalType {
    EMAIL("email"),
    PHONE("phone"),
    ID("id"),
    OTHER("other"),
    USER("user");

    private String value;

    TerminalType(String value) {
        this.value = value;
    }
}
