package com.evoting.entity.security.entity.party;

import com.evoting.entity.security.entity.UserEntity;
import com.evoting.entity.security.statics.TerminalType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Setter
@Getter
@Entity
@Table(name = "user_in_party", schema = "evote",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"party_id_fk", "user_id_fk"})}
)
public class UserInPartyEntity {
    @Id
    @Column(name = "id_pk")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "User_In_party_Generator")
    @SequenceGenerator(name = "User_In_party_Generator", allocationSize = 1, sequenceName = "USER_IN_PARTY_SEQ")
    private int id;
    @Column(name = "party_id_fk", nullable = false)
    private Integer partyId;
    @Column(name = "user_id_fk", nullable = false)
    private Integer userId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "party_id_fk", insertable = false, updatable = false)
    private PartyEntity party;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id_fk", insertable = false, updatable = false)
    private UserEntity user;
    @Column(name = "terminal", nullable = false)
    @Enumerated(EnumType.STRING)
    private TerminalType terminal;
    @Basic
    @Column(name = "invited")
    private LocalDateTime invited;


    public static class UserInPartyBuilder {
        private Integer userId;
        private Integer partyId;
        private TerminalType terminal;

        public UserInPartyBuilder setUserId(Integer userId) {
            this.userId = userId;
            return this;
        }

        public UserInPartyBuilder setPartyId(Integer partyId) {
            this.partyId = partyId;
            return this;
        }

        public UserInPartyBuilder setTerminal(TerminalType terminal) {
            this.terminal = terminal;
            return this;
        }

        public UserInPartyEntity build() {
            UserInPartyEntity userInPartyEntity = new UserInPartyEntity();
            userInPartyEntity.setPartyId(this.partyId);
            userInPartyEntity.setUserId(this.userId);
            userInPartyEntity.setTerminal(this.terminal);
            userInPartyEntity.setInvited(null);
            return userInPartyEntity;
        }
    }
}
