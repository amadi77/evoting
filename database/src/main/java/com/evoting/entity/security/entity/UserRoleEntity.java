package com.evoting.entity.security.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "role", schema = "evote")
@Setter
@Getter
public class UserRoleEntity {
    @Id
    @Column(name = "id_pk", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "User_Role_Generator")
    @SequenceGenerator(name = "User_Role_Generator", sequenceName = "USER_ROLE_SEQ", allocationSize = 1)
    private int id;
    @Basic
    @Column(name = "name", nullable = false, length = 100)
    private String name;
    @Basic
    @Column(name = "fa_name", nullable = false, length = 100)
    private String faName;
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "user_role",
            schema = "evote",
            joinColumns = @JoinColumn(name = "role_id_fk", nullable = false, referencedColumnName = "id_pk"),
            inverseJoinColumns = @JoinColumn(name = "user_id_fk", nullable = false, referencedColumnName = "id_pk")
    )
    private Set<UserEntity> users;
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "role_in_permission",
            schema = "evote",
            joinColumns = @JoinColumn(name = "role_id_fk", nullable = false, referencedColumnName = "id_pk"),
            inverseJoinColumns = @JoinColumn(name = "permission_id_fk", nullable = false, referencedColumnName = "id_pk")
    )

    private Set<SecurityPermissionEntity> permissions;
}
