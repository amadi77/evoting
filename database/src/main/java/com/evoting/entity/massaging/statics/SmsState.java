package com.evoting.entity.massaging.statics;

import lombok.Getter;

@Getter
public enum SmsState {
    PENDING( 0 ),
    SENDING( 1 ),
    RECEIVED( 2 );

    private int value;

    SmsState(int value) {
        this.value = value;
    }
}
