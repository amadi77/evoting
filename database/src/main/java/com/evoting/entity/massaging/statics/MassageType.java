package com.evoting.entity.massaging.statics;

import lombok.Getter;

@Getter
public enum MassageType {
    SMS,
    EMAIL,
    INNER,
    SMS_INNER,
    EMAIL_INNER,
    SMS_EMAIL_INNER
}
