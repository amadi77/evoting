package com.evoting.entity.massaging.entity;

import com.evoting.entity.massaging.statics.SmsState;
import com.evoting.entity.massaging.statics.SmsStatus;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Table(name = "SMS", schema = "evote")
@Entity
@Getter
@Setter
public class SmsEntity {
    @Id
    @Column(name = "ID_PK", nullable = false, scale = 0, precision = 10)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Sms_generator")
    @SequenceGenerator(name = "Sms_generator", allocationSize = 1, sequenceName = "SmsGenerator")
    private int id;
    @Basic
    @Column(name = "MASSAGE", length = 100, nullable = false)
    private String massage;
    @Basic
    @Column(name = "MOBILE_NUMBER", length = 15, nullable = false)
    private String mobileNumber;
    @Enumerated(EnumType.STRING)
    @Column(name = "STATE", nullable = false)
    private SmsState state;
    @Enumerated(EnumType.STRING)
    @Column(name = "STATUS", nullable = false)
    private SmsStatus status;
    @Basic
    @Column(name = "RESPONSE_MASSAGE", length = 200)
    private String responseMassage;
    @Basic
    @Column(name = "RESPONSE_CODE", length = 10)
    private String responseCode;

}
