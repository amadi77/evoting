package com.evoting.entity.massaging.statics;

import lombok.Getter;

@Getter
public enum SmsStatus {
    FAILED( 0 ),
    SUCCESSFUL( 1 );
    private int value;

    SmsStatus(int value) {
        this.value = value;
    }
}
