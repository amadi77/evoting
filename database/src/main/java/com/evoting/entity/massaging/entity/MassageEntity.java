package com.evoting.entity.massaging.entity;

import com.evoting.entity.massaging.statics.MassageType;
import com.evoting.entity.security.entity.UserEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "massage", schema = "evote")
@Entity
@Setter
@Getter
public class MassageEntity {
    @Id
    @Column(name = "ID_PK", nullable = false, scale = 0, precision = 10)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Message_generator")
    @SequenceGenerator(name = "Message_generator", allocationSize = 1, sequenceName = "MassageGenerator")
    private int id;
    @Basic
    @Column(name = "message", nullable = false, length = 500)
    private String message;
    @Basic
    @Column(name = "title", nullable = false, length = 100)
    private String title;
    @Basic
    @Column(name = "created", nullable = false)
    private LocalDateTime created;
    @Basic
    @Column(name = "sender_id_fk", nullable = false)
    private Integer senderId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sender_id_fk", nullable = false, insertable = false, updatable = false)
    private UserEntity sender;
    @Basic
    @Column(name = "receiver_id_fk", nullable = false)
    private Integer receiverId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "receiver_id_fk", nullable = false, insertable = false, updatable = false)
    private UserEntity receiver;
    @Basic
    @Column(name = "receiver_email", length = 100)
    private String receiverEmail;
    @Basic
    @Column(name = "receiver_mobile", length = 20)
    private String receiverMobile;
    @Enumerated(EnumType.STRING)
    @Column(name = "massage_type", nullable = false)
    private MassageType massageType;
}
