package com.evoting.admin.repository.service;

import com.evoting.admin.repository.dao.UserInSelectionDao;
import com.evoting.entity.core.entity.UserInSelectionEntity;
import com.evoting.entity.security.statics.TerminalType;
import com.evoting.utility.repository.AbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Service
public class UserInSelectionService extends AbstractService<UserInSelectionEntity, UserInSelectionDao> {

    @Autowired
    public UserInSelectionService(UserInSelectionDao dao) {
        super(dao);
    }

    public List<UserInSelectionEntity> getAllWithSelectionIdAndUserIds(int selectionId, Set<Integer> userIds, TerminalType terminalType) {
        return getDao().getAllWithSelectionIdAndUserIds(selectionId, userIds, terminalType);
    }

    public void create(int selectionId, int userId, TerminalType terminalType) {
        UserInSelectionEntity entity = new UserInSelectionEntity();
        entity.setCreated(LocalDateTime.now());
        entity.setUserId(userId);
        entity.setSelectionId(selectionId);
        entity.setTerminal(terminalType);
        createEntity(entity);
    }
}
