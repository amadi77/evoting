package com.evoting.admin.repository.service;

import com.evoting.entity.security.entity.party.PartyEntity;
import com.evoting.admin.dto.party.PartyFilter;
import com.evoting.admin.dto.party.PartyIn;
import com.evoting.admin.dto.party.PartyOut;
import com.evoting.admin.dto.party.PartyPageableFilter;
import com.evoting.admin.repository.dao.PartyDao;
import com.evoting.admin.security.object.UserContextDto;
import com.evoting.utility.model.object.ReportFilter;
import com.evoting.utility.model.object.SystemError;
import com.evoting.utility.model.object.SystemException;
import com.evoting.utility.model.object.report.ReportCondition;
import com.evoting.utility.model.object.report.ReportOption;
import com.evoting.utility.repository.AbstractFilterableService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static com.evoting.admin.security.authentication.filter.JwtUser.getAuthenticatedUser;

@Service
public class PartyService extends AbstractFilterableService<PartyEntity, PartyFilter, PartyDao> {
    private final ModelMapper modelMapper;

    @Autowired
    public PartyService(PartyDao dao, ModelMapper modelMapper) {
        super(dao);
        this.modelMapper = modelMapper;
    }

    public List<PartyEntity> getAllEntities(PartyPageableFilter filter, String[] include) {
        return getAllEntities(filter(filter), include);
    }

    public List<PartyOut> getAll(PartyPageableFilter filter, String[] include) throws SystemException {
        UserContextDto user = getAuthenticatedUser();
        filter.setUserId(user.getId());
        return getAllEntities(filter, include).stream().map(PartyOut::new).collect(Collectors.toList());
    }

    public int count(PartyFilter filter) throws SystemException {
        UserContextDto user = getAuthenticatedUser();
        filter.setUserId(user.getId());

        return countEntity(filter(filter));
    }

    public PartyOut getById(int id, String[] include) throws SystemException {
        PartyEntity entity = getEntityByIdWithOwnerCheck(id, include);
        return new PartyOut(entity);
    }

    public boolean delete(int id) throws SystemException {
        getEntityByIdWithOwnerCheck(id, null);
        deleteById(id);
        return true;
    }

    public PartyOut create(PartyIn partyIn) throws SystemException {
        PartyEntity entity = modelMapper.map(partyIn, PartyEntity.class);
        UserContextDto authenticatedUser = getAuthenticatedUser();
        entity.setOwnerId(authenticatedUser.getId());
        createEntity(entity);
        return new PartyOut(entity);
    }

    public PartyOut update(int id, PartyIn partyIn) throws SystemException {
        PartyEntity entity = getEntityByIdWithOwnerCheck(id, null);
        modelMapper.map(partyIn, entity);
        updateEntity(entity);
        return new PartyOut(entity);
    }

    public PartyEntity getEntityByIdWithOwnerCheck(Integer id, String[] include) throws SystemException {
        PartyEntity entity = getEntityById(id, include);
        UserContextDto user = getAuthenticatedUser();
        if (!entity.getOwnerId().equals(user.getId())) {
            throw new SystemException(SystemError.DATA_NOT_FOUND, id, 1810);
        }
        return entity;
    }

    @Override
    public ReportFilter filter(PartyFilter filter) {
        ReportOption reportOption = new ReportOption();
        if (filter instanceof PartyPageableFilter) {
            PartyPageableFilter pageableFilter = (PartyPageableFilter) filter;
            reportOption.setPageNumber(pageableFilter.getPage());
            reportOption.setPageSize(pageableFilter.getSize());
            reportOption.setSortOptions(pageableFilter.getSort());
        }
        ReportCondition reportCondition = new ReportCondition();
        reportCondition.addEqualCondition("id", filter.getId());
        reportCondition.addEqualCondition("ownerId", filter.getUserId());
        reportCondition.addEqualCondition("name", filter.getName());

        return new ReportFilter(reportCondition, reportOption);
    }
}
