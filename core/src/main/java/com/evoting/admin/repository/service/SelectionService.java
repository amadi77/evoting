package com.evoting.admin.repository.service;

import com.evoting.admin.dto.SelectionInvitationTemplate;
import com.evoting.admin.dto.party.InvitationResultOut;
import com.evoting.admin.dto.selection.*;
import com.evoting.admin.dto.vote.OptionVoteCountResult;
import com.evoting.admin.masseging.bl.NotificationService;
import com.evoting.admin.masseging.gmail.dto.GmailIn;
import com.evoting.admin.repository.dao.SelectionDao;
import com.evoting.admin.security.authentication.filter.JwtUser;
import com.evoting.admin.security.object.UserContextDto;
import com.evoting.entity.core.entity.OptionEntity;
import com.evoting.entity.core.entity.SelectionEntity;
import com.evoting.entity.core.entity.UserInSelectionEntity;
import com.evoting.entity.core.entity.VoteEntity;
import com.evoting.entity.security.entity.UserEntity;
import com.evoting.entity.security.entity.party.PartyEntity;
import com.evoting.entity.security.entity.party.UserInPartyEntity;
import com.evoting.entity.security.statics.TerminalType;
import com.evoting.statics.Template;
import com.evoting.utility.bl.HashService;
import com.evoting.utility.bl.template.ITemplateService;
import com.evoting.utility.model.object.ReportFilter;
import com.evoting.utility.model.object.SystemError;
import com.evoting.utility.model.object.SystemException;
import com.evoting.utility.model.object.report.ReportCondition;
import com.evoting.utility.model.object.report.ReportOption;
import com.evoting.utility.repository.AbstractFilterableService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class SelectionService extends AbstractFilterableService<SelectionEntity, SelectionFilter, SelectionDao> {
    private final ModelMapper modelMapper;
    private final VoteService voteService;
    private final PartyService partyService;
    private final NotificationService notificationService;
    private final ITemplateService templateService;
    private final UserInSelectionService userInSelectionService;
    private final HashService hashService;

    @Autowired
    public SelectionService(SelectionDao dao, ModelMapper modelMapper, VoteService voteService, PartyService partyService, NotificationService notificationService, ITemplateService templateService, UserInSelectionService userInSelectionService, HashService hashService) {
        super(dao);
        this.modelMapper = modelMapper;
        this.voteService = voteService;
        this.partyService = partyService;
        this.notificationService = notificationService;
        this.templateService = templateService;
        this.userInSelectionService = userInSelectionService;
        this.hashService = hashService;
    }

    public List<SelectionEntity> getAllEntities(SelectionPageableFilter filter, String[] include) {
        return getAllEntities(filter(filter), include);
    }

    public List<SelectionOut> getAll(SelectionPageableFilter filter, String[] include) throws SystemException {
        UserContextDto user = JwtUser.getAuthenticatedUser();
        filter.setOwnerId(user.getId());
        return getAllEntities(filter, include).stream().map(SelectionOut::new).collect(Collectors.toList());
    }

    public List<SelectionInfo> getAllInfo(SelectionPageableFilter filter) throws SystemException {
        UserContextDto user = JwtUser.getAuthenticatedUser();
        filter.setOwnerId(user.getId());
        return getAllEntities(filter, null).stream().map(SelectionInfo::new).collect(Collectors.toList());
    }

    public SelectionOut getById(int id,String[] include) throws SystemException {
//        String[] include = {"options", "parties"};
        SelectionEntity entity = getEntityByIdWithOwnerCheck(id, include);
        List<OptionVoteCountResult> optionVoteCount = voteService.getVoteCountEachOptionBySelectionId(id);
        return new SelectionOut(entity, optionVoteCount);
    }

    public SelectionEntity getByIdAndOwnerId(Integer selectionId, Integer ownerId) {
        return getDao().getByIdAndOwnerId(selectionId, ownerId);
    }

    public int count(SelectionFilter filter) throws SystemException {
        UserContextDto user = JwtUser.getAuthenticatedUser();
        filter.setOwnerId(user.getId());
        return countEntity(filter(filter));
    }

    public SelectionOut create(SelectionIn selectionIn) throws SystemException {
        SelectionEntity selectionEntity = modelMapper.map(selectionIn, SelectionEntity.class);
        UserContextDto user = JwtUser.getAuthenticatedUser();
        selectionEntity.setOwnerId(user.getId());
        List<PartyEntity> partyServiceEntities = partyService.getEntitiesByIds(Arrays.asList(selectionIn.getPartyIds()));
        selectionEntity.getParties().clear();
        selectionEntity.getParties().addAll(partyServiceEntities);
        createEntity(selectionEntity);
        return new SelectionOut(selectionEntity);
    }

    public SelectionOut update(int id, SelectionIn selectionIn) throws SystemException {
        SelectionEntity entity = getEntityByIdWithOwnerCheck(id, null);
        if ((entity.getToDate().compareTo(LocalDateTime.now()) < 0 || entity.getFromDate().compareTo(LocalDateTime.now()) > 0) || entity.isFinished()) {
            throw new SystemException(SystemError.ILLEGAL_REQUEST, "can't update started or finished selection", 3001);
        }
        modelMapper.map(selectionIn, entity);
        List<PartyEntity> partyServiceEntities = partyService.getEntitiesByIds(Arrays.asList(selectionIn.getPartyIds()));
        entity.getParties().clear();
        entity.getParties().addAll(partyServiceEntities);
        updateEntity(entity);
        return new SelectionOut(entity);
    }

    public boolean delete(int id) throws SystemException {
        SelectionEntity entity = getEntityByIdWithOwnerCheck(id, null);
        if (entity.getToDate().compareTo(LocalDateTime.now()) < 0 || entity.getFromDate().compareTo(LocalDateTime.now()) > 0 || entity.isFinished()) {
            throw new SystemException(SystemError.ILLEGAL_REQUEST, "can't delete started or finished selection", 3002);
        }
        try {
            deleteById(id);
        } catch (Exception e) {
            throw new SystemException(SystemError.ILLEGAL_REQUEST, "can't delete ", 3003);
        }
        return true;
    }

    public boolean finishSelection(int id) throws SystemException {
        SelectionEntity entity = getEntityById(id, null);
        entity.setFinished(true);
        updateOptionVoteCount(entity);
        updateEntity(entity);
        return true;
    }

    public SelectionEntity getEntityByIdAndPartyIds(Integer selectionId, List<Integer> partyIds) throws SystemException {
        UserContextDto user = JwtUser.getAuthenticatedUser();
        SelectionEntity entity = getDao().getEntityByIdAndPartyId(selectionId, partyIds, user.getId());
        if (entity == null) {
            throw new SystemException(SystemError.DATA_NOT_FOUND, "selectionId:" + selectionId + " userId:" + user.getId(), 1831);
        }
        return entity;
    }

    public SelectionEntity getEntityByIdWithOwnerCheck(Integer id, String[] include) throws SystemException {
        SelectionEntity entity = getEntityById(id, include);
        UserContextDto user = JwtUser.getAuthenticatedUser();
        if (!entity.getOwnerId().equals(user.getId())) {
            throw new SystemException(SystemError.DATA_NOT_FOUND, id, 1820);
        }
        return entity;
    }

    public InvitationResultOut sendInvitationForSelection(Integer selectionId, List<Integer> partyIds) throws SystemException {
        SelectionEntity selection;
        if (partyIds != null && !partyIds.isEmpty()) {
            selection = getEntityByIdAndPartyIds(selectionId, partyIds);
        } else {
            selection = getEntityByIdWithOwnerCheck(selectionId, new String[]{"parties.members.user"});
        }
        if (selection.getToDate().isBefore(LocalDateTime.now())) {
            throw new SystemException(SystemError.ILLEGAL_REQUEST, "selection is over", 1841);
        }
        InvitationResultOut resultOut = new InvitationResultOut();
        List<UserEntity> users = new ArrayList<>();
        Set<Integer> userIds = new HashSet<>();
        for (PartyEntity eachParty : selection.getParties()) {
            for (UserInPartyEntity eachPartyMember : eachParty.getMembers()) {
                users.add(eachPartyMember.getUser());
                userIds.add(eachPartyMember.getUserId());
//                if (eachPartyMember.getInvited() == null) {
                /*try {
                    GmailIn gmailIn = new GmailIn();
                    gmailIn.setEmail(eachPartyMember.getUser().getEmail());
                    gmailIn.setSubject(templateService.render(Template.SEND_INVITATION_SUBJECT, selection.getName()));
                    gmailIn.setMessage(templateService.render(Template.SEND_INVITATION_EMAIL, new SelectionInvitationTemplate(selection)));
                    notificationService.sendEmail(gmailIn);
                    resultOut.increaseInvited();
//                        eachPartyMember.setInvited(LocalDateTime.now());
                } catch (SystemException e) {
                    if (e.getErrorCode() == 4901) {
                        resultOut.increaseFailed();
//                            todo
                    }
                }*/
//                }
            }
        }
        List<UserInSelectionEntity> userInSelectionInvitation = userInSelectionService.getAllWithSelectionIdAndUserIds(selectionId, userIds, TerminalType.EMAIL);
        for (UserEntity eachUser : users) {
            boolean checkSend = true;
            for (UserInSelectionEntity eachInvitation : userInSelectionInvitation) {
                if (eachUser.getId() == eachInvitation.getUserId()) {
                    checkSend = false;
                    break;
                }
            }
            if (checkSend) {
                sendEmail(selection, eachUser, resultOut);
            }
        }
        return resultOut;
    }

    private void sendEmail(SelectionEntity selection, UserEntity entity, InvitationResultOut resultOut) {
        try {
            GmailIn gmailIn = new GmailIn();
            gmailIn.setEmail(entity.getEmail());
            gmailIn.setSubject(templateService.render(Template.SEND_INVITATION_SUBJECT, selection.getName()));
            gmailIn.setMessage(templateService.render(Template.SEND_INVITATION_EMAIL, new SelectionInvitationTemplate(selection)));
            notificationService.sendEmail(gmailIn);
            resultOut.increaseInvited();
            userInSelectionService.create(selection.getId(), entity.getId(), TerminalType.EMAIL);
//                        eachPartyMember.setInvited(LocalDateTime.now());
        } catch (SystemException e) {
            if (e.getErrorCode() == 4901) {
                resultOut.increaseFailed();
//                            todo
            }
        }
    }

    public SelectionOut extend(int id, SelectionEditIn editIn) throws SystemException {
        SelectionEntity entity = getEntityByIdWithOwnerCheck(id, null);
        if (editIn.getToDate().isBefore(entity.getToDate())){
            throw new SystemException(SystemError.ILLEGAL_REQUEST,"can't extend selection",3071);
        }
        modelMapper.map(editIn, entity);
        updateEntity(entity);
        return new SelectionOut(entity);
    }

    public void updateOptionVoteCount(int selectionId) throws SystemException {
        SelectionEntity selection = getDao().getSelectionWithOptionAndVote(selectionId);
        if (selection == null) {
            throw new SystemException(SystemError.DATA_NOT_FOUND, "selectionId: " + selectionId, 1204);
        }
        updateOptionVoteCount(selection);
        updateEntity(selection);
    }

    public void updateOptionVoteCount(SelectionEntity selection) throws SystemException {
        List<VoteEntity> votes = voteService.getAllEntitiesBySelectionId(selection.getId());
        for (OptionEntity eachOption : selection.getOptions()) {
            List<VoteEntity> optionVotes = votes.stream().filter(o -> o.getOptionId() == eachOption.getId()).collect(Collectors.toList());
            int optionVoteCount = 0;
            for (VoteEntity eachVote : optionVotes) {
                String hash = "option_" +
                        eachVote.getOptionId() +
                        "selection_" +
                        eachVote.getSelectionId() +
                        "user_" +
                        eachVote.getUserId();
                if (hashService.encrypt(hash).equals(eachVote.getVoteHash())) {
                    optionVoteCount++;
                }
            }
            if (eachOption.getVoteCount() != optionVoteCount) {
                selection.setCheating(true);
            }
        }
    }

    @Override
    public ReportFilter filter(SelectionFilter filter) {
        ReportOption reportOption = new ReportOption();
        if (filter instanceof SelectionPageableFilter) {
            SelectionPageableFilter pageableFilter = (SelectionPageableFilter) filter;
            reportOption.setPageNumber(pageableFilter.getPage());
            reportOption.setPageSize(reportOption.getPageSize());
            reportOption.setSortOptions(pageableFilter.getSort());
        }
        ReportCondition reportCondition = new ReportCondition();
        reportCondition.addEqualCondition("id", filter.getId());
        reportCondition.addEqualCondition("ownerId", filter.getOwnerId());
        reportCondition.addEqualCondition("name", filter.getName());
        reportCondition.addMinTimeCondition("deadline", filter.getDeadlineMin());
        reportCondition.addMinTimeCondition("fromDate", filter.getFromDateMin());
        reportCondition.addMinTimeCondition("toDate", filter.getToDateMin());
        reportCondition.addMaxTimeCondition("deadline", filter.getDeadlineMax());
        reportCondition.addMaxTimeCondition("fromDate", filter.getFromDateMax());
        reportCondition.addMaxTimeCondition("toDate", filter.getToDateMax());
        return new ReportFilter(reportCondition, reportOption);
    }
}
