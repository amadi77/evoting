package com.evoting.admin.repository.service;

import com.evoting.admin.dto.party.UserPartyInfo;
import com.evoting.admin.dto.party.user.UserInPartyFilter;
import com.evoting.admin.dto.party.user.UserInPartyPageableFilter;
import com.evoting.admin.dto.party.user.UserPartyRegisterIn;
import com.evoting.admin.dto.party.user.UserToPartyResult;
import com.evoting.admin.repository.dao.UserInPartyDao;
import com.evoting.admin.security.authentication.filter.JwtUser;
import com.evoting.admin.security.object.UserContextDto;
import com.evoting.entity.security.entity.UserEntity;
import com.evoting.entity.security.entity.party.PartyEntity;
import com.evoting.entity.security.entity.party.UserInPartyEntity;
import com.evoting.entity.security.statics.TerminalType;
import com.evoting.user.repository.UserService;
import com.evoting.utility.model.object.ReportFilter;
import com.evoting.utility.model.object.SystemError;
import com.evoting.utility.model.object.SystemException;
import com.evoting.utility.model.object.report.ReportCondition;
import com.evoting.utility.model.object.report.ReportCriteriaJoinCondition;
import com.evoting.utility.model.object.report.ReportOption;
import com.evoting.utility.repository.AbstractFilterableService;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.criteria.JoinType;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserInPartyService extends AbstractFilterableService<UserInPartyEntity, UserInPartyFilter, UserInPartyDao> {
    private final UserService userService;
    private final PartyService partyService;


    @Autowired
    public UserInPartyService(UserService userService, PartyService partyService) {
        this.userService = userService;
        this.partyService = partyService;
    }

    public void addUserToParty(int partyId, UserPartyRegisterIn registerIn, TerminalType type) throws SystemException {
        if (registerIn.getEmail() == null && registerIn.getMobile() == null) {
            throw new SystemException(SystemError.ILLEGAL_REQUEST, "mobile or email must full", 2301);
        }
        UserInPartyEntity userInPartyEntity = new UserInPartyEntity();
        PartyEntity entity = partyService.getEntityById(partyId, null);
        UserContextDto owner = JwtUser.getAuthenticatedUser();
        if (!entity.getOwnerId().equals(owner.getId())) {
            throw new SystemException(SystemError.ILLEGAL_REQUEST, "", 1845);
        }
        userInPartyEntity.setParty(entity);
        UserEntity user = null;
        boolean check = true;
        switch (type) {
            case OTHER:
                break;
            case PHONE:
                user = getUserByMobileOrEmail(registerIn.getMobile(), null);
                break;
            case EMAIL:
                user = getUserByMobileOrEmail(null, registerIn.getEmail());
                break;
            case USER:
                break;
            case ID:
                userInPartyEntity.setUserId(registerIn.getId());
                check = false;
                break;
        }
        if (check) {
            if (user == null) {
                user = createUserEntityObject(registerIn.getEmail(), registerIn.getMobile());
                userService.createEntity(user);
            }
            userInPartyEntity.setUserId(user.getId());
            userInPartyEntity.setUser(user);
        }
        userInPartyEntity.setPartyId(partyId);
        userInPartyEntity.setTerminal(type);
        try {
            createEntity(userInPartyEntity);
        } catch (Exception e) {
            throw new SystemException(SystemError.ILLEGAL_REQUEST, "can't add user to party twice", 1856);
        }
    }

    public void removeUserFromParty(int userId, int partyId) throws SystemException {
        UserInPartyEntity entity = getDao().getEntityByPartyIdAndUserId(userId, partyId);
        UserContextDto user = JwtUser.getAuthenticatedUser();
        if (!entity.getParty().getOwnerId().equals(user.getId())) {
            throw new SystemException(SystemError.ILLEGAL_REQUEST, "can't confirm request", 1855);
        }
        deleteById(entity.getId());
    }

    public UserToPartyResult addUsersToParty(int partyId, List<UserPartyRegisterIn> registerIns) {
        List<UserInPartyEntity> userInPartyEntities = new ArrayList<>();
        List<UserPartyRegisterIn> notFound = new ArrayList<>();
        UserInPartyEntity.UserInPartyBuilder userInPartyBuilder = new UserInPartyEntity.UserInPartyBuilder();
        int success = 0, duplicate = 0;
        for (UserPartyRegisterIn registerIn : registerIns) {
            userInPartyBuilder.setPartyId(partyId);
            if (registerIn.getId() != null) {
                userInPartyEntities.add(userInPartyBuilder
                        .setUserId(registerIn.getId())
                        .setTerminal(TerminalType.ID)
                        .build());
            } else {
                addUserToParty(registerIn, userInPartyEntities, userInPartyBuilder, notFound);
            }
        }
        if (!notFound.isEmpty()) {
            List<UserEntity> newUsers = createUsers(notFound);
            for (UserEntity newUser : newUsers) {
                UserInPartyEntity userInPartyEntity = addUserToParty(newUser, userInPartyEntities, userInPartyBuilder);
                try {
                    createEntity(userInPartyEntity);
                    success++;
                } catch (Exception e) {
                    duplicate++;
                }
            }
        }
//        saveOrUpdateEntityCollection(userInPartyEntities);
        return new UserToPartyResult(success, duplicate);
    }

    public UserToPartyResult addUsersToParty(int partyId, MultipartFile file) throws SystemException {
        List<UserPartyRegisterIn> users = new ArrayList<>();
        if (file == null) {
            throw new SystemException(SystemError.ILLEGAL_REQUEST, "file must not null", 2331);
        }
        try {
            Workbook workbook = new XSSFWorkbook(file.getInputStream());
            Sheet sheet = workbook.getSheetAt(0);
            Iterator<Row> rows = sheet.rowIterator();
            while (rows.hasNext()) {
                Row row = rows.next();
                UserPartyRegisterIn user = new UserPartyRegisterIn();
                if (row.getCell(0) != null) {
                    user.setEmail(row.getCell(0).getStringCellValue());
                }
                if (row.getCell(1) != null) {
                    user.setMobile(row.getCell(1).getStringCellValue());
                }
                users.add(user);
            }
        } catch (IOException | SystemException ignored) {
//todo must change this part
        }
        return addUsersToParty(partyId, users);
    }

    private void addUserToParty(UserPartyRegisterIn registerIn, List<UserInPartyEntity> userInPartyEntities, UserInPartyEntity.UserInPartyBuilder userInPartyBuilder, List<UserPartyRegisterIn> notFound) {
        UserEntity user;
        if (registerIn.getEmail() != null) {
            try {
                user = getUserByMobileOrEmail(null, registerIn.getEmail());
                userInPartyEntities.add(userInPartyBuilder
                        .setUserId(user.getId())
                        .setTerminal(TerminalType.EMAIL)
                        .build());

            } catch (Exception e) {
                notFound.add(registerIn);
            }
        } else if (registerIn.getMobile() != null) {
            try {
                user = getUserByMobileOrEmail(registerIn.getMobile(), null);
                userInPartyEntities.add(userInPartyBuilder
                        .setUserId(user.getId())
                        .setTerminal(TerminalType.PHONE)
                        .build());
            } catch (Exception e) {
                notFound.add(registerIn);
            }
        }
    }

    private UserInPartyEntity addUserToParty(UserEntity userEntity, List<UserInPartyEntity> userInPartyEntities, UserInPartyEntity.UserInPartyBuilder userInPartyBuilder) {
        userInPartyBuilder.setUserId(userEntity.getId());
        if (userEntity.getEmail() != null) {
//            userInPartyEntities.add(userInPartyBuilder
//                    .setUserId(userEntity.getId())
            userInPartyBuilder.setTerminal(TerminalType.EMAIL);

        } else if (userEntity.getPhoneNumber() != null) {
//            userInPartyEntities.add(userInPartyBuilder
//                    .setUserId(userEntity.getId())
            userInPartyBuilder.setTerminal(TerminalType.PHONE);
        } else {
            return null;
        }
        return userInPartyBuilder.build();
    }

    private List<UserEntity> createUsers(List<UserPartyRegisterIn> notFound) {
        List<UserEntity> userEntities = new ArrayList<>();
        for (UserPartyRegisterIn eachUser : notFound) {
            userEntities.add(createUserEntityObject(eachUser.getEmail(), eachUser.getMobile()));
        }
        userService.saveOrUpdateEntityCollection(userEntities);
        return userEntities;
    }

    private UserEntity getUserByMobileOrEmail(String mobile, String email) throws SystemException {
        return userService.getByMobileOrEmail(mobile, email);
    }

    private UserEntity createUserEntityObject(String email, String phoneNumber) {
        UserEntity user = new UserEntity();
        user.setEmail(email);
        user.setPhoneNumber(phoneNumber);
        user.setRequirePasswordChange(true);
//            todo send email
        return user;
    }

    public List<UserInPartyEntity> getAllEntities(UserInPartyPageableFilter filter, String[] include) throws SystemException {
        UserContextDto user = JwtUser.getAuthenticatedUser();
        filter.setOwnerId(user.getId());
        return getAllEntities(filter(filter), include);
    }

    public List<UserPartyInfo> getAll(UserInPartyPageableFilter filter, String[] include) throws SystemException {
        return getAllEntities(filter, include).stream().map(UserPartyInfo::new).collect(Collectors.toList());
    }

    public int count(UserInPartyFilter filter) throws SystemException {
        UserContextDto user = JwtUser.getAuthenticatedUser();
        filter.setOwnerId(user.getId());
        return countEntity(filter(filter));
    }

    @Override
    public ReportFilter filter(UserInPartyFilter filter) {
        ReportOption reportOption = new ReportOption();
        if (filter instanceof UserInPartyPageableFilter) {
            UserInPartyPageableFilter partyPageableFilter = (UserInPartyPageableFilter) filter;
            reportOption.setPageNumber(partyPageableFilter.getPage());
            reportOption.setPageSize(partyPageableFilter.getSize());
            reportOption.setSortOptions(partyPageableFilter.getSort());
        }
        ReportCondition reportCondition = new ReportCondition();
        reportCondition.addEqualCondition("id", filter.getId());

        ReportCriteriaJoinCondition partyJoin = new ReportCriteriaJoinCondition("party", JoinType.INNER);
        partyJoin.addEqualCondition("id", filter.getPartyId());
        partyJoin.addEqualCondition("ownerId", filter.getOwnerId());

        ReportCriteriaJoinCondition userJoin = new ReportCriteriaJoinCondition("user", JoinType.INNER);
        userJoin.addEqualCondition("id", filter.getUserId());
        userJoin.addLikeCondition("phoneNumber", filter.getMobile());
        userJoin.addLikeCondition("email", filter.getEmail());

        reportCondition.addJoinCondition(partyJoin);
        reportCondition.addJoinCondition(userJoin);


        return new ReportFilter(reportCondition, reportOption);
    }
}
