package com.evoting.admin.repository.dao;

import com.evoting.entity.core.entity.SelectionEntity;
import com.evoting.utility.repository.Dao;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class SelectionDao extends Dao<SelectionEntity> {

    public SelectionEntity getByIdAndOwnerId(int id, int ownerId) {
        Query query = this.getEntityManager().createQuery("select selection from SelectionEntity selection where selection.ownerId=:ownerId and selection.id=:id");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("ownerId", ownerId);
        parameters.put("id", id);
        List<SelectionEntity> selections = queryHql(query, parameters);
        return selections.isEmpty() ? null : selections.get(0);
    }

    public SelectionEntity getEntityByIdAndPartyId(Integer selectionId, List<Integer> partyIds, Integer ownerId) {
        Query query = this.getEntityManager().createQuery("select selection from SelectionEntity selection " +
                " join fetch selection.parties party " +
                " join fetch party.members members " +
                " join fetch members.user " +
                " where selection.id= :selectionId and party.id in (:partyIds) and selection.ownerId=:ownerId");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("selectionId", selectionId);
        parameters.put("partyIds", partyIds);
        parameters.put("ownerId", ownerId);
        List<SelectionEntity> selections = queryHql(query, parameters);
        return selections.isEmpty() ? null : selections.get(0);
    }

    public SelectionEntity getSelectionWithOptionAndVote(int selectionId) {
        Query query = this.getEntityManager().createQuery("select seleciton from SelectionEntity seleciton join fetch seleciton.options option join fetch option.votes where seleciton.id = :id");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("selectionId", selectionId);
        List<SelectionEntity> selections = queryHql(query, parameters);
        return selections.isEmpty() ? null : selections.get(0);
    }
}
