package com.evoting.admin.repository.dao;

import com.evoting.entity.core.entity.OptionEntity;
import com.evoting.utility.repository.Dao;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class OptionDao extends Dao<OptionEntity> {

    public OptionEntity getEntityById(Integer id, Integer ownerId) {
        Query query = this.getEntityManager().createQuery("select option from OptionEntity option join option.selection selection where selection.ownerId = :ownerId and option.id=:id");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("id", id);
        parameters.put("ownerId", ownerId);
        List<OptionEntity> options = queryHql(query, parameters);
        return options.isEmpty() ? null : options.get(0);
    }
}
