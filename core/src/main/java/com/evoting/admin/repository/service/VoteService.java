package com.evoting.admin.repository.service;

import com.evoting.admin.dto.vote.OptionVoteCountResult;
import com.evoting.admin.repository.dao.VoteDao;
import com.evoting.entity.core.entity.VoteEntity;
import com.evoting.utility.model.object.ReportFilter;
import com.evoting.utility.model.object.report.ReportCondition;
import com.evoting.utility.model.object.report.ReportOption;
import com.evoting.utility.repository.AbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VoteService extends AbstractService<VoteEntity, VoteDao> {

    @Autowired
    public VoteService(VoteDao dao) {
        super(dao);
    }

    public List<OptionVoteCountResult> getVoteCountEachOptionBySelectionId(Integer selectionId) {
        return getDao().getVoteCountEachOptionBySelectionId(selectionId);
    }

    public Integer countByOptionId(Integer optionId) {
        ReportCondition reportCondition = new ReportCondition();
        reportCondition.addEqualCondition("optionId", optionId);

        return countEntity(new ReportFilter(reportCondition, new ReportOption()));
    }

    public List<VoteEntity> getAllEntitiesBySelectionId(int selectionId) {
        return getDao().getAllBySelectionId(selectionId);
    }
}
