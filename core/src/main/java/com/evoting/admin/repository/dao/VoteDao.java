package com.evoting.admin.repository.dao;

import com.evoting.admin.dto.vote.OptionVoteCountResult;
import com.evoting.entity.core.entity.VoteEntity;
import com.evoting.utility.repository.Dao;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class VoteDao extends Dao<VoteEntity> {

    public List<OptionVoteCountResult> getVoteCountEachOptionBySelectionId(Integer selectionId) {
        Query query = this.getEntityManager().createQuery("select new com.evoting.admin.dto.vote.OptionVoteCountResult(coalesce(count(vote),0),vote.optionId) from VoteEntity vote  where vote.selectionId=:selectionId group by vote.optionId");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("selectionId", selectionId);
        return queryHql(query, parameters);
    }

    public List<VoteEntity> getAllBySelectionId(int selectionId) {
        Query query = this.getEntityManager().createQuery("select vote from VoteEntity vote where vote.selectionId = :selectionId");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("selectionId", selectionId);
        return queryHql(query, parameters);
    }
}
