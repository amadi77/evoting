package com.evoting.admin.repository.dao;

import com.evoting.entity.core.entity.UserInSelectionEntity;
import com.evoting.entity.security.statics.TerminalType;
import com.evoting.utility.repository.Dao;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Repository
public class UserInSelectionDao extends Dao<UserInSelectionEntity> {

    public List<UserInSelectionEntity> getAllWithSelectionIdAndUserIds(int selectionId, Set<Integer> userIds, TerminalType terminalType) {
        Query query = this.getEntityManager().createQuery("select user from UserInSelectionEntity user " +
                "where user.selectionId=:selectionId " +
                "and user.userId in (:userIds) " +
                "and user.terminal=:terminal");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("selectionId", selectionId);
        parameters.put("userIds", userIds);
        parameters.put("terminal", terminalType);
        return queryHql(query, parameters);
    }


}
