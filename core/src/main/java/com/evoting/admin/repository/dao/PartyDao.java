package com.evoting.admin.repository.dao;

import com.evoting.utility.repository.Dao;
import com.evoting.entity.security.entity.party.PartyEntity;
import org.springframework.stereotype.Repository;

@Repository
public class PartyDao extends Dao<PartyEntity> {

}
