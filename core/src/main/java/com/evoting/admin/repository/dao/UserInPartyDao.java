package com.evoting.admin.repository.dao;


import com.evoting.entity.security.entity.party.UserInPartyEntity;
import com.evoting.utility.repository.Dao;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class UserInPartyDao extends Dao<UserInPartyEntity> {
    public UserInPartyEntity getEntityByPartyIdAndUserId(int userId, int partyId) {
        Query query = getEntityManager().createQuery("select entity from UserInPartyEntity entity join fetch entity.party where entity.userId=:userId and entity.partyId=:partyId");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("userId", userId);
        parameters.put("partyId", partyId);
        List<UserInPartyEntity> entities = queryHql(query, parameters);
        return entities.isEmpty() ? null : entities.get(0);
    }

   /* public void updateByUserInPArtyIds(List<UserInPartyId> ids) {
        Query query = this.getEntityManager().createQuery("update UserInPartyEntity set invited = :now where id in (:ids)");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("ids", ids);
        parameters.put("now", LocalDateTime.now());
        updateHqlQuery(query, parameters);


    }*/
}
