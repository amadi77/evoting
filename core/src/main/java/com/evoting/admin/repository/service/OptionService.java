package com.evoting.admin.repository.service;

import com.evoting.admin.dto.option.*;
import com.evoting.admin.dto.vote.OptionVoteCountResult;
import com.evoting.admin.repository.dao.OptionDao;
import com.evoting.admin.security.authentication.filter.JwtUser;
import com.evoting.admin.security.object.UserContextDto;
import com.evoting.entity.core.entity.OptionEntity;
import com.evoting.entity.core.entity.SelectionEntity;
import com.evoting.utility.model.object.ReportFilter;
import com.evoting.utility.model.object.SystemError;
import com.evoting.utility.model.object.SystemException;
import com.evoting.utility.model.object.report.ReportCondition;
import com.evoting.utility.model.object.report.ReportOption;
import com.evoting.utility.repository.AbstractFilterableService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OptionService extends AbstractFilterableService<OptionEntity, OptionFilter, OptionDao> {
    private final ModelMapper modelMapper;
    private final SelectionService selectionService;
    private final VoteService voteService;

    @Autowired
    public OptionService(OptionDao dao, ModelMapper modelMapper, SelectionService selectionService,@Lazy VoteService voteService) {
        super(dao);
        this.modelMapper = modelMapper;
        this.selectionService = selectionService;
        this.voteService = voteService;
    }

    public List<OptionEntity> getAllEntities(OptionPageableFilter filter, String[] include) {
        return getAllEntities(filter(filter), include);
    }

    public List<OptionOut> getAll(OptionPageableFilter filter, String[] include) {
        List<OptionVoteCountResult> voteCountResults = voteService.getVoteCountEachOptionBySelectionId(filter.getSelectionId());
        return getAllEntities(filter, include).stream().map(o -> new OptionOut(o, voteCountResults)).collect(Collectors.toList());
    }

    public List<OptionInfo> getAllInfo(OptionPageableFilter filter) {
        return getAllEntities(filter, null).stream().map(OptionInfo::new).collect(Collectors.toList());
    }

    public OptionOut getById(int id) throws SystemException {
        UserContextDto user = JwtUser.getAuthenticatedUser();
        OptionEntity entity = getDao().getEntityById(id, user.getId());
        if (entity == null) {
            throw new SystemException(SystemError.DATA_NOT_FOUND, "option id:" + id, 6502);
        }
        Integer count = voteService.countByOptionId(id);
        OptionOut optionOut = new OptionOut(entity);
        optionOut.setVoteCount(count);
        return optionOut;
    }

    public int count(OptionFilter filter) {
        return countEntity(filter(filter));
    }

    public OptionOut create(OptionIn optionIn) throws SystemException {
        UserContextDto user = JwtUser.getAuthenticatedUser();
        SelectionEntity selection = selectionService.getByIdAndOwnerId(optionIn.getSelectionId(), user.getId());
        if (selection == null) {
            throw new SystemException(SystemError.DATA_NOT_FOUND, "selection not found id:" + optionIn.getSelectionId(), 6501);
        }
        if (selection.getDeadline().isBefore(LocalDateTime.now())){
            throw new SystemException(SystemError.ILLEGAL_REQUEST, "deadline is pass", 6505);
        }
        OptionEntity optionEntity = modelMapper.map(optionIn, OptionEntity.class);
        createEntity(optionEntity);
        return new OptionOut(optionEntity);
    }

    public OptionOut update(int id, OptionIn optionIn) throws SystemException {
        UserContextDto user = JwtUser.getAuthenticatedUser();
        SelectionEntity selection = selectionService.getByIdAndOwnerId(optionIn.getSelectionId(), user.getId());
        if (selection == null) {
            throw new SystemException(SystemError.DATA_NOT_FOUND, "selection not found id:" + optionIn.getSelectionId(), 6501);
        }
        if (selection.getDeadline().isBefore(LocalDateTime.now())){
            throw new SystemException(SystemError.ILLEGAL_REQUEST, "deadline is pass", 6505);
        }
        OptionEntity entity = getEntityById(id, null);
        modelMapper.map(optionIn, entity);
        updateEntity(entity);
        return new OptionOut(entity);
    }

    public boolean delete(int id) throws SystemException {
        UserContextDto user = JwtUser.getAuthenticatedUser();
        OptionEntity entity = getDao().getEntityById(id, user.getId());
        try {
            if (entity != null) {
                deleteById(id);
            }
        } catch (Exception e) {
            throw new SystemException(SystemError.ILLEGAL_REQUEST, "can't delete ", 6503);
        }
        return true;
    }

    @Override
    public ReportFilter filter(OptionFilter filter) {
        ReportOption reportOption = new ReportOption();
        if (filter instanceof OptionPageableFilter) {
            OptionPageableFilter pageableFilter = (OptionPageableFilter) filter;
            reportOption.setPageNumber(pageableFilter.getPage());
            reportOption.setPageSize(reportOption.getPageSize());
            reportOption.setSortOptions(pageableFilter.getSort());
        }
        ReportCondition reportCondition = new ReportCondition();
        reportCondition.addEqualCondition("id", filter.getId());
        reportCondition.addLikeCondition("uniqCode", filter.getUniqCode());
        reportCondition.addEqualCondition("selectionId", filter.getSelectionId());
        reportCondition.addCaseInsensitiveLikeCondition("title", filter.getTitle());
        return new ReportFilter(reportCondition, reportOption);
    }

}
