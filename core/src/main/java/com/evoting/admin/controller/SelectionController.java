package com.evoting.admin.controller;

import com.evoting.admin.dto.party.InvitationResultOut;
import com.evoting.admin.dto.selection.SelectionEditIn;
import com.evoting.admin.dto.selection.SelectionFilter;
import com.evoting.admin.dto.selection.SelectionIn;
import com.evoting.admin.dto.selection.SelectionPageableFilter;
import com.evoting.admin.repository.service.SelectionService;
import com.evoting.pub.dto.SelectionOut;
import com.evoting.statics.CoreRest;
import com.evoting.utility.model.object.SystemException;
import com.evoting.utility.statics.constants.RestApi;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Api(tags = "Admin Selection Controller")
@RestController
@RequestMapping(value = RestApi.REST_ADMIN)
@Validated
public class SelectionController {
    private final SelectionService service;

    @Autowired
    public SelectionController(SelectionService service) {
        this.service = service;
    }

    @ApiOperation(value = "get all instance match with filter", response = SelectionOut.class, responseContainer = "List")
    @GetMapping(value = CoreRest.SELECTION, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<SelectionOut>> getAll(SelectionPageableFilter filter, BindingResult bindingResult, @RequestParam(name = "include", required = false) String[] include, HttpServletRequest request) throws SystemException {
        return new ResponseEntity(service.getAll(filter, include), HttpStatus.OK);
    }

    @ApiOperation(value = "get all instance match with filter", response = Integer.class)
    @GetMapping(value = CoreRest.SELECTION_COUNT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Integer> count(SelectionFilter filter, BindingResult bindingResult, HttpServletRequest request) throws SystemException {
        return new ResponseEntity<>(service.count(filter), HttpStatus.OK);
    }

    @ApiOperation(value = "get all instance match with filter", response = SelectionOut.class)
    @GetMapping(value = CoreRest.SELECTION_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SelectionOut> getById(@PathVariable Integer id, @RequestParam(value = "include", required = false) String[] include, HttpServletRequest request) throws SystemException {
        return new ResponseEntity(service.getById(id, include), HttpStatus.OK);
    }

    @ApiOperation(value = "get all instance match with filter", response = SelectionOut.class)
    @PostMapping(value = CoreRest.SELECTION, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SelectionOut> create(@Valid @RequestBody SelectionIn selectionIn, BindingResult bindingResult, HttpServletRequest request) throws SystemException {
        return new ResponseEntity(service.create(selectionIn), HttpStatus.OK);
    }

    @ApiOperation(value = "get all instance match with filter", response = SelectionOut.class)
    @PutMapping(value = CoreRest.SELECTION_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SelectionOut> update(@Valid @PathVariable Integer id, @RequestBody SelectionIn selectionIn, BindingResult bindingResult, HttpServletRequest request) throws SystemException {
        return new ResponseEntity(service.update(id, selectionIn), HttpStatus.OK);
    }

    @ApiOperation(value = "get all instance match with filter", response = SelectionOut.class)
    @PutMapping(value = CoreRest.SELECTION_ID_EXTEND, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SelectionOut> extend(@Valid @PathVariable Integer id, @RequestBody SelectionEditIn selectionIn, BindingResult bindingResult, HttpServletRequest request) throws SystemException {
        return new ResponseEntity(service.extend(id, selectionIn), HttpStatus.OK);
    }

    @ApiOperation(value = "get all instance match with filter", response = SelectionOut.class)
    @PostMapping(value = CoreRest.SELECTION_ID_VOTE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void checkVote(@Valid @PathVariable Integer id, HttpServletRequest request) throws SystemException {
        service.updateOptionVoteCount(id);
    }

    @ApiOperation(value = "get all instance match with filter", response = SelectionOut.class)
    @PatchMapping(value = CoreRest.SELECTION_ID_FINISH, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Boolean> finish(@Valid @PathVariable Integer id, HttpServletRequest request) throws SystemException {
        return new ResponseEntity(service.finishSelection(id), HttpStatus.OK);
    }

    @ApiOperation(value = "get all instance match with filter", response = Boolean.class)
    @DeleteMapping(value = CoreRest.SELECTION_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Boolean> delete(@PathVariable Integer id, HttpServletRequest request) throws SystemException {
        return new ResponseEntity<>(service.delete(id), HttpStatus.OK);
    }

    @ApiOperation(value = "invite users in selection", response = InvitationResultOut.class)
    @PostMapping(value = CoreRest.SELECTION_ID_INVITE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<InvitationResultOut> invite(@PathVariable Integer id, @RequestBody List<Integer> partyIds, HttpServletRequest request) throws SystemException {
        return new ResponseEntity<>(service.sendInvitationForSelection(id, partyIds), HttpStatus.OK);
    }

}
