package com.evoting.admin.controller;

import com.evoting.admin.dto.option.OptionFilter;
import com.evoting.admin.dto.option.OptionIn;
import com.evoting.admin.dto.option.OptionOut;
import com.evoting.admin.dto.option.OptionPageableFilter;
import com.evoting.admin.repository.service.OptionService;
import com.evoting.statics.CoreRest;
import com.evoting.utility.model.object.SystemException;
import com.evoting.utility.statics.constants.RestApi;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Api(tags = "Admin Option Controller")
@RestController
@RequestMapping(value = RestApi.REST_ADMIN)
@Validated
public class OptionController {

    private final OptionService optionService;

    @Autowired
    public OptionController(OptionService optionService) {
        this.optionService = optionService;
    }

    @ApiOperation(value = "Get All Option Matching Filter", response = OptionOut.class, responseContainer = "List")
    @GetMapping(value = CoreRest.OPTION, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<OptionOut>> getAll(@Valid OptionPageableFilter filter, BindingResult bindingResult,@RequestParam(name = "include",required = false) String[] include, HttpServletRequest request) {
        return new ResponseEntity<>(optionService.getAll(filter, include), HttpStatus.OK);
    }

    @ApiOperation(value = "count Option Matching Filter", response = Integer.class)
    @GetMapping(value = CoreRest.OPTION_COUNT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Integer> count(OptionFilter filter, HttpServletRequest request) {
        return new ResponseEntity<>(optionService.count(filter), HttpStatus.OK);
    }

    @ApiOperation(value = "create one instance of option", response = OptionOut.class)
    @PostMapping(value = CoreRest.OPTION, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OptionOut> create(@Valid @RequestBody OptionIn optionIn, BindingResult bindingResult, HttpServletRequest request) throws SystemException {
        return new ResponseEntity<>(optionService.create(optionIn), HttpStatus.OK);
    }

    @ApiOperation(value = "update one instance of option", response = OptionOut.class)
    @PutMapping(value = CoreRest.OPTION_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OptionOut> update(@PathVariable Integer id, @Valid @RequestBody OptionIn optionIn, BindingResult bindingResult, HttpServletRequest request) throws SystemException {
        return new ResponseEntity<>(optionService.update(id, optionIn), HttpStatus.OK);
    }
    @ApiOperation(value = "get one instance of option", response = OptionOut.class)
    @GetMapping(value = CoreRest.OPTION_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OptionOut> getById(@PathVariable Integer id, HttpServletRequest request) throws SystemException {
        return new ResponseEntity<>(optionService.getById(id), HttpStatus.OK);
    }
    @ApiOperation(value = "delete one instance of option", response = Boolean.class)
    @DeleteMapping(value = CoreRest.OPTION_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Boolean> delete(@PathVariable Integer id, HttpServletRequest request) throws SystemException {
        return new ResponseEntity<>(optionService.delete(id), HttpStatus.OK);
    }
}
