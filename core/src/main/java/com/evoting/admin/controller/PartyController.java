package com.evoting.admin.controller;

import com.evoting.admin.dto.party.PartyFilter;
import com.evoting.admin.dto.party.PartyIn;
import com.evoting.admin.dto.party.PartyOut;
import com.evoting.admin.dto.party.PartyPageableFilter;
import com.evoting.admin.repository.service.PartyService;
import com.evoting.statics.CoreRest;
import com.evoting.utility.model.object.SystemException;
import com.evoting.utility.statics.constants.RestApi;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Api(tags = "Admin Party Controller")
@RestController
@RequestMapping(value = RestApi.REST_ADMIN)
@Validated
public class PartyController {
    private final PartyService partyService;

    @Autowired
    public PartyController(PartyService partyService) {
        this.partyService = partyService;
    }

    @ApiOperation(value = "get all instance match with filter", response = PartyOut.class, responseContainer = "List")
    @GetMapping(value = CoreRest.PARTY, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<PartyOut>> getAll(@Valid PartyPageableFilter filter, BindingResult bindingResult, @RequestParam(name = "include", required = false) String[] include, HttpServletRequest request) throws SystemException {
        return new ResponseEntity<>(partyService.getAll(filter, include), HttpStatus.OK);
    }

    @ApiOperation(value = "count all instance match with filter", response = Integer.class)
    @GetMapping(value = CoreRest.PARTY_COUNT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Integer> count(@Valid PartyFilter filter, BindingResult bindingResult, HttpServletRequest request) throws SystemException {
        return new ResponseEntity<>(partyService.count(filter), HttpStatus.OK);
    }

    @ApiOperation(value = "get an instance with {id}", response = PartyOut.class)
    @GetMapping(value = CoreRest.PARTY_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PartyOut> getById(@PathVariable("id") Integer id, @RequestParam(name = "include", required = false) String[] include, HttpServletRequest request) throws SystemException {
        return new ResponseEntity<>(partyService.getById(id, include), HttpStatus.OK);
    }

    @ApiOperation(value = "update model with have id ", response = PartyOut.class)
    @PutMapping(value = CoreRest.PARTY_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PartyOut> update(@PathVariable("id") Integer id,
                                           @RequestBody PartyIn partyIn, BindingResult bindingResult, HttpServletRequest request) throws SystemException {
        return new ResponseEntity<>(partyService.update(id, partyIn), HttpStatus.OK);
    }

    @ApiOperation(value = "create an instance with in model", response = PartyOut.class)
    @PostMapping(value = CoreRest.PARTY, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PartyOut> create(@RequestBody PartyIn partyIn, BindingResult bindingResult, HttpServletRequest request) throws SystemException {
        return new ResponseEntity<>(partyService.create(partyIn), HttpStatus.OK);
    }

    @ApiOperation(value = "delete instance with {id}", response = Boolean.class)
    @DeleteMapping(value = CoreRest.PARTY_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Boolean> delete(@PathVariable Integer id, HttpServletRequest request) throws SystemException {
        return new ResponseEntity<>(partyService.delete(id), HttpStatus.OK);
    }

}
