package com.evoting.admin.controller;

import com.evoting.admin.dto.party.UserPartyInfo;
import com.evoting.admin.dto.party.user.UserInPartyPageableFilter;
import com.evoting.admin.dto.party.user.UserPartyRegisterIn;
import com.evoting.admin.dto.party.user.UserToPartyResult;
import com.evoting.admin.repository.service.UserInPartyService;
import com.evoting.entity.security.statics.TerminalType;
import com.evoting.statics.CoreRest;
import com.evoting.utility.model.object.SystemException;
import com.evoting.utility.statics.constants.RestApi;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.MediaType.MULTIPART_FORM_DATA_VALUE;

@Api(tags = "Admin UserInParty Controller")
@RestController
@RequestMapping(value = RestApi.REST_ADMIN)
@Validated
public class UserInPartyController {
    private final UserInPartyService service;


    @Autowired
    public UserInPartyController(UserInPartyService service) {
        this.service = service;
    }


    @ApiOperation(value = "add user to party")
    @PostMapping(value = CoreRest.USER_IN_PARTY, produces = MediaType.APPLICATION_JSON_VALUE)
    public void addUserToParty(@PathVariable("partyId") Integer id,
                               @RequestBody UserPartyRegisterIn registerIn,
                               BindingResult bindingResult,
                               @PathVariable(name = "terminalType") TerminalType terminalType,
                               HttpServletRequest request) throws SystemException {
        service.addUserToParty(id, registerIn, terminalType);
    }


    @ApiOperation(value = "add user to party")
    @DeleteMapping(value = CoreRest.USER_IN_PARTY_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public void removeFromParty(@PathVariable("partyId") Integer partyId,
                                @PathVariable("userId") Integer userId,
                                HttpServletRequest request) throws SystemException {
        service.removeUserFromParty(userId, partyId);
    }

    @ApiOperation(value = "add users to party")
    @PostMapping(value = CoreRest.USER_IN_PARTY_GROUP, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserToPartyResult> addUsersToParty(@PathVariable("partyId") Integer id,
                                                             @RequestBody List<UserPartyRegisterIn> registerIns,
                                                             BindingResult bindingResult,
                                                             HttpServletRequest request) throws SystemException {
        return new ResponseEntity<>(service.addUsersToParty(id, registerIns), HttpStatus.OK);
    }

    @ApiOperation(value = "add users to party with file")
    @PostMapping(value = CoreRest.USER_IN_PARTY_FILE, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<UserToPartyResult> addUsersToPartyWithFile(@PathVariable("partyId") Integer id,
                                                                     @RequestBody MultipartFile file,
                                                                     HttpServletRequest request) throws SystemException {
        return new ResponseEntity<>(service.addUsersToParty(id, file), HttpStatus.OK);
    }

    @ApiOperation(value = "add users to party with file")
    @GetMapping(value = CoreRest.USER_IN_PARTY_GET_ALL, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<UserPartyInfo>> getAll(@Valid UserInPartyPageableFilter filter,
                                                      BindingResult bindingResult,
                                                      @RequestParam(value = "include", required = false) String[] include,
                                                      HttpServletRequest request) throws SystemException {
        return new ResponseEntity<>(service.getAll(filter, include), HttpStatus.OK);
    }

    @ApiOperation(value = "add users to party with file")
    @GetMapping(value = CoreRest.USER_IN_PARTY_COUNT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Integer> count(@Valid UserInPartyPageableFilter filter,
                                         BindingResult bindingResult,
                                         HttpServletRequest request) throws SystemException {
        return new ResponseEntity<>(service.count(filter), HttpStatus.OK);
    }

}
