package com.evoting.admin.dto.option;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class OptionIn {
    @NotNull
    private String title;
    private String description;
    private String uniqCode;
    private String fields;
    @NotNull
    private Integer selectionId;
}
