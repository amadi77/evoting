package com.evoting.admin.dto.selection;

import com.evoting.utility.repository.service.FilterBase;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
public class SelectionFilter implements FilterBase {
    private String name;
    private LocalDateTime deadlineMin;
    private LocalDateTime deadlineMax;
    private LocalDateTime fromDateMin;
    private LocalDateTime fromDateMax;
    private LocalDateTime toDateMin;
    private LocalDateTime toDateMax;
    private Integer id;
    private Integer ownerId;
}
