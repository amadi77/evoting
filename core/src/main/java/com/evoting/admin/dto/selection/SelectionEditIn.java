package com.evoting.admin.dto.selection;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Setter
@Getter
public class SelectionEditIn {
    @NotNull
    private LocalDateTime toDate;
}
