package com.evoting.admin.dto.party;

import com.evoting.utility.config.StaticApplicationContext;
import com.evoting.entity.security.entity.party.PartyEntity;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.Hibernate;
import org.modelmapper.ModelMapper;

import java.util.List;
import java.util.stream.Collectors;

@Setter
@Getter
@NoArgsConstructor
public class PartyOut extends PartyIn{
    private Integer id;
    @Setter(AccessLevel.PRIVATE)
    private List<UserPartyInfo> members;

    public PartyOut(PartyEntity entity) {
        if (entity!=null){
            ModelMapper modelMapper = StaticApplicationContext.getContext().getBean(ModelMapper.class);
            modelMapper.map(entity,this);
            if (Hibernate.isInitialized(entity.getMembers())){
                this.members = entity.getMembers().stream().map(UserPartyInfo::new).collect(Collectors.toList());
            }
        }
    }
}
