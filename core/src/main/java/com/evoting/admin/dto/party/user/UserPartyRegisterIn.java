package com.evoting.admin.dto.party.user;

import com.evoting.utility.bl.NormalizeEngine;
import com.evoting.utility.bl.ValidationEngine;
import com.evoting.utility.model.object.IValidation;
import com.evoting.utility.model.object.SystemException;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserPartyRegisterIn implements IValidation {
    private String mobile;
    private String email;
    private Integer id;

    public void setMobile(String mobile) throws SystemException {
        this.mobile = NormalizeEngine.getNormalizedPhoneNumber(mobile);
    }

    public void setEmail(String email) throws SystemException {
        ValidationEngine.validateEmail(email);
        this.email = email;
    }

    @Override
    public void validate() throws SystemException {
        if (mobile != null) {
            setMobile(mobile);
        }
        if (email != null) {
            setEmail(email);
        }
    }
}
