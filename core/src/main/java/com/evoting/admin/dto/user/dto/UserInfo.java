package com.evoting.admin.dto.user.dto;

import com.evoting.entity.security.entity.UserEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class UserInfo {
    private Integer userId;
    private String fullName;

    public UserInfo(UserEntity entity) {
        this.userId = entity.getId();
        this.fullName = entity.getFullName();
    }
}
