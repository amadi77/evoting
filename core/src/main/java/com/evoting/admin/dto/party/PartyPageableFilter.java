package com.evoting.admin.dto.party;

import com.evoting.utility.model.object.SortOption;
import com.evoting.utility.statics.enums.SortType;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class PartyPageableFilter extends PartyFilter{
    private int size;
    private int page;
    private List<SortOption> sort;

    public PartyPageableFilter() {
        this.size = 10;
        this.page = 1;
        this.sort = new ArrayList<>();
        this.sort.add(new SortOption("id", SortType.DESCENDING));
    }
}
