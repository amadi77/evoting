package com.evoting.admin.dto.option;

import com.evoting.entity.core.entity.OptionEntity;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class OptionInfo {
    private Integer id;
    private String title;

    public OptionInfo(OptionEntity entity) {
        if (entity != null) {
            this.id = entity.getId();
            this.title = entity.getTitle();
        }
    }
}
