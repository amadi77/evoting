package com.evoting.admin.dto.party;

import com.evoting.utility.repository.service.FilterBase;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PartyFilter implements FilterBase {
    private Integer id;
    private String name;
    private Integer userId;
}
