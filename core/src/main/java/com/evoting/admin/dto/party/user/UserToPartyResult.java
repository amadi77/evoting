package com.evoting.admin.dto.party.user;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserToPartyResult {
    private int success;
    private int duplicate;

    public UserToPartyResult(int success, int duplicate) {
        this.success = success;
        this.duplicate = duplicate;
    }
}
