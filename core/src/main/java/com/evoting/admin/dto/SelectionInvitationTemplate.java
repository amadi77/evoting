package com.evoting.admin.dto;

import com.evoting.entity.core.entity.SelectionEntity;
import com.evoting.utility.bl.time.JalaliService;
import com.evoting.utility.config.StaticApplicationContext;
import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;
import java.time.LocalDateTime;

@Setter
@Getter
public class SelectionInvitationTemplate {
    private String selectionName;
    private Timestamp fromDate;
    private Timestamp toDate;

    public SelectionInvitationTemplate(SelectionEntity entity) {
        JalaliService jalali = StaticApplicationContext.getContext().getBean(JalaliService.class);
        this.selectionName = entity.getName();
//        this.fromDate = jalali.timestampToJalali(Timestamp.valueOf(entity.getFromDate()));
//        this.toDate = entity.getToDate();
    }
}
