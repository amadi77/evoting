package com.evoting.admin.dto.party;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PartyIn{
    private String name;
}
