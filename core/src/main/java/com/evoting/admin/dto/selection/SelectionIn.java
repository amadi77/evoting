package com.evoting.admin.dto.selection;

import com.evoting.utility.model.object.IValidation;
import com.evoting.utility.model.object.SystemError;
import com.evoting.utility.model.object.SystemException;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Setter
@Getter
public class SelectionIn extends SelectionEditIn implements IValidation {
    @NotNull
    private String name;
    @NotNull
    private Integer limitVote;
    @NotNull
    private LocalDateTime fromDate;
    @NotNull
    private LocalDateTime deadline;
    @NotNull
    private Integer[] partyIds;

    @Override
    public void validate() throws SystemException {
        if (deadline.isAfter(fromDate)) {
            throw new SystemException(SystemError.ILLEGAL_REQUEST, "deadline can't be after from date", 2201);
        }
        if (getToDate() != null && getToDate().isBefore(fromDate)) {
            throw new SystemException(SystemError.ILLEGAL_REQUEST, "toDate can't be after from date", 2202);
        }
    }
}
