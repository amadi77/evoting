package com.evoting.admin.dto.vote;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class OptionVoteCountResult {
    private Integer optionId;
    private Integer voteCount;

    public OptionVoteCountResult(Long voteCount, Integer optionId) {
        this.optionId = optionId;
        this.voteCount = voteCount.intValue();
    }
}
