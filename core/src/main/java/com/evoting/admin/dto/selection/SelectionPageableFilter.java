package com.evoting.admin.dto.selection;

import com.evoting.utility.model.object.SortOption;
import com.evoting.utility.statics.enums.SortType;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class SelectionPageableFilter extends SelectionFilter {
    private Integer size;
    private Integer page;
    private List<SortOption> sort;

    public SelectionPageableFilter() {
        this.size = 20;
        this.page = 1;
        this.sort = new ArrayList<>();
        sort.add(new SortOption("id", SortType.DESCENDING));
    }
}
