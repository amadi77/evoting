package com.evoting.admin.dto.party.user;

import com.evoting.utility.model.object.SortOption;
import com.evoting.utility.statics.enums.SortType;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class UserInPartyPageableFilter extends UserInPartyFilter{
    private int size;
    private int page;
    private List<SortOption> sort;

    public UserInPartyPageableFilter() {
        this.size = 10;
        this.page = 1;
        this.sort = new ArrayList<>();
        this.sort.add(new SortOption("id", SortType.DESCENDING));
    }
}
