package com.evoting.admin.dto.option;

import com.evoting.utility.repository.service.FilterBase;
import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class OptionFilter implements FilterBase {
    private Integer id;
    @NotNull
    private Integer selectionId;
    private String uniqCode;
    private String title;
}
