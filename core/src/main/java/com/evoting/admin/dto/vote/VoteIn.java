package com.evoting.admin.dto.vote;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class VoteIn {
    private Integer[] optionIds;
}
