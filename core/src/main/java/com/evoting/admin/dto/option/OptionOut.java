package com.evoting.admin.dto.option;

import com.evoting.entity.core.entity.OptionEntity;
import com.evoting.admin.dto.selection.SelectionInfo;
import com.evoting.admin.dto.vote.OptionVoteCountResult;
import com.evoting.utility.config.StaticApplicationContext;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.Hibernate;
import org.modelmapper.ModelMapper;

import java.util.List;

@Setter
@Getter
public class OptionOut extends OptionIn {
    private Integer id;
    @Setter(AccessLevel.PRIVATE)
    private SelectionInfo selection;
    private int voteCount;

    public OptionOut(OptionEntity entity) {
        if (entity != null) {
            ModelMapper modelMapper = StaticApplicationContext.getContext().getBean(ModelMapper.class);
            modelMapper.map(entity, this);
            if (Hibernate.isInitialized(entity.getSelection())) {
                this.selection = new SelectionInfo(entity.getSelection());
            }

        }
    }

    public OptionOut(OptionEntity entity, List<OptionVoteCountResult> optionVoteCount) {
        if (entity != null) {
            ModelMapper modelMapper = StaticApplicationContext.getContext().getBean(ModelMapper.class);
            modelMapper.map(entity, this);
            optionVoteCount.stream().filter(o -> o.getOptionId().equals(this.id)).findFirst().ifPresent(o -> this.voteCount = o.getVoteCount());
            if (Hibernate.isInitialized(entity.getSelection())) {
                this.selection = new SelectionInfo(entity.getSelection());
            }

        }
    }
}
