package com.evoting.admin.dto.party.user;

import com.evoting.utility.repository.service.FilterBase;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Setter
@Getter
public class UserInPartyFilter implements FilterBase {
    private Integer id;
    @NotNull
    private Integer partyId;
    private Integer userId;
    private Integer ownerId;
    private String mobile;
    private String email;
}
