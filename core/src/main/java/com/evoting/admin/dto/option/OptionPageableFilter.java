package com.evoting.admin.dto.option;

import com.evoting.utility.model.object.SortOption;
import com.evoting.utility.statics.enums.SortType;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class OptionPageableFilter extends OptionFilter {
    private Integer size;
    private Integer page;
    private List<SortOption> sort;

    public OptionPageableFilter() {
        this.size = 20;
        this.page = 1;
        this.sort = new ArrayList<>();
        this.sort.add(new SortOption("id", SortType.DESCENDING));
    }
}
