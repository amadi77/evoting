package com.evoting.admin.dto.selection;

import com.evoting.admin.dto.option.OptionOut;
import com.evoting.admin.dto.party.PartyInfo;
import com.evoting.admin.dto.vote.OptionVoteCountResult;
import com.evoting.entity.core.entity.SelectionEntity;
import com.evoting.utility.config.StaticApplicationContext;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.Hibernate;
import org.modelmapper.ModelMapper;

import java.util.List;
import java.util.stream.Collectors;

@Setter
@Getter
@NoArgsConstructor
public class SelectionOut extends SelectionIn {
    private Integer id;
    @Setter(AccessLevel.PRIVATE)
    private List<OptionOut> options;
    @Setter(AccessLevel.PRIVATE)
    private List<PartyInfo> parties;
    private Boolean cheating;
    private Boolean finished;

    public SelectionOut(SelectionEntity entity) {
        if (entity != null) {
            ModelMapper modelMapper = StaticApplicationContext.getContext().getBean(ModelMapper.class);
            modelMapper.map(entity, this);
           /* if (Hibernate.isInitialized(entity.getOptions())) {
                this.options = entity.getOptions().stream().map(OptionOut::new).collect(Collectors.toList());
            }*/
            if (Hibernate.isInitialized(entity.getParties()) && entity.getParties() != null) {
                this.parties = entity.getParties().stream().map(PartyInfo::new).collect(Collectors.toList());
                Integer[] partyIds = new Integer[]{};
                partyIds = parties.stream().map(PartyInfo::getId).collect(Collectors.toList()).toArray(partyIds);
                this.setPartyIds(partyIds);
            }
        }
    }

    public SelectionOut(SelectionEntity entity, List<OptionVoteCountResult> optionVoteCount) {
        if (entity != null) {
            ModelMapper modelMapper = StaticApplicationContext.getContext().getBean(ModelMapper.class);
            modelMapper.map(entity, this);
            if (Hibernate.isInitialized(entity.getOptions()) && entity.getOptions() != null) {
                this.options = entity.getOptions().stream().map(o -> new OptionOut(o, optionVoteCount)).collect(Collectors.toList());
            }
            if (Hibernate.isInitialized(entity.getParties()) && entity.getParties() != null) {
                this.parties = entity.getParties().stream().map(PartyInfo::new).collect(Collectors.toList());
                Integer[] partyIds = new Integer[]{};
                partyIds = parties.stream().map(PartyInfo::getId).collect(Collectors.toList()).toArray(partyIds);
                this.setPartyIds(partyIds);
            }
        }
    }
}
