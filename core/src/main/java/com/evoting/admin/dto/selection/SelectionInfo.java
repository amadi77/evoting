package com.evoting.admin.dto.selection;

import com.evoting.entity.core.entity.SelectionEntity;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class SelectionInfo {
    private Integer id;
    private String name;

    public SelectionInfo(SelectionEntity entity) {
        if (entity != null) {
            this.id = entity.getId();
            this.name = entity.getName();
        }
    }
}
