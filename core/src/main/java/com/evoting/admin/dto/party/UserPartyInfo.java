package com.evoting.admin.dto.party;

import com.evoting.entity.security.entity.party.UserInPartyEntity;
import com.evoting.admin.dto.user.dto.UserInfo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class UserPartyInfo extends UserInfo {
    private String email;
    private String phoneNumber;

    public UserPartyInfo(UserInPartyEntity entity) {
        super(entity.getUser());
        switch (entity.getTerminal()) {
            case ID:
                break;
            case USER:
                this.phoneNumber = entity.getUser().getPhoneNumber();
                this.email = entity.getUser().getEmail();
                break;
            case EMAIL:
                this.email = entity.getUser().getEmail();
                break;
            case PHONE:
                this.phoneNumber = entity.getUser().getPhoneNumber();
                break;
            case OTHER:
                break;
            default:
        }
    }
}
