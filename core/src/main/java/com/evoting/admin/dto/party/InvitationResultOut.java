package com.evoting.admin.dto.party;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class InvitationResultOut {
    private int invited;
    private int failed;

    public void increaseInvited() {
        invited++;
    }

    public void increaseFailed() {
        failed++;
    }
}
