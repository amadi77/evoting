package com.evoting.statics;

public class CoreRest {
    public static final String OPTION = "/option";
    public static final String OPTION_COUNT = "/option/count";
    public static final String OPTION_ID = "/option/{id}";
    public static final String OPTION_INFO = "/option/info";

    public static final String PARTY = "/party";
    public static final String PARTY_COUNT = "/party/count";
    public static final String PARTY_ID = "/party/{id}";
    public static final String PARTY_INFO = "/party/info";

    public static final String VOTE = "/vote";

    public static final String SELECTION = "/selection";
    public static final String SELECTION_COUNT = "/selection/count";
    public static final String SELECTION_ID = "/selection/{id}";
    public static final String SELECTION_ID_EXTEND = "/selection/{id}/extend";
    public static final String SELECTION_ID_VOTE = "/selection/{id}/vote";
    public static final String SELECTION_ID_INVITE = "/selection/{id}/invite";
    public static final String SELECTION_ID_FINISH = "/selection/{id}/finish";
    public static final String SELECTION_INFO = "/selection/info";


    public static final String USER_IN_PARTY_GET_ALL = "/user/party";
    public static final String USER_IN_PARTY_COUNT = "/user/party/count";
    public static final String USER_IN_PARTY = "/user/party/{partyId}/terminal/{terminalType}";
    public static final String USER_IN_PARTY_GROUP = "/user/party/{partyId}/group";
    public static final String USER_IN_PARTY_ID = "/user/party/{partyId}/user/{userId}";
    public static final String USER_IN_PARTY_FILE = "/user/party/{partyId}/file";
}
