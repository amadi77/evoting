package com.evoting.statics;

public abstract class Template {
    public final static String SEND_INVITATION_SUBJECT = "template/selection-invitation-subject";
    public final static String SEND_INVITATION_EMAIL = "template/selection-invitation-email";
}
