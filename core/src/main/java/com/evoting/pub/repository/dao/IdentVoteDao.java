package com.evoting.pub.repository.dao;

import com.evoting.entity.core.entity.VoteEntity;
import com.evoting.pub.dto.SelectionVoteResult;
import com.evoting.utility.repository.Dao;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class IdentVoteDao extends Dao<VoteEntity> {

    public List<SelectionVoteResult> canVoteByUserIdAndSelectionIds(int userId, List<Integer> selectionIds) {
        Query query = this.getEntityManager().createQuery("select new com.evoting.pub.dto.SelectionVoteResult(coalesce(count(votes.id),0),votes.selectionId) from VoteEntity votes where votes.userId=:userId and votes.selectionId in (:selectionIds) group by votes.selectionId");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("userId", userId);
        parameters.put("selectionIds", selectionIds);

        return queryHql(query, parameters);
    }

    public SelectionVoteResult canVoteByUserIdAndSelectionId(int userId, Integer selectionId) {
        Query query = this.getEntityManager().createQuery("select new com.evoting.pub.dto.SelectionVoteResult(coalesce(count(votes.id),0),votes.selectionId) from VoteEntity votes where votes.userId=:userId and votes.selectionId = :selectionId group by votes.selectionId");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("userId", userId);
        parameters.put("selectionId", selectionId);
        List<SelectionVoteResult> results = queryHql(query, parameters);
        return results.isEmpty() ? null : results.get(0);
    }
}
