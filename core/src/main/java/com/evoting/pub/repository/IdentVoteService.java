package com.evoting.pub.repository;

import com.evoting.admin.repository.service.OptionService;
import com.evoting.admin.security.authentication.filter.JwtUser;
import com.evoting.admin.security.object.UserContextDto;
import com.evoting.entity.core.entity.OptionEntity;
import com.evoting.entity.core.entity.SelectionEntity;
import com.evoting.entity.core.entity.VoteEntity;
import com.evoting.pub.dto.SelectionVoteResult;
import com.evoting.pub.dto.VoteFilter;
import com.evoting.pub.dto.VoteIn;
import com.evoting.pub.repository.dao.IdentVoteDao;
import com.evoting.utility.bl.HashService;
import com.evoting.utility.model.object.ReportFilter;
import com.evoting.utility.model.object.SystemError;
import com.evoting.utility.model.object.SystemException;
import com.evoting.utility.model.object.report.ReportCondition;
import com.evoting.utility.model.object.report.ReportOption;
import com.evoting.utility.repository.AbstractFilterableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class IdentVoteService extends AbstractFilterableService<VoteEntity, VoteFilter, IdentVoteDao> {
    private final IdentSelectionService selectionService;
    private final HashService hashService;
    private final OptionService optionService;

    @Autowired
    public IdentVoteService(@Lazy IdentSelectionService selectionService, IdentVoteDao dao, HashService hashService, OptionService optionService) {
        super(dao);
        this.selectionService = selectionService;
        this.hashService = hashService;
        this.optionService = optionService;
    }

    public int count(VoteFilter filter) {
        return countEntity(filter(filter));
    }

    public List<SelectionVoteResult> checkCanVoteToSelectionOrNot(List<Integer> selectionIds) throws SystemException {
        UserContextDto user = JwtUser.getAuthenticatedUser();
        return getDao().canVoteByUserIdAndSelectionIds(user.getId(), selectionIds);
    }

    public SelectionVoteResult checkCanVoteToSelectionOrNot(Integer selectionId) throws SystemException {
        UserContextDto user = JwtUser.getAuthenticatedUser();
        return getDao().canVoteByUserIdAndSelectionId(user.getId(), selectionId);
    }

    public void vote(VoteIn vote) throws SystemException {
        UserContextDto user = JwtUser.getAuthenticatedUser();
        SelectionEntity selection = selectionService.getEntityById(vote.getSelectionId());

        Set<Integer> optionIds = new HashSet<>(vote.getOptionIds());

        if (selection.getFromDate().isAfter(LocalDateTime.now())) {
            throw new SystemException(SystemError.VIOLATION_ERROR, "selection hasn't started", 1862);
        }
        if (selection.getToDate().isBefore(LocalDateTime.now()) || selection.isFinished()) {
            throw new SystemException(SystemError.VIOLATION_ERROR, "selection was finish", 1863);
        }
        if (optionIds.size() > selection.getLimitVote()) {
            throw new SystemException(SystemError.VIOLATION_ERROR, "max vote count is bigger than max ", 1864);
        }
        VoteFilter voteFilter = new VoteFilter();
        voteFilter.setSelectionId(vote.getSelectionId());
        voteFilter.setUserId(user.getId());
        if (count(voteFilter) != 0) {
            throw new SystemException(SystemError.VIOLATION_ERROR, "already voted", 1861);
        }

        List<OptionEntity> options = optionService.getEntitiesByIds(vote.getOptionIds());
        options.forEach(o -> o.setVoteCount(o.getVoteCount() + 1));
        List<VoteEntity> votes = new ArrayList<>();
        for (Integer eachOption : optionIds) {
            VoteEntity voteEntity = new VoteEntity();
            voteEntity.setOptionId(eachOption);
            voteEntity.setSelectionId(vote.getSelectionId());
            voteEntity.setUserId(user.getId());
            hashVote(voteEntity);
            votes.add(voteEntity);
        }
//        todo hold vote count of each option on that or not?
        saveOrUpdateEntityCollection(votes);
        optionService.saveOrUpdateEntityCollection(options);
    }

    private void hashVote(VoteEntity voteEntity) throws SystemException {
        String builder =
                "option_" +
                        voteEntity.getOptionId() +
                        "selection_" +
                        voteEntity.getSelectionId() +
                        "user_" +
                        voteEntity.getUserId();
        voteEntity.setVoteHash(hashService.encrypt(builder));
    }

    @Override
    public ReportFilter filter(VoteFilter filter) {
        ReportOption reportOption = new ReportOption();
        ReportCondition reportCondition = new ReportCondition();
        reportCondition.addEqualCondition("userId", filter.getUserId());
        reportCondition.addEqualCondition("selectionId", filter.getSelectionId());
        reportCondition.addEqualCondition("optionId", filter.getOptionId());
        return new ReportFilter(reportCondition, reportOption);
    }
}
