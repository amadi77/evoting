package com.evoting.pub.repository;

import com.evoting.admin.security.authentication.filter.JwtUser;
import com.evoting.admin.security.object.UserContextDto;
import com.evoting.entity.core.entity.SelectionEntity;
import com.evoting.pub.dto.SelectionFilter;
import com.evoting.pub.dto.SelectionOut;
import com.evoting.pub.dto.SelectionPageableFilter;
import com.evoting.pub.dto.SelectionVoteResult;
import com.evoting.pub.repository.dao.IdentSelectionDao;
import com.evoting.utility.model.object.ReportFilter;
import com.evoting.utility.model.object.SystemError;
import com.evoting.utility.model.object.SystemException;
import com.evoting.utility.model.object.report.ReportCondition;
import com.evoting.utility.model.object.report.ReportCriteriaJoinCondition;
import com.evoting.utility.model.object.report.ReportOption;
import com.evoting.utility.repository.AbstractFilterableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.JoinType;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class IdentSelectionService extends AbstractFilterableService<SelectionEntity, SelectionFilter, IdentSelectionDao> {
    private final IdentVoteService voteService;

    @Autowired
    public IdentSelectionService(IdentSelectionDao dao, IdentVoteService voteService) {
        super(dao);
        this.voteService = voteService;
    }

    public List<SelectionEntity> getAllEntities(SelectionPageableFilter filter, String[] include) throws SystemException {
        UserContextDto user = JwtUser.getAuthenticatedUser();
        filter.setUserId(user.getId());
        return getAllEntities(filter(filter), null);
    }

    public List<SelectionOut> getAll(SelectionPageableFilter filter) throws SystemException {
        String[] include = {"parties"};
        List<SelectionEntity> selectionEntities = getAllEntities(filter, include);
        List<Integer> selectionIds = selectionEntities.stream().map(SelectionEntity::getId).collect(Collectors.toList());
        List<SelectionVoteResult> selectionResult = voteService.checkCanVoteToSelectionOrNot(selectionIds);

        return selectionEntities.stream().map(o -> new SelectionOut(o, selectionResult)).collect(Collectors.toList());
    }

    public SelectionEntity getEntityById(int selectionId) throws SystemException {
        UserContextDto user = JwtUser.getAuthenticatedUser();
        SelectionEntity entity = getDao().getByIdAndUserId(selectionId, user.getId());
        if (entity == null) {
            throw new SystemException(SystemError.DATA_NOT_FOUND, "selection not found:" + selectionId, 1851);
        }
        return entity;
    }

    public SelectionOut getById(int id) throws SystemException {
        SelectionVoteResult selectionVoteResult = voteService.checkCanVoteToSelectionOrNot(id);
        if (selectionVoteResult!=null && selectionVoteResult.getVoted()) {
            throw new SystemException(SystemError.ILLEGAL_REQUEST, "can't see options again", 1852);
        }
        SelectionEntity entity = getEntityById(id);
        return new SelectionOut(entity);
    }

    @Override
    public ReportFilter filter(SelectionFilter filter) {
        ReportOption reportOption = new ReportOption();
        if (filter instanceof SelectionPageableFilter) {
            SelectionPageableFilter pageableFilter = (SelectionPageableFilter) filter;
            reportOption.setSortOptions(pageableFilter.getSort());
            reportOption.setPageSize(pageableFilter.getSize());
            reportOption.setPageNumber(pageableFilter.getPage());
            reportOption.setDistinct(true);
        }

        ReportCondition reportCondition = new ReportCondition();
        ReportCriteriaJoinCondition partyJoin = new ReportCriteriaJoinCondition("parties", JoinType.INNER);
        ReportCriteriaJoinCondition memberJoin = new ReportCriteriaJoinCondition("members", JoinType.INNER);
        memberJoin.addEqualCondition("userId", filter.getUserId());
        partyJoin.addJoinCondition(memberJoin);
        reportCondition.addJoinCondition(partyJoin);

        return new ReportFilter(reportCondition, reportOption);
    }
}
