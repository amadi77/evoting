package com.evoting.pub.repository.dao;

import com.evoting.entity.core.entity.SelectionEntity;
import com.evoting.utility.repository.Dao;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class IdentSelectionDao extends Dao<SelectionEntity> {

    public List<SelectionEntity> getAll(int userId) {
        Query query = this.getEntityManager().createQuery("select selection from SelectionEntity selection " +
                " join fetch selection.parties parties " +
                " join parties.members members where members.userId=:userId");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("userId", userId);
        return queryHql(query, parameters);
    }

    public SelectionEntity getByIdAndUserId(int selectionId, int userId) {
        Query query = this.getEntityManager().createQuery("select selection from SelectionEntity selection " +
                " join fetch selection.parties parties " +
                " join fetch selection.options " +
                " join parties.members members where members.userId=:userId and selection.id = :selectionId");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("userId", userId);
        parameters.put("selectionId", selectionId);
        List<SelectionEntity> selectionEntities = queryHql(query, parameters);
        return selectionEntities.isEmpty() ? null : selectionEntities.get(0);
    }
}
