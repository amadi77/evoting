package com.evoting.pub.controller;

import com.evoting.pub.dto.VoteIn;
import com.evoting.pub.repository.IdentVoteService;
import com.evoting.statics.CoreRest;
import com.evoting.utility.model.object.SystemException;
import com.evoting.utility.statics.constants.RestApi;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@Api(tags = "Ident Selection Controller")
@RequestMapping(value = RestApi.REST_IDENTIFIED)
@Validated
public class IdentSelectionController {
    private final IdentVoteService service;

    @Autowired
    public IdentSelectionController(IdentVoteService service) {
        this.service = service;
    }

    @ApiOperation(value = "vote to user")
    @PostMapping(value = CoreRest.VOTE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void getById(@RequestBody VoteIn voteIn, BindingResult bindingResult, HttpServletRequest request) throws SystemException {
        service.vote(voteIn);
    }
}
