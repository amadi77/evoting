package com.evoting.pub.controller;

import com.evoting.pub.dto.SelectionOut;
import com.evoting.pub.dto.SelectionPageableFilter;
import com.evoting.pub.repository.IdentSelectionService;
import com.evoting.statics.CoreRest;
import com.evoting.utility.model.object.SystemException;
import com.evoting.utility.statics.constants.RestApi;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@Api(tags = "Ident Selection Controller")
@RequestMapping(value = RestApi.REST_IDENTIFIED)
@Validated
public class IdentVoteController {
    private final IdentSelectionService service;

    @Autowired
    public IdentVoteController(IdentSelectionService service) {
        this.service = service;
    }

    @ApiOperation(value = "get all active selection", response = SelectionOut.class, responseContainer = "List")
    @GetMapping(value = CoreRest.SELECTION, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<SelectionOut>> getAll(SelectionPageableFilter filter, HttpServletRequest request) throws SystemException {
        return new ResponseEntity<>(service.getAll(filter), HttpStatus.OK);
    }

    @ApiOperation(value = "get with options selection", response = SelectionOut.class, responseContainer = "List")
    @GetMapping(value = CoreRest.SELECTION_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SelectionOut> getById(@PathVariable("id") Integer id, HttpServletRequest request) throws SystemException {
        return new ResponseEntity<>(service.getById(id), HttpStatus.OK);
    }
}
