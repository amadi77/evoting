package com.evoting.pub.dto;

import com.evoting.entity.security.entity.party.PartyEntity;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PartyInfo {
    private Integer id;
    private String name;

    public PartyInfo(PartyEntity entity) {
        if (entity != null) {
            this.id = entity.getId();
            this.name = entity.getName();
        }
    }
}
