package com.evoting.pub.dto;

import com.evoting.utility.repository.service.FilterBase;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class SelectionFilter implements FilterBase {
    private Integer userId;
}
