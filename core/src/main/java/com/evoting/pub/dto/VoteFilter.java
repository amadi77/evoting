package com.evoting.pub.dto;

import com.evoting.utility.repository.service.FilterBase;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class VoteFilter implements FilterBase {
    private Integer userId;
    private Integer selectionId;
    private Integer optionId;
}
