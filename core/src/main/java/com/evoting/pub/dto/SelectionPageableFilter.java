package com.evoting.pub.dto;

import com.evoting.utility.model.object.SortOption;
import com.evoting.utility.statics.enums.SortType;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class SelectionPageableFilter extends SelectionFilter {
    @Min(1)
    private Integer size;
    @Min(1)
    private Integer page;
    private List<SortOption> sort;

    public SelectionPageableFilter() {
        this.size = 10;
        this.page = 1;
        this.sort = new ArrayList<>();
        this.sort.add(new SortOption("fromDate", SortType.DESCENDING));
    }
}
