package com.evoting.pub.dto;

import com.evoting.entity.core.entity.OptionEntity;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class OptionOut {
    private Integer id;
    private String description;
    private String fields;
    private String title;
    private String uniqCode;

    public OptionOut(OptionEntity entity) {
        if (entity != null) {
            this.id = entity.getId();
            this.description = entity.getDescription();
            this.fields = entity.getFields();
            this.title = entity.getTitle();
            this.uniqCode = entity.getUniqCode();
        }
    }
}
