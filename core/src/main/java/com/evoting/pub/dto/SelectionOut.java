package com.evoting.pub.dto;

import com.evoting.entity.core.entity.SelectionEntity;
import com.evoting.utility.config.StaticApplicationContext;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.Hibernate;
import org.modelmapper.ModelMapper;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Setter
@Getter
public class SelectionOut {
    private Integer id;
    private String name;
    private Integer limitVote;
    private LocalDateTime fromDate;
    private LocalDateTime toDate;
    private Boolean voted;
    @Setter(AccessLevel.PRIVATE)
    private List<PartyInfo> parties;
    @Setter(AccessLevel.PRIVATE)
    private List<OptionOut> options;

    public SelectionOut(SelectionEntity entity) {
        ModelMapper modelMapper = StaticApplicationContext.getContext().getBean(ModelMapper.class);
        modelMapper.map(entity, this);
        if (Hibernate.isInitialized(entity.getParties()) && entity.getParties() != null) {
            this.parties = entity.getParties().stream().map(PartyInfo::new).collect(Collectors.toList());
        }
        if (Hibernate.isInitialized(entity.getOptions()) && entity.getOptions() != null) {
            this.options = entity.getOptions().stream().map(OptionOut::new).collect(Collectors.toList());
        }
    }

    public SelectionOut(SelectionEntity entity, List<SelectionVoteResult> selectionVoteResults) {
        ModelMapper modelMapper = StaticApplicationContext.getContext().getBean(ModelMapper.class);
        modelMapper.map(entity, this);
        if (Hibernate.isInitialized(entity.getParties()) && entity.getParties() != null) {
            this.parties = entity.getParties().stream().map(PartyInfo::new).collect(Collectors.toList());
        }
        if (Hibernate.isInitialized(entity.getOptions()) && entity.getOptions() != null) {
            this.options = entity.getOptions().stream().map(OptionOut::new).collect(Collectors.toList());
        }
        selectionVoteResults.stream().filter(o -> o.getSelectionId().equals(this.id)).findFirst().ifPresent(o -> this.voted = o.getVoted());
    }
}
