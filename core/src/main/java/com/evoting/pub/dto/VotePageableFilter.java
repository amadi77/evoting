package com.evoting.pub.dto;

import com.evoting.utility.model.object.SortOption;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class VotePageableFilter extends VoteFilter{
    private Integer size;
    private Integer page;
    private List<SortOption> sort;

    public VotePageableFilter() {
        this.size = 10;
        this.page = 1;
        this.sort = new ArrayList<>();
    }
}
