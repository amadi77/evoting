package com.evoting.pub.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class SelectionVoteResult {
    private Boolean voted;
    private Integer selectionId;

    public SelectionVoteResult(Object voted, Integer selectionId) {
        this.voted = Integer.parseInt(voted.toString())>0;
        this.selectionId = selectionId;
    }
}
