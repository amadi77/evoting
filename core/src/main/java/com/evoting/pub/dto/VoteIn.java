package com.evoting.pub.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class VoteIn {
    private List<Integer> optionIds;
    private Integer selectionId;
}
